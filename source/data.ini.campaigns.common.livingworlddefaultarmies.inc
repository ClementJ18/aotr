//-------------------------------------------------------------------------------------------------
//	Default army composition for each faction
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Men of the West
//-------------------------------------------------------------------------------------------------
SpawnArmy
	ScriptingName = HeroArmy1
	SpawnForTemplates = PlayerMen
	HeroTemplateName = GondorFaramir
	PlayerArmy = FaramirPlayerArmy
	Icon = MoWArmyIcon
	Banner = BannerMen
End

SpawnArmy
	ScriptingName = HeroArmy2
	SpawnForTemplates = PlayerMen
	HeroTemplateName = GondorBoromir
	PlayerArmy = BoromirPlayerArmy
	Icon = MoWArmyIcon
	Banner = BannerMen
    PalantirMovie = Palantir_513	
End

SpawnArmy
	ScriptingName = HeroArmy3
	SpawnForTemplates = PlayerMen
	HeroTemplateName = GondorImrahil_WOTR
	PlayerArmy = ImrahilPlayerArmy
	Icon = MoWArmyIcon
	Banner = BannerMen
    PalantirMovie = Palantir_513	
End

SpawnArmy
	ScriptingName = GarrisonArmy1
	SpawnForTemplates = PlayerMen
	PlayerArmy = MenOfTheWest_StartingArmy
	Icon = MoWArmyIcon
End

//-------------------------------------------------------------------------------------------------
// Rohan
//-------------------------------------------------------------------------------------------------
SpawnArmy
	ScriptingName = HeroArmy1
	SpawnForTemplates = PlayerRohan
	HeroTemplateName = RohanErkenbrand
	PlayerArmy = ErkenbrandPlayerArmy
	Icon = RohanArmyIcon
	Banner = BannerRohan
End

SpawnArmy
	ScriptingName = HeroArmy2
	SpawnForTemplates = PlayerRohan
	HeroTemplateName = RohanTheodred
	PlayerArmy = TheodredPlayerArmy
	Icon = RohanArmyIcon
	Banner = BannerRohan
End

SpawnArmy
	ScriptingName = HeroArmy3
	SpawnForTemplates = PlayerRohan
	HeroTemplateName = RohanEomer
	PlayerArmy = EomerPlayerArmy
	Icon = RohanArmyIcon
	Banner = BannerRohan
End

SpawnArmy
	ScriptingName = GarrisonArmy1
	SpawnForTemplates = PlayerRohan
	PlayerArmy = Rohan_StartingArmy
	Icon = RohanArmyIcon
End

//-------------------------------------------------------------------------------------------------
// Lothlorien
//-------------------------------------------------------------------------------------------------

SpawnArmy
	ScriptingName = HeroArmy1
	SpawnForTemplates = PlayerElves
	HeroTemplateName = ElvenRumil
	PlayerArmy = RumilPlayerArmy
	Icon = ElfArmyIcon
	Banner = BannerElves
End

SpawnArmy
	ScriptingName = HeroArmy2
	SpawnForTemplates = PlayerElves
	HeroTemplateName = ElvenHaldir
	PlayerArmy = HaldirPlayerArmy
	Icon = ElfArmyIcon
	Banner = BannerElves
End

SpawnArmy
	ScriptingName = HeroArmy3
	SpawnForTemplates = PlayerElves
	HeroTemplateName = ElvenCeleborn
	PlayerArmy = CelebornPlayerArmy
	Icon = ElfArmyIcon
	Banner = BannerElves
End

SpawnArmy
	ScriptingName = GarrisonArmy1
	SpawnForTemplates = PlayerElves
	PlayerArmy = Elven_StartingArmy
	Icon = ElfArmyIcon
End

//-------------------------------------------------------------------------------------------------
// Dwarves
//-------------------------------------------------------------------------------------------------

SpawnArmy
	ScriptingName = HeroArmy1
	SpawnForTemplates = PlayerDwarves
	HeroTemplateName = DwarvenBalin
	PlayerArmy = BalinPlayerArmy
	Icon = DwarfArmyIcon
	Banner = BannerDwarves
End

SpawnArmy
	ScriptingName = HeroArmy2
	SpawnForTemplates = PlayerDwarves
	HeroTemplateName = DwarvenDain_WOTR
	PlayerArmy = DainPlayerArmy
	Icon = DwarfArmyIcon
	Banner = BannerDwarves
End

SpawnArmy
	ScriptingName = HeroArmy3
	SpawnForTemplates = PlayerDwarves
	HeroTemplateName = DwarvenCaptainofDale
	PlayerArmy = CaptainofDalePlayerArmy
	Icon = DwarfArmyIcon
	Banner = BannerDwarves
End

SpawnArmy
	ScriptingName = GarrisonArmy1
	SpawnForTemplates = PlayerDwarves
	PlayerArmy = Dwarven_StartingArmy
	Icon = DwarfArmyIcon
End

//-------------------------------------------------------------------------------------------------
// Rivendell
//-------------------------------------------------------------------------------------------------

SpawnArmy
	ScriptingName = HeroArmy1
	SpawnForTemplates = PlayerRivendell
	HeroTemplateName = ElvenGildor
	PlayerArmy = GildorPlayerArmy
	Icon = RivendellArmyIcon
	Banner = BannerRivendell
End

SpawnArmy
	ScriptingName = HeroArmy2
	SpawnForTemplates = PlayerRivendell
	HeroTemplateName = ElvenAragorn
	PlayerArmy = StriderPlayerArmy
	Icon = RivendellArmyIcon
	Banner = BannerRivendell
End

SpawnArmy
	ScriptingName = HeroArmy3
	SpawnForTemplates = PlayerRivendell
	HeroTemplateName = ElvenGlorfindel_WOTR
	PlayerArmy = GlorfindelPlayerArmy
	Icon = RivendellArmyIcon
	Banner = BannerRivendell
End

SpawnArmy
	ScriptingName = GarrisonArmy1
	SpawnForTemplates = PlayerRivendell
	PlayerArmy = Rivendell_StartingArmy
	Icon = RivendellArmyIcon
End

//-------------------------------------------------------------------------------------------------
// Mordor
//-------------------------------------------------------------------------------------------------

SpawnArmy
	ScriptingName = HeroArmy1
	SpawnForTemplates = PlayerMordor
	HeroTemplateName = MordorGothmog
	PlayerArmy = GothmogPlayerArmy
	Icon = MordorArmyIcon
	Banner = BannerMordor
End

SpawnArmy
	ScriptingName = HeroArmy2
	SpawnForTemplates = PlayerMordor
	HeroTemplateName = MordorMouthOfSauron
	PlayerArmy = MouthOfSauronArmy
	Icon = MordorArmyIcon
	Banner = BannerMordor
End

SpawnArmy
	ScriptingName = HeroArmy3
	SpawnForTemplates = PlayerMordor
	HeroTemplateName = MordorKhamul_WOTR
	PlayerArmy = KhamulPlayerArmy
	Icon = MordorArmyIcon
	Banner = BannerMordor
End

SpawnArmy
	ScriptingName = GarrisonArmy1
	SpawnForTemplates = PlayerMordor
	PlayerArmy = Mordor_StartingArmy
	Icon = MordorArmyIcon
End

//-------------------------------------------------------------------------------------------------
// Isengard
//-------------------------------------------------------------------------------------------------

SpawnArmy
	ScriptingName = HeroArmy1
	SpawnForTemplates = PlayerIsengard
	HeroTemplateName = IsengardUgluk
	PlayerArmy = UglukPlayerArmy
	Icon = IsengardArmyIcon
	Banner = BannerIsengard
End

SpawnArmy
	ScriptingName = HeroArmy2
	SpawnForTemplates = PlayerIsengard
	HeroTemplateName = IsengardLurtz
	PlayerArmy = LurtzPlayerArmy
	Icon = IsengardArmyIcon
	Banner = BannerIsengard
End

SpawnArmy
	ScriptingName = HeroArmy3
	SpawnForTemplates = PlayerIsengard
	HeroTemplateName = IsengardWulfgar_WOTR
	PlayerArmy = WulfgarPlayerArmy
	Icon = DunlandArmyIcon
	Banner = BannerIsengard
End

SpawnArmy
	ScriptingName = GarrisonArmy1
	SpawnForTemplates = PlayerIsengard
	PlayerArmy = Isengard_StartingArmy
	Icon = IsengardArmyIcon
End

//-------------------------------------------------------------------------------------------------
// Wild
//-------------------------------------------------------------------------------------------------

SpawnArmy
	ScriptingName = HeroArmy1
	SpawnForTemplates = PlayerWild
	HeroTemplateName = WildMuzgash
	PlayerArmy = MuzgashPlayerArmy
	Icon = WildArmyIcon
	Banner = BannerWild
End

SpawnArmy
	ScriptingName = HeroArmy2
	SpawnForTemplates = PlayerWild
	HeroTemplateName = WildUrshak
	PlayerArmy = UrshakPlayerArmy
	Icon = WildArmyIcon
	Banner = BannerWild
End

SpawnArmy
	ScriptingName = HeroArmy3
	SpawnForTemplates = PlayerWild
	HeroTemplateName = WildGreatGoblin
	PlayerArmy = GreatGoblinPlayerArmy
	Icon = WildArmyIcon
	Banner = BannerWild
End

SpawnArmy
	ScriptingName = GarrisonArmy1
	SpawnForTemplates = PlayerWild
	PlayerArmy = CorruptedWild_StartingArmy
	Icon = WildArmyIcon
End

//-------------------------------------------------------------------------------------------------
// Angmar
//-------------------------------------------------------------------------------------------------

SpawnArmy
	ScriptingName = HeroArmy1
	SpawnForTemplates = PlayerAngmar
	HeroTemplateName = AngmarRogash
	PlayerArmy = RogashPlayerArmy
	Icon = HeroRogashIcon
	Banner = BannerAngmar
End

SpawnArmy
	ScriptingName = HeroArmy2
	SpawnForTemplates = PlayerAngmar
	HeroTemplateName = AngmarMorgramir
	PlayerArmy = MorgramirPlayerArmy
	Icon = HeroMorgramirIcon
	Banner = BannerAngmar
End

SpawnArmy
	ScriptingName = HeroArmy3
	SpawnForTemplates = PlayerAngmar
	HeroTemplateName = AngmarWitchking
	PlayerArmy = WitchKingArmy
	Icon = HeroAngmarWitchkingIcon
	Banner = BannerAngmar
End

SpawnArmy
	ScriptingName = HeroArmy4
	SpawnForTemplates = PlayerAngmar
	HeroTemplateName = AngmarHwaldar
	PlayerArmy = HwaldarPlayerArmy
	Icon = HeroHwaldarIcon
	Banner = BannerAngmar
End

SpawnArmy
	ScriptingName = GarrisonArmy1
	SpawnForTemplates = PlayerAngmar
	PlayerArmy = Angmar_StartingArmy
	Icon = AngmarArmyIcon
End