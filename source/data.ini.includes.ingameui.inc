;-----------------------------------------------------------------------------------------------
;-----------------------------Rivendell----------------------------------------------
;-----------------------------------------------------------------------------------------------
	RadiusCursorTemplate = GandalfSummon
		Texture        = scgandalfsummon
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
  End
  RadiusCursorTemplate = OldManWillowSummon
		Texture        = scoldmanwillow
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
  End
	RadiusCursorTemplate = ArnorRuinRadiusCursor
		Texture        = SCArnorRuin
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
  End
  RadiusCursorTemplate = BamfurlongRadiusCursor
		Texture        = SCBamfurlong
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
  End  
  RadiusCursorTemplate = HallOfFireRadiusCursor
		Texture        = SCHOFSummon
	    Style          = SHADOW_ALPHA_DECAL
	    OpacityMin     = 20%
	    OpacityMax     = 60%
	    OpacityThrobTime  = 1000
	    Color          = R:255 G:255 B:255 A:255
	    OnlyVisibleToOwningPlayer = Yes
  End
  RadiusCursorTemplate = SongOfSpringRadiusCursor
		Texture        = scspringsong
	    Style          = SHADOW_ALPHA_DECAL
	    OpacityMin     = 20%
	    OpacityMax     = 60%
	    OpacityThrobTime  = 1000
	    Color          = R:255 G:255 B:255 A:255
	    OnlyVisibleToOwningPlayer = Yes
  End
  RadiusCursorTemplate = ArwenTinuvielRadiusCursor
		Texture        = SCTinuviel
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
  End    
  RadiusCursorTemplate = ArwenEvenstarRadiusCursor
		Texture        = scevenstar
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
  End   
  RadiusCursorTemplate = GlorfindelAmanRadiusCursor
		Texture        = sclightaman
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
  End  
  RadiusCursorTemplate = ElrondHomelyHouseRadiusCursor
		Texture        = schomelyhouse
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
  End
  RadiusCursorTemplate = ElrondRingOfAirRadiusCursor
		Texture        = scvilya
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
  End 
  RadiusCursorTemplate = AkallabethCursor
		Texture        = screvakall
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
  End   
;-----------------------------------------------------------------------------------------------
;-----------------------------Erebor----------------------------------------------
;-----------------------------------------------------------------------------------------------	
	RadiusCursorTemplate = IronHillsSummon
		Texture        = scIHsummon
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	
	RadiusCursorTemplate = KhazadDumSummon
		Texture        = scmoriasummon
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	
	RadiusCursorTemplate = ThorinProphecy
		Texture        = scprophecy
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	
	RadiusCursorTemplate = WyrmSlayersRunes
		Texture        = scrunesoffire
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = EreborHorn
		Texture        = scereborhorn
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = BardRabbleRouser
		Texture        = scrabblerouser
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = ThrorsHoard
		Texture        = scthrorshoard
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = EreborUnkindness
		Texture        = scunkindness
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = EreborFullyArmedandFilthy
		Texture        = scfilthy
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
;-----------------------------------------------------------------------------------------------
;-----------------------------Lorien----------------------------------------------
;-----------------------------------------------------------------------------------------------	
	RadiusCursorTemplate = RingOfAdamant
		Texture        = scringofadamant
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = MirkwoodSummon
		Texture        = scmirksummon
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = GoNoFurther
		Texture        = scnofurther
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = NandorTreasure
		Texture        = scossiriand
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = MallornGrove
		Texture        = scmallorngrove
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = ElvenHorn
		Texture        = sclorienhorn
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = RadagastSummon
		Texture        = scradagastsummon
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = CelebornWisdom
		Texture        = scwisebeyond
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
;-----------------------------------------------------------------------------------------------
;-----------------------------Gondor----------------------------------------------
;-----------------------------------------------------------------------------------------------
	RadiusCursorTemplate = DenethorFavor
		Texture        = scstewardfavor
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = DenethorHighAuthority
		Texture        = schighauthority
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End	
	RadiusCursorTemplate = GondorWhiteTree
		Texture        = scwhitetree
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = GondorLoneTower
		Texture        = scgondortower
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = IthilienGardenRadiusCursor
		Texture        = scithiliengarden
	    Style          = SHADOW_ALPHA_DECAL
	    OpacityMin     = 20%
	    OpacityMax     = 60%
	    OpacityThrobTime  = 1000
	    Color          = R:255 G:255 B:255 A:255
	    OnlyVisibleToOwningPlayer = Yes
	End 
	RadiusCursorTemplate = PrepareForBattleCursor
		Texture        = scprepareforbattle
	    Style          = SHADOW_ALPHA_DECAL
	    OpacityMin     = 20%
	    OpacityMax     = 60%
	    OpacityThrobTime  = 1000
	    Color          = R:255 G:255 B:255 A:255
	    OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = NumenorEngineeringCursor
		Texture        = scnumengi
	    Style          = SHADOW_ALPHA_DECAL
	    OpacityMin     = 20%
	    OpacityMax     = 60%
	    OpacityThrobTime  = 1000
	    Color          = R:255 G:255 B:255 A:255
	    OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = MordorChallengeCursor
		Texture        = scmordorchallenge
	    Style          = SHADOW_ALPHA_DECAL
	    OpacityMin     = 20%
	    OpacityMax     = 60%
	    OpacityThrobTime  = 1000
	    Color          = R:255 G:255 B:255 A:255
	    OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = CaptainHornCursor
		Texture        = sccapthorn
	    Style          = SHADOW_ALPHA_DECAL
	    OpacityMin     = 20%
	    OpacityMax     = 60%
	    OpacityThrobTime  = 1000
	    Color          = R:255 G:255 B:255 A:255
	    OnlyVisibleToOwningPlayer = Yes
	End	
;-----------------------------------------------------------------------------------------------
;-----------------------------Rohan----------------------------------------------
;-----------------------------------------------------------------------------------------------	
	RadiusCursorTemplate = OathOfCirionRadiusCursor
		Texture        = SCOathOfCirion
	    Style          = SHADOW_ALPHA_DECAL
	    OpacityMin     = 20%
	    OpacityMax     = 60%
	    OpacityThrobTime  = 1000
	    Color          = R:255 G:255 B:255 A:255
	    OnlyVisibleToOwningPlayer = Yes
  End
  RadiusCursorTemplate = EverholtBoarRadiusCursor
		Texture        = SCBoarSummon
	    Style          = SHADOW_ALPHA_DECAL
	    OpacityMin     = 20%
	    OpacityMax     = 60%
	    OpacityThrobTime  = 1000
	   Color          = R:255 G:255 B:255 A:255
	    OnlyVisibleToOwningPlayer = Yes
  End  
  RadiusCursorTemplate = StormcrowRadiusCursor
		Texture        = SCStormcrow
	    Style          = SHADOW_ALPHA_DECAL
	    OpacityMin     = 20%
	    OpacityMax     = 60%
	    OpacityThrobTime  = 1000
	    Color          = R:255 G:255 B:255 A:255
	    OnlyVisibleToOwningPlayer = Yes
  End
  RadiusCursorTemplate = WestfoldHornRadiusCursor
		Texture        = scwestfoldhorn
	    Style          = SHADOW_ALPHA_DECAL
	    OpacityMin     = 20%
	    OpacityMax     = 60%
	    OpacityThrobTime  = 1000
	    Color          = R:255 G:255 B:255 A:255
	    OnlyVisibleToOwningPlayer = Yes
  End
  RadiusCursorTemplate = MarkHelmRadiusCursor
		Texture        = scmarkhelm
	    Style          = SHADOW_ALPHA_DECAL
	    OpacityMin     = 20%
	    OpacityMax     = 60%
	    OpacityThrobTime  = 1000
	    Color          = R:255 G:255 B:255 A:255
	    OnlyVisibleToOwningPlayer = Yes
  End
  RadiusCursorTemplate = MarkFrecaRadiusCursor
		Texture        = scmarkfreca
	    Style          = SHADOW_ALPHA_DECAL
	    OpacityMin     = 20%
	    OpacityMax     = 60%
	    OpacityThrobTime  = 1000
	    Color          = R:255 G:255 B:255 A:255
	    OnlyVisibleToOwningPlayer = Yes
  End
  RadiusCursorTemplate = PukelSummonRadiusCursor
		Texture        = scpukelsummon
	    Style          = SHADOW_ALPHA_DECAL
	    OpacityMin     = 20%
	    OpacityMax     = 60%
	    OpacityThrobTime  = 1000
	    Color          = R:255 G:255 B:255 A:255
	    OnlyVisibleToOwningPlayer = Yes
  End   
  RadiusCursorTemplate = PukelFaithfulRadiusCursor
		Texture        = scpukelfaithful
	    Style          = SHADOW_ALPHA_DECAL
	    OpacityMin     = 20%
	    OpacityMax     = 60%
	    OpacityThrobTime  = 1000
	    Color          = R:255 G:255 B:255 A:255
	    OnlyVisibleToOwningPlayer = Yes
  End 
  RadiusCursorTemplate = PukelPathsRadiusCursor
		Texture        = scpukelpaths
	    Style          = SHADOW_ALPHA_DECAL
	    OpacityMin     = 20%
	    OpacityMax     = 60%
	    OpacityThrobTime  = 1000
	    Color          = R:255 G:255 B:255 A:255
	    OnlyVisibleToOwningPlayer = Yes
  End 
  RadiusCursorTemplate = PukelStoneSkinRadiusCursor
		Texture        = scpukelskin
	    Style          = SHADOW_ALPHA_DECAL
	    OpacityMin     = 20%
	    OpacityMax     = 60%
	    OpacityThrobTime  = 1000
	    Color          = R:255 G:255 B:255 A:255
	    OnlyVisibleToOwningPlayer = Yes
  End   
   RadiusCursorTemplate = SurrenderArmsRadiusCursor
 	Texture        = scsurrenderarms
    	Style          = SHADOW_ALPHA_DECAL
	OpacityMin     = 20%
    	OpacityMax     = 60%
    	OpacityThrobTime  = 1000
    	Color          = R:255 G:255 B:255 A:255
    	OnlyVisibleToOwningPlayer = Yes
  End  
;-----------------------------------------------------------------------------------------------
;-----------------------------Misty Mountains----------------------------------------------
;-----------------------------------------------------------------------------------------------
	RadiusCursorTemplate = DrumsInTheDeep
		Texture        = scdrumsdeep
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = UrshakIntimidation
		Texture        = scintimidation
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = FireDrakeStrikeRadiusCursor
		Texture        = SCCDragonStrike
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1500
		Color          = R:255 G:255 B:255 A:128
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = AngmarCitadelRadiusCursor
		Texture        = scangmarruin
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1500
		Color          = R:255 G:255 B:255 A:128
		OnlyVisibleToOwningPlayer = Yes
	End	
   RadiusCursorTemplate = BlackPitRadiusCursor
 	Texture        = scblackpit
    	Style          = SHADOW_ALPHA_DECAL
	OpacityMin     = 20%
    	OpacityMax     = 60%
    	OpacityThrobTime  = 1000
    	Color          = R:255 G:255 B:255 A:255
    	OnlyVisibleToOwningPlayer = Yes
  End
  RadiusCursorTemplate = WyrmSummonRadiusCursor
	Texture        = SCCCallOfTheDeep
    Style          = SHADOW_ALPHA_DECAL
    OpacityMin     = 20%
    OpacityMax     = 60%
    OpacityThrobTime  = 1000
    Color          = R:255 G:255 B:255 A:255
    OnlyVisibleToOwningPlayer = Yes
  End  
;-----------------------------------------------------------------------------------------------
;-----------------------------Mordor----------------------------------------------
;-----------------------------------------------------------------------------------------------
	RadiusCursorTemplate = WitchKingsHour
		Texture        = scmyhour
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = MoSBaradDur
		Texture        = scmouthbarad
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = MoSSorcery
		Texture        = scmouthsorcery
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
	RadiusCursorTemplate = MoSCursed
		Texture        = scmouthcursed
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End	
;-----------------------------------------------------------------------------------------------
;-----------------------------Isengard----------------------------------------------
;-----------------------------------------------------------------------------------------------
	RadiusCursorTemplate = IsenWhiteHand
		Texture        = scwhitehand
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End
;-----------------------------------------------------------------------------------------------
;-----------------------------Guldur----------------------------------------------
;-----------------------------------------------------------------------------------------------
	RadiusCursorTemplate = GuldurEncasingWebs
		Texture        = scencasingwebs
		Style          = SHADOW_ALPHA_DECAL
		OpacityMin     = 20%
		OpacityMax     = 60%
		OpacityThrobTime  = 1000
		Color          = R:255 G:255 B:255 A:255
		OnlyVisibleToOwningPlayer = Yes
	End	