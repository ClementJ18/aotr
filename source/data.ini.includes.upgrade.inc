Upgrade Upgrade_WOTRMode
	Type			= PLAYER
End
Upgrade Upgrade_GuldurFaction
	DisplayName		= UPGRADE:GuldurFaction
	Type			= PLAYER
End
Upgrade Upgrade_MirkwoodFaction
	DisplayName		= UPGRADE:MirkwoodFaction
	Type			= PLAYER
End
Upgrade Upgrade_TechnologyHardenedSkin
	DisplayName       = UPGRADE:HardenedSkin
	Type              = PLAYER
	BuildCost		= 500
	BuildTime		= 30
	ResearchCompleteEvaEvent	= UpgradeGenericGoblinReady
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_IMPORTANT
End
Upgrade Upgrade_HardenedSkin
	Type			= OBJECT
	BuildCost		= 300
	BuildTime		= 10
	DisplayName		= UPGRADE:HardenedSkin
	Tooltip		= UPGRADE:HardenedSkin
	ResearchSound	= UpgradeHeavyArmor
	UpgradeFX		= FX_PorterDeliverHeavyArmor
	ButtonImage     = BuildingNoArt
	Cursor			= WeaponUpgrade
	StrategicIcon	= AptStrategicUnitUpgradeArmor
	;RequiredObjectFilter = NONE +FireWyrmLair
End
Upgrade Upgrade_SentryTowerGarrisonGondor
	Type			= OBJECT
	BuildCost		= 150
	BuildTime		= WILD_SENTRY_TOWER_ARROW_UPGRADE_BUILDTIME
	DisplayName		= UPGRADE:GondorBattleTowerGarrison
	ResearchSound	= UpgradeFlamingArrows
End
Upgrade Upgrade_AmrothKnights
  DisplayName       = CONTROLBAR:AmrothKnights
  Type              = PLAYER
  PersistsInCampaign = Yes
End
Upgrade Upgrade_NimrodelWaters
	Type			= OBJECT
	BuildCost		= 300 ;375 ;500
	BuildTime		= 50 ;10
	DisplayName		= UPGRADE:NimrodelWaters
	Tooltip		= UPGRADE:ToolTipNimrodelWaters
	ResearchCompleteEvaEvent	= UpgradeWatersofNimrodel
End
Upgrade Upgrade_MarketplaceUpgradeGrandHarvestLorien
	BuildCost		= 600
	BuildTime		= 60
	DisplayName     = UPGRADE:MarketplaceUpgradeGrandHarvestLorien
	Type            = PLAYER
	ResearchCompleteEvaEvent	= UpgradeMirrorofGaladriel
End
Upgrade Upgrade_ThingsThatWere
	Type			= PLAYER
	BuildCost		= 600 ;MEN_FORTRESS_BANNERS_BUILDCOST
	BuildTime		= 30 ;MEN_FORTRESS_BANNERS_BUILDTIME
	DisplayName		= UPGRADE:ThingsthatWere
	ResearchCompleteEvaEvent	= UpgradeMirrorofGaladriel
	;UpgradeFX        = FX_DwarvenFortressUpgrade
	;ResearchSound	= UpgradeMenFortressBanner
	;ResearchCompleteEvaEvent   = UpgradeFortress
	;SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FORTRESS
End
Upgrade Upgrade_ThingsThatAre
	Type			= OBJECT
	BuildCost		= 600 ;MEN_FORTRESS_BANNERS_BUILDCOST
	BuildTime		= 30 ;MEN_FORTRESS_BANNERS_BUILDTIME
	DisplayName		= UPGRADE:ThingsthatAre
	ResearchCompleteEvaEvent	= UpgradeMirrorofGaladriel
End
Upgrade Upgrade_HasIsenSiegeWorks
    Type = OBJECT
	RequiredObjectFilter = NONE +IsengardSiegeWorks +IsengardSiegeWorksFoundation
End
Upgrade Upgrade_TREESTART
	Type			= OBJECT
End
Upgrade Upgrade_TURNBROWN
	Type			= OBJECT
End
Upgrade Upgrade_TURNDEAD
	Type			= OBJECT
End
Upgrade Upgrade_SwitchToTree
	Type			= OBJECT 
End
Upgrade Upgrade_DwarvenHornsoftheGate
	Type			= OBJECT
	BuildCost		= 1000
	BuildTime		= DWARVEN_OIL_CASK_BUILDTIME
	UpgradeFX		= FX_DwarvenFortressUpgrade
	DisplayName		= Upgrade:FortressOilCasks
	ResearchSound	= UpgradeDwarfFortressOilCasks
	ResearchCompleteEvaEvent   = UpgradeFortress
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FORTRESS
End
Upgrade Upgrade_Arkenstone
  DisplayName       = CONTROLBAR:ElvenGifts
  Type              = PLAYER
  PersistsInCampaign = Yes
End
Upgrade Upgrade_Evilboyos
  DisplayName       = UPGRADE:ElvenAllies
  Type              = PLAYER
  ButtonImage       = SCGrabPassenger
End

; ---- Roac specific upgrades

Upgrade Upgrade_RoacLevel3
	DisplayName       = UPGRADE:RoacLevel3
	Type              = OBJECT
	Tooltip		= TOOLTIP:RoacLevel3
	BuildCost		= 500 ;TDH_ROAC_LEVEL2_BUILDCOST
	BuildTime		= 10 ;TDH_ROAC_LEVEL_BUILDTIME
		NoUpgradeDiscount = Yes
End
Upgrade Upgrade_RoacLevel5
	DisplayName       = UPGRADE:RoacLevel5
	Type              = OBJECT
	Tooltip		= TOOLTIP:RoacLevel5
	BuildCost		= 800 ;TDH_ROAC_LEVEL3_BUILDCOST
	BuildTime		= 10 ;TDH_ROAC_LEVEL_BUILDTIME
		NoUpgradeDiscount = Yes
End

; ---- Denethor specific upgrades -also used by Arwen for levels 3 & 6

Upgrade Upgrade_DenethorLevel3
	DisplayName       = UPGRADE:DenethorLevel3
	Type              = OBJECT
	;Tooltip		= TOOLTIP:DenethorLevel3
	BuildCost		= 300
	BuildTime		= 10
	NoUpgradeDiscount = Yes
End
Upgrade Upgrade_DenethorLevel6
	DisplayName       = UPGRADE:DenethorLevel6
	Type              = OBJECT
	;Tooltip		= TOOLTIP:DenethorLevel6
	BuildCost		= 300
	BuildTime		= 10
	NoUpgradeDiscount = Yes
End
Upgrade Upgrade_DenethorLevel10
	DisplayName       = UPGRADE:DenethorLevel10
	Type              = OBJECT
	;Tooltip		= TOOLTIP:DenethorLevel10
	BuildCost		= 300
	BuildTime		= 10
	NoUpgradeDiscount = Yes
End
Upgrade Upgrade_ArwenLevel10
	DisplayName       = UPGRADE:ArwenLevel10
	Type              = OBJECT
	;Tooltip		= TOOLTIP:ArwenLevel10
	BuildCost		= 500
	BuildTime		= 10
	NoUpgradeDiscount = Yes
End
Upgrade Upgrade_BTDurinBanner
	Type			= OBJECT
	BuildCost		= 150
	BuildTime		= WILD_SENTRY_TOWER_ARROW_UPGRADE_BUILDTIME
	DisplayName		= UPGRADE:BTDurinBanner
	ResearchSound	= CampDwarfUpgradeGeneric
End
Upgrade Upgrade_BTKhazadBanner
	Type			= OBJECT
	BuildCost		= 150
	BuildTime		= WILD_SENTRY_TOWER_ARROW_UPGRADE_BUILDTIME
	DisplayName		= UPGRADE:BTKhazadBanner
	ResearchSound	= CampDwarfUpgradeGeneric
End
Upgrade Upgrade_BTEredMithrimBanner
	Type			= OBJECT
	BuildCost		= 150
	BuildTime		= WILD_SENTRY_TOWER_ARROW_UPGRADE_BUILDTIME
	DisplayName		= UPGRADE:BTEredMithrimBanner
	ResearchSound	= CampDwarfUpgradeGeneric
End
Upgrade Upgrade_BTIronHillsBanner
	Type			= OBJECT
	BuildCost		= 300 ;150
	BuildTime		= WILD_SENTRY_TOWER_ARROW_UPGRADE_BUILDTIME
	DisplayName		= UPGRADE:BTIronHillsBanner
	ResearchSound	= CampDwarfUpgradeGeneric
End
Upgrade Upgrade_BTEredLuinBanner
	Type			= OBJECT
	BuildCost		= 500 ;150
	BuildTime		= WILD_SENTRY_TOWER_ARROW_UPGRADE_BUILDTIME
	DisplayName		= UPGRADE:BTEredLuinBanner
	ResearchSound	= CampDwarfUpgradeGeneric
End

Upgrade Upgrade_DwarvenWorkshopLevel2
	Type			= OBJECT
	BuildCost		= 500 ;DWARVEN_FORGEWORKS_LEVEL2_UPGRADE_COST
	BuildTime		= DWARVEN_FORGEWORKS_LEVEL2_UPGRADE_BUILDTIME
	DisplayName	    = UPGRADE:DwarvenWorkshopLevel2
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FACTORY_UNITUNLOCK
End
Upgrade Upgrade_DwarvenWorkshopLevel3
	Type			= OBJECT
	BuildCost		= 200 ;DWARVEN_FORGEWORKS_LEVEL3_UPGRADE_COST
	BuildCost		= 200 ;DWARVEN_FORGEWORKS_LEVEL3_UPGRADE_COST
	BuildTime		= DWARVEN_FORGEWORKS_LEVEL3_UPGRADE_BUILDTIME
	DisplayName	    = UPGRADE:DwarvenWorkshopLevel3
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_IMPORTANT
End

Upgrade Upgrade_IsMounted
    Type = OBJECT
End

Upgrade Upgrade_FortressMapUpgrade
	DisplayName     = UPGRADE:MarketplaceUpgradeIronOre
	Type            = PLAYER
	BuildCost		= 1000 ;GONDOR_IRONORE_BUILDCOST
	BuildTime		= 5 ;GONDOR_IRONORE_BUILDTIME
	;ResearchCompleteEvaEvent	= UpgradeIronOre
End

//------  New Rohan Upgrades	--------------------------------------------------------
Upgrade Upgrade_TechnologyRohanBasicTraining
	DisplayName       = UPGRADE:RohanBasicTraining
	Type              = PLAYER
	BuildCost		= GONDOR_TECH_BASIC_TRAINING_BUILDCOST
	BuildTime		= GONDOR_TECH_BASIC_TRAINING_BUILDTIME
	ResearchCompleteEvaEvent	= UpgradeBannerCarrierTechnologyReady
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_IMPORTANT
End
Upgrade Upgrade_RohanBasicTraining
	DisplayName       = UPGRADE:RohanBasicTraining
	Type              = OBJECT
	Tooltip		= UPGRADE:RohanBasicTraining
	BuildCost		= 100;GONDOR_PERSONAL_BASIC_TRAINING_BUILDCOST
	BuildTime		= GONDOR_PERSONAL_BASIC_TRAINING_BUILDTIME
	ResearchCompleteEvaEvent	= UpgradeBannerCarrierReady
	RequiredObjectFilter = ANY +RohanBarracks +RohanBarracksFoundation
	StrategicIcon	= AptStrategicUnitUpgradeBannerCarrier
End
Upgrade Upgrade_TechnologyRohanFireArrows
	DisplayName       = UPGRADE:GondorFireArrowUpgrade
	Type              = PLAYER
	BuildCost		= GONDOR_TECH_FIRE_ARROWS_BUILDCOST
	BuildTime		= GONDOR_TECH_FIRE_ARROWS_BUILDTIME
	ResearchCompleteEvaEvent	= UpgradeFlameArrowsReady
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FORTRESS
End
Upgrade Upgrade_RohanFireArrows
	DisplayName       = UPGRADE:GondorFireArrowUpgrade
  	Tooltip		= TOOLTIP:GondorFlamingArrows
	Type              = OBJECT
	BuildCost		= GONDOR_PERSONAL_FIRE_ARROWS_BUILDCOST
	BuildTime		= GONDOR_PERSONAL_FIRE_ARROWS_BUILDTIME
	ResearchSound	= UpgradeFlamingArrows
	UpgradeFX		= FX_PorterDeliverFlamingArrows
	RequiredObjectFilter = ANY +RohanArcherRange +RohanArcherRangeFoundation
	StrategicIcon	= AptStrategicUnitUpgradeArrow
End
Upgrade Upgrade_TechnologyRohanForgedBlades
	DisplayName       = UPGRADE:GondorForgedBlades
	Type              = PLAYER
	BuildCost		= GONDOR_TECH_FORGED_BLADES_BUILDCOST
	BuildTime		= GONDOR_TECH_FORGED_BLADES_BUILDTIME
	ResearchCompleteEvaEvent	= UpgradeForgedBladesReady
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FORTRESS
End
Upgrade Upgrade_RohanForgedBlades
	DisplayName       = UPGRADE:GondorForgedBlades
  	Tooltip		= TOOLTIP:GondorForgedBlades
	Type              = OBJECT
	BuildCost		= GONDOR_PERSONAL_FORGED_BLADES_BUILDCOST
	BuildTime		= GONDOR_PERSONAL_FORGED_BLADES_BUILDTIME
	ResearchSound	= UpgradeForgedBlades
	UpgradeFX		= FX_PorterDeliverForgedBlades
	RequiredObjectFilter = ANY +RohanBarracks +RohanBarracksFoundation
	StrategicIcon	= AptStrategicUnitUpgradeBlade
End
Upgrade Upgrade_TechnologyRohanHeavyArmor
	DisplayName       = UPGRADE:GondorHeavyArmor
	Type              = PLAYER
	BuildCost		= GONDOR_TECH_HEAVY_ARMOR_BUILDCOST
	BuildTime		= GONDOR_TECH_HEAVY_ARMOR_BUILDTIME
	ResearchCompleteEvaEvent	= UpgradeHeavyArmorReady
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FORTRESS
End
Upgrade Upgrade_RohanHeavyArmor
	DisplayName       = UPGRADE:GondorHeavyArmor
	Tooltip		= TOOLTIP:GondorHeavyArmor
	Type              = OBJECT
	BuildCost		= GONDOR_PERSONAL_HEAVY_ARMOR_BUILDCOST
	BuildTime		= GONDOR_PERSONAL_HEAVY_ARMOR_BUILDTIME
	ResearchSound	= UpgradeHeavyArmor
	UpgradeFX		= FX_PorterDeliverHeavyArmor
	RequiredObjectFilter = ANY +RohanBarracks +RohanBarracksFoundation
	StrategicIcon	= AptStrategicUnitUpgradeArmor
End
Upgrade Upgrade_TechnologyRohanHorseShield
	DisplayName       = UPGRADE:GondorKnightShield
	Type              = PLAYER
	BuildCost		= GONDOR_TECH_KNIGHT_SHIELD_BUILDCOST
	BuildTime		= GONDOR_TECH_KNIGHT_SHIELD_BUILDTIME
	ResearchCompleteEvaEvent	= UpgradeKnightShieldsReady
End
Upgrade Upgrade_RohanHorseShield
	DisplayName       = UPGRADE:GondorKnightShield
  	Tooltip		= TOOLTIP:GondorKnightShields
	Type              = OBJECT
	BuildCost		= GONDOR_PERSONAL_KNIGHT_SHIELD_BUILDCOST
	BuildTime		= GONDOR_PERSONAL_KNIGHT_SHIELD_BUILDTIME
	ResearchSound	= UpgradeHeavyArmor
	UpgradeFX		= FX_PorterDeliverHorseShields
	RequiredObjectFilter = ANY +RohanStable +RohanStableFoundation
	StrategicIcon	= AptStrategicUnitUpgradeHorseShield
End
Upgrade Upgrade_TechnologyRohanFireStones
	DisplayName       = UPGRADE:GondorFireStones
	Type              = PLAYER
	BuildCost		= GONDOR_TECH_FIRE_STONES_BUILDCOST
	BuildTime		= GONDOR_TECH_FIRE_STONES_BUILDTIME
	ResearchCompleteEvaEvent	= UpgradeFlamingShotsReady
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FORTRESS
End
Upgrade Upgrade_RohanFireStones
	DisplayName       = UPGRADE:GondorFireStones
	Type              = OBJECT
	Tooltip		= TOOLTIP:GondorFireStones
	BuildCost		= GONDOR_PERSONAL_FIRE_STONES_BUILDCOST
	BuildTime		= GONDOR_PERSONAL_FIRE_STONES_BUILDTIME
	ResearchSound	= UpgradeFireStones
	UpgradeFX		= FX_PorterDeliverFireStones
	RequiredObjectFilter = NONE +RohanArmory
	StrategicIcon	= AptStrategicUnitUpgradeArrow
End

Upgrade Upgrade_RohanFortressHornsOfHammerhand
	Type			= PLAYER
	BuildCost		= 800 ;MEN_FORTRESS_HOUSES_OF_HEALING_BUILDCOST
	BuildTime		= MEN_FORTRESS_HOUSES_OF_HEALING_BUILDTIME
	DisplayName		= Upgrade:FortressHouseofHealing
	UpgradeFX        = FX_DwarvenFortressUpgrade
	ResearchCompleteEvaEvent   = UpgradeFortress
	ResearchSound = UpgradeDwarfFortressStonework	
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FORTRESS
End

Upgrade Upgrade_RohanFortressFlamingMunitionsTrigger
	Type			= OBJECT
	BuildCost		= 500
	BuildTime		= 30
	ResearchSound = UpgradeFireStones
	DisplayName		= Upgrade:FortressFlamingMunitions
	UpgradeFX        = FX_DwarvenFortressUpgrade
	ResearchCompleteEvaEvent   = UpgradeFortress	
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FORTRESS
End

Upgrade Upgrade_RohanFortressHastyFortificationsTrigger // You actually buy this one, which gives the one below to the whole castle
	Type			= OBJECT
	BuildCost		= 800 ;MEN_FORTRESS_NUMENORIAN_STONEWORK_BUILDCOST
	BuildTime		= MEN_FORTRESS_NUMENORIAN_STONEWORK_BUILDTIME
	DisplayName		= Upgrade:FortressNumenorStonework
	UpgradeFX        = FX_DwarvenFortressUpgrade
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FORTRESS
	ResearchCompleteEvaEvent	= UpgradeFortress
	ResearchSound = UpgradeDwarfFortressStonework
End

Upgrade Upgrade_RohanFortressHastyFortifications
	Type			= OBJECT
	BuildCost		= 800 ;GONDOR_NEMENORSTONEWORK_BUILDCOST
	BuildTime		= GONDOR_NEMENORSTONEWORK_BUILDTIME
	DisplayName		= Upgrade:FortressNumenorStonework
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FORTRESS
	ResearchCompleteEvaEvent	= UpgradeFortress
	ResearchSound = UpgradeDwarfFortressStonework
End
Upgrade Upgrade_RohanFortressDesperateGarrison
	Type			= OBJECT
	BuildCost		= 650 ;GONDOR_NEMENORSTONEWORK_BUILDCOST
	BuildTime		= GONDOR_NEMENORSTONEWORK_BUILDTIME
	DisplayName		= Upgrade:FortressNumenorStonework
	UpgradeFX        = FX_DwarvenFortressUpgrade	
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FORTRESS
	ResearchCompleteEvaEvent	= UpgradeFortress
	ResearchSound = UpgradeDwarfFortressSiegeKegs
End
Upgrade Upgrade_RohanFortressMeadHall
	Type			= OBJECT
	BuildCost		= 800 ;GONDOR_NEMENORSTONEWORK_BUILDCOST
	BuildTime		= GONDOR_NEMENORSTONEWORK_BUILDTIME
	DisplayName		= Upgrade:FortressNumenorStonework
	UpgradeFX        = FX_DwarvenFortressUpgrade	
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FORTRESS
	ResearchCompleteEvaEvent	= UpgradeFortress
	ResearchSound = UpgradeRohanFortressMeadMS
End
Upgrade Upgrade_RohanFortressRideoftheRohirrim
	Type			= OBJECT
	BuildCost		= 1200 ;MEN_FORTRESS_IVORY_TOWER_BUILDCOST
	BuildTime		= MEN_FORTRESS_IVORY_TOWER_BUILDTIME
	DisplayName		= Upgrade:FortressIvoryTower
	UpgradeFX        = FX_DwarvenFortressUpgrade
	ResearchCompleteEvaEvent	= UpgradeFortress
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FORTRESS
	ResearchSound = UpgradeRohanFortressRohirrimMS	
End

Upgrade Upgrade_DunedainSpears
  Type               = OBJECT
  Tooltip            = TOOLTIP:DunedainSpear
  DisplayName        = UPGRADE:DunedainSpear
  ButtonImage        = BuildingNoArt
  Cursor             = WeaponUpgrade
  BuildTime          = 0.0
  BuildCost          = 100
  ResearchSound = DunedainRangerVoiceEquipSpears
End

Upgrade Upgrade_DunedainSwords
  Type               = OBJECT
  Tooltip            = TOOLTIP:DunedainSword
  DisplayName        = UPGRADE:DunedainSword
  ButtonImage        = BuildingNoArt
  Cursor             = WeaponUpgrade
  BuildTime          = 0.0
  BuildCost          = 100
  ResearchSound = DunedainRangerVoiceEquipSwords
End

Upgrade Upgrade_StoneTrollDreadHelmet
  Type               = OBJECT
  Tooltip            = TOOLTIP:DreadHelmet
  DisplayName        = UPGRADE:DreadHelmet
  ButtonImage        = BuildingNoArt
  Cursor             = WeaponUpgrade
  BuildTime          = GONDOR_PERSONAL_KNIGHT_SHIELD_BUILDTIME
  BuildCost          = 500
End

Upgrade Upgrade_StoneTrollSerratedArmor
  Type               = OBJECT
  Tooltip            = TOOLTIP:SerratedArmor
  DisplayName        = UPGRADE:SerratedArmor
  ButtonImage        = BuildingNoArt
  Cursor             = WeaponUpgrade
  BuildTime          = GONDOR_PERSONAL_KNIGHT_SHIELD_BUILDTIME
  BuildCost          = 500
End

Upgrade Upgrade_StoneTrollScytheGauntlets
  Type               = OBJECT
  Tooltip            = TOOLTIP:ScytheGauntlets
  DisplayName        = UPGRADE:ScytheGauntlets
  ButtonImage        = BuildingNoArt
  Cursor             = WeaponUpgrade
  BuildTime          = GONDOR_PERSONAL_KNIGHT_SHIELD_BUILDTIME
  BuildCost          = 500
End

Upgrade Upgrade_MallornTreeLevel2
   Type            = OBJECT
    BuildCost      = 250
    BuildTime      = ELVEN_BARRACKS_LEVEL2_UPGRADE_BUILDTIME
    DisplayName	= UPGRADE:MallornTreeLevel2
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FACTORY_UNITUNLOCK
End

Upgrade Upgrade_MallornTreeLevel3
    	Type           = OBJECT
    	BuildCost      = 600
    	BuildTime      = ELVEN_BARRACKS_LEVEL3_UPGRADE_BUILDTIME
    	DisplayName	= UPGRADE:MallornTreeLevel3
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FACTORY_UNITUNLOCK
End

//------------------- WotR upgrades used for hero revival during RTS plays ----------------------------------------

Upgrade Upgrade_WOTRCaHHero
	Type			= PLAYER
End

Upgrade Upgrade_WOTRHeroUnit1
	Type			= PLAYER
End

Upgrade Upgrade_WOTRHeroUnit2
	Type			= PLAYER
End

Upgrade Upgrade_WOTRHero1
	Type			= PLAYER
End

Upgrade Upgrade_WOTRHero2
	Type			= PLAYER
End

Upgrade Upgrade_WOTRHero3
	Type			= PLAYER
End

Upgrade Upgrade_WOTRHero4
	Type			= PLAYER
End

Upgrade Upgrade_WOTRHero5
	Type			= PLAYER
End

Upgrade Upgrade_WOTRHero6
	Type			= PLAYER
End

Upgrade Upgrade_WOTRHero7
	Type			= PLAYER
End

Upgrade Upgrade_WOTRHero8
	Type			= PLAYER
End

Upgrade Upgrade_WOTRHero9
	Type			= PLAYER
End

Upgrade Upgrade_WOTRHero10
	Type			= PLAYER
End

Upgrade Upgrade_DraftTowerWestfold
   Type            = OBJECT
    BuildCost      = 600
    BuildTime      = 30
	DisplayName		= UPGRADE:DraftTowerWestfold
End

Upgrade Upgrade_DraftTowerFirienholt
   Type            = OBJECT
    BuildCost      = 600
    BuildTime      = 30
	DisplayName		= UPGRADE:DraftTowerFirienholt	
End

Upgrade Upgrade_DraftTowerEored
   Type            = OBJECT
    BuildCost      = 900
    BuildTime      = 30
	DisplayName		= UPGRADE:DraftTowerEored	
End

Upgrade Upgrade_WildFortressGoblinTownTrigger
	Type			= PLAYER
	BuildCost		= 800
	BuildTime		= 15
	ResearchSound	= UpgradeGoblinFortressWebCocoon
	DisplayName		= Upgrade:FortressWebCocoon
	UpgradeFX        = FX_DwarvenFortressUpgrade
	ResearchCompleteEvaEvent   = UpgradeFortress
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FORTRESS
End

Upgrade Upgrade_WildFortressGoblinTown
	Type			= OBJECT	
	DisplayName	    	= Upgrade:FortressWebCocoon
End

Upgrade Upgrade_WildFortressNorthernWastes
	Type			= PLAYER
	BuildCost		= 1500
	BuildTime		= WILD_FORTRESS_WEBCOCOON_BUILDTIME
	ResearchSound	= UpgradeGoblinFortressWebCocoon
	DisplayName		= Upgrade:FortressWebCocoon
	UpgradeFX        = FX_DwarvenFortressUpgrade
	ResearchCompleteEvaEvent   = UpgradeFortress
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FORTRESS
End

Upgrade Upgrade_MineshaftTunnelCollapse
	Type			= OBJECT
	BuildCost		= 150
	BuildTime		= 30
	DisplayName		= UPGRADE:MineshaftTunnelCollapse
	Tooltip		= UPGRADE:ToolTipTunnelCollapse
End

Upgrade Upgrade_RohanStableLevel3
	Type			= OBJECT
	BuildCost		= 700
	BuildTime		= GONDOR_STABLE_LEVEL3_UPGRADE_BUILDTIME
	DisplayName		= Upgrade:GondorStablesLevel3
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FACTORY_UNITUNLOCK
End

//----Lodge----
Upgrade Upgrade_WildLodgeLevel2
	Type			= OBJECT
	BuildCost		= 400
	BuildTime		= GOBLIN_CAVE_LEVEL2_UPGRADE_BUILDTIME
	DisplayName		= Upgrade:WildFissureLevel2
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FACTORY_UNITUNLOCK
End
Upgrade Upgrade_WildLodgeLevel3
	Type			= OBJECT
	BuildCost		= 800
	BuildTime		= GOBLIN_CAVE_LEVEL3_UPGRADE_BUILDTIME
	DisplayName		= Upgrade:WildFissureLevel3
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FACTORY_UNITUNLOCK
End

//---- New Berserker Upgrades -----
Upgrade Upgrade_IsengardBerserkerMansblood
  Type               = OBJECT
  Tooltip            = TOOLTIP:Mansblood
  DisplayName        = UPGRADE:Mansblood
  ButtonImage        = BuildingNoArt
  Cursor             = WeaponUpgrade
  BuildTime          = GONDOR_PERSONAL_KNIGHT_SHIELD_BUILDTIME
  BuildCost          = 150
End
Upgrade Upgrade_IsengardBerserkerThickSkin
  Type               = OBJECT
  Tooltip            = TOOLTIP:ThickSkin
  DisplayName        = UPGRADE:ThickSkin
  ButtonImage        = BuildingNoArt
  Cursor             = WeaponUpgrade
  BuildTime          = GONDOR_PERSONAL_KNIGHT_SHIELD_BUILDTIME
  BuildCost          = 300
End
Upgrade Upgrade_IsengardBerserkerIronFist
  Type               = OBJECT
  Tooltip            = TOOLTIP:IronFist
  DisplayName        = UPGRADE:IronFist
  ButtonImage        = BuildingNoArt
  Cursor             = WeaponUpgrade
  BuildTime          = GONDOR_PERSONAL_KNIGHT_SHIELD_BUILDTIME
  BuildCost          = 450
End
Upgrade Upgrade_GondorBodkinArrows
	DisplayName       = UPGRADE:GondorBodkinArrowUpgrade
  	Tooltip		= TOOLTIP:GondorBodkingArrows
	Type              = OBJECT
	BuildCost		= GONDOR_PERSONAL_FIRE_ARROWS_BUILDCOST
	BuildTime		= GONDOR_PERSONAL_FIRE_ARROWS_BUILDTIME
	ResearchSound	= UpgradeSilverThornArrows
	UpgradeFX		= FX_PorterDeliverFlamingArrows
	RequiredObjectFilter = ANY +GondorArcherRange +GondorForbiddenPool +GondorArcherRangeFoundation
	StrategicIcon	= AptStrategicUnitUpgradeArrow
End
Upgrade Upgrade_TechnologyGuldurBasicTraining
	DisplayName       	= UPGRADE:GuldurBasicTraining
	Type              	= PLAYER
	BuildCost		= MORDOR_TECH_BASIC_TRAINING_BUILDCOST
	BuildTime		= MORDOR_TECH_BASIC_TRAINING_BUILDTIME
	ResearchCompleteEvaEvent  = UpgradeBannerCarrierTechnologyReady-Mordor
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_IMPORTANT
End
Upgrade Upgrade_GuldurBasicTraining
	DisplayName       	= UPGRADE:GuldurBasicTraining
	Type             	= OBJECT
	Tooltip			= UPGRADE:GuldurBasicTraining
	BuildCost		= 100
	BuildTime		= MORDOR_PERSONAL_BASIC_TRAINING_BUILDTIME
	ResearchCompleteEvaEvent  = UpgradeBannerCarrierReady
	RequiredObjectFilter 	= NONE +GuldurGaol +GuldurGaolFoundation +GuldurGaol_DE
	StrategicIcon		= AptStrategicUnitUpgradeBannerCarrier
End
Upgrade Upgrade_TechnologyGuldurFireArrows
	DisplayName       	= UPGRADE:MordorFireArrowUpgrade
	Type              	= PLAYER
	BuildCost			= 1500
	BuildTime			= MORDOR_TECH_FIRE_ARROWS_BUILDTIME
	ResearchCompleteEvaEvent	= UpgradeFlameArrowsReady
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FORTRESS
End
Upgrade Upgrade_GuldurFireArrows
	DisplayName       		= UPGRADE:MordorFireArrowUpgrade
	Type              		= OBJECT
	Tooltip					= TOOLTIP:MordorFireArrows
	BuildCost				= 600
	BuildTime				= MORDOR_PERSONAL_FIRE_ARROWS_BUILDTIME
	ResearchSound			= UpgradeFlamingArrows
	UpgradeFX				= FX_PorterDeliverFlamingArrows
	RequiredObjectFilter 	= NONE +GuldurGaol +GuldurGaolFoundation
	StrategicIcon			= AptStrategicUnitUpgradeArrow
End
Upgrade Upgrade_GuldurFortressEncroachment
	Type				= OBJECT
	BuildCost			= 800
	BuildTime			= ELVEN_BLESSEDMIST_BUILDTIME
	DisplayName			= UPGRADE:FortressGuldurBlight
	UpgradeFX        	= FX_DwarvenFortressUpgrade
	ResearchSound		= UpgradeElfFortressBlessedMist
	ResearchCompleteEvaEvent   = UpgradeFortress
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FORTRESS
End
Upgrade Upgrade_GuldurFortressBlight
	Type				= OBJECT
	BuildCost			= 800
	BuildTime			= ELVEN_BLESSEDMIST_BUILDTIME
	DisplayName			= UPGRADE:FortressGuldurBlight
	UpgradeFX        	= FX_DwarvenFortressUpgrade
	ResearchSound		= UpgradeElfFortressBlessedMist
	ResearchCompleteEvaEvent   = UpgradeFortress
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FORTRESS
End
Upgrade Upgrade_GuldurFortressPalantir
	Type				= OBJECT
	BuildCost			= 600
	BuildTime			= ELVEN_BLESSEDMIST_BUILDTIME
	DisplayName			= UPGRADE:FortressGuldurPalantir
	UpgradeFX        	= FX_DwarvenFortressUpgrade
	ResearchSound		= UpgradeElfFortressBlessedMist
	ResearchCompleteEvaEvent   = UpgradeFortress
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FORTRESS
End
Upgrade Upgrade_GuldurFortressSorcery
	Type				= OBJECT
	BuildCost			= 1500
	BuildTime			= ELVEN_BLESSEDMIST_BUILDTIME
	DisplayName			= UPGRADE:FortressGuldurPalantir
	UpgradeFX        	= FX_DwarvenFortressUpgrade
	ResearchSound		= UpgradeElfFortressBlessedMist
	ResearchCompleteEvaEvent   = UpgradeFortress
	SkirmishAIHeuristic = AI_UPGRADEHEURISTIC_FORTRESS
End
Upgrade Upgrade_TechnologyGuldurNecromancy
	DisplayName       	= UPGRADE:GuldurNecromancy
	Type              	= PLAYER
	BuildCost			= 800
	BuildTime			= GONDOR_TECH_FIRE_STONES_BUILDTIME
	;ResearchSound		= CampRivendellElfLibraryUpgrade
	;ResearchCompleteEvaEvent	= UpgradeRivendellLibrary
End