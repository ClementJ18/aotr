-- define lua functions 
function NoOp(self, source)
end


function kill(self) -- Kill unit self.
	ExecuteAction("NAMED_KILL", self);
end

function RadiatePhialFear( self )
	ObjectBroadcastEventToEnemies( self, "BeAfraidOfPhial", 75 )
end

function RadiateUncontrollableFear( self )
	ObjectBroadcastEventToEnemies( self, "BeUncontrollablyAfraid", 350 )
end

function OnMoving(self)
    ObjectRemoveUpgrade( self, "Upgrade_NotMovingBow" )
    ObjectGrantUpgrade( self, "Upgrade_MovingBow" )
end

function OnNotMoving(self)
    ObjectRemoveUpgrade( self, "Upgrade_MovingBow" )
    ObjectGrantUpgrade( self, "Upgrade_NotMovingBow" )
end

-- ;;,;;
function RadiateUncontrollableFearBoromir( self )
	ObjectBroadcastEventToEnemies( self, "BeUncontrollablyAfraid", 300 )
end

function RadiateGateDamageFear(self)
	ObjectBroadcastEventToAllies(self, "BeAfraidOfGateDamaged", 200)
end

function RadiateBalrogFear(self)
	ObjectBroadcastEventToEnemies(self, "BeAfraidOfBalrog", 180)
end

function OnMumakilCreated(self)
	ObjectHideSubObjectPermanently( self, "Houda", true )
	ObjectHideSubObjectPermanently( self, "Houda01", true )
end

function OnTrollCreated(self)
	ObjectHideSubObjectPermanently( self, "Trunk01", true )
	ObjectGrantUpgrade( self, "Upgrade_SwitchToRockThrowing" )
end

function OnGundabadTrollCreated(self)
	ObjectHideSubObjectPermanently( self, "Trunk01", true )
	ObjectHideSubObjectPermanently( self, "HELMET", true )
	ObjectHideSubObjectPermanently( self, "SCYTHES", true )
	ObjectHideSubObjectPermanently( self, "ARMOR", true )
	ObjectHideSubObjectPermanently( self, "PAULDRONS", true )
	ObjectGrantUpgrade( self, "Upgrade_SwitchToRockThrowing" )
end

function OnCreepTrollCreated(self)
	ObjectHideSubObjectPermanently( self, "Trunk01", true )
	ObjectHideSubObjectPermanently( self, "ROCK", true )
end

function OnCaptureFlagGenericEvent(self,data)
	local str = ObjectCapturingObjectPlayerSide(self)
	if str == nil then
		str = ObjectPlayerSide(self)
	end


	ObjectHideSubObjectPermanently( self, "FLAG_ISENGARD", true)
	ObjectHideSubObjectPermanently( self, "FLAG_MORDOR", true)
	ObjectHideSubObjectPermanently( self, "FLAG_WILD", true)
	ObjectHideSubObjectPermanently( self, "FLAG_MEN", true)
	ObjectHideSubObjectPermanently( self, "FLAG_ELVES", true)
	ObjectHideSubObjectPermanently( self, "FLAG_DWARVES", true)
	ObjectHideSubObjectPermanently( self, "FLAG_ANGMAR", true)
	
	ObjectHideSubObjectPermanently( self, "FLAG_ARNOR", true)
	ObjectHideSubObjectPermanently( self, "FLAG_ROHAN", true)

	if str == "Isengard" then
		ObjectHideSubObjectPermanently( self, "FLAG_ISENGARD", false)
	elseif str == "Mordor" then
		ObjectHideSubObjectPermanently( self, "FLAG_MORDOR", false)
	elseif str == "Men" then
		ObjectHideSubObjectPermanently( self, "FLAG_MEN", false)
	elseif str == "Arnor" then	-- Added for v2.3 ;;,;;
		ObjectHideSubObjectPermanently( self, "FLAG_ARNOR", false)
	elseif str == "Dwarves" then
		ObjectHideSubObjectPermanently( self, "FLAG_DWARVES", false)
	elseif str == "Elves" then
		ObjectHideSubObjectPermanently( self, "FLAG_ELVES", false)
	elseif str == "Wild" then
		ObjectHideSubObjectPermanently( self, "FLAG_WILD", false)
		elseif str == "Rohan" then
		ObjectHideSubObjectPermanently( self, "FLAG_ROHAN", false)
	elseif str == "Angmar" then
		ObjectHideSubObjectPermanently( self, "FLAG_ANGMAR", false)
	else
		ObjectHideSubObjectPermanently( self, "FLAG_NEUTRAL", false)
	end
end

function OnTrollGenericEvent(self,data)

	local str = tostring( data )

	if str == "show_rock" then
		ObjectHideSubObjectPermanently( self, "ROCK", false )
	elseif str == "hide_rock" then
		ObjectHideSubObjectPermanently( self, "ROCK", true )
	end
end

function OnEntCreated(self)
	--ObjectShowSubObjectPermanently( self, "ROCK", true )
	ObjectGrantUpgrade( self, "Upgrade_SwitchToRockThrowing" )
end

function OnMountainGiantCreated(self)
	--ObjectHideSubObjectPermanently( self, "ROCK", true )
	ObjectGrantUpgrade( self, "Upgrade_SwitchToRockThrowing" )
end

function OnMountainGiantGenericEvent(self)
	
	local str = tostring( data )

	if str == "show_rock" then
		ObjectHideSubObjectPermanently( self, "ROCK", false )
	elseif str == "hide_rock" then
		ObjectHideSubObjectPermanently( self, "ROCK", true )
	end
end

function GoIntoRampage(self)
	ObjectEnterRampageState(self)
		
	--Broadcast fear to surrounding unit(if we actually rampaged)
	if ObjectTestModelCondition(self, "WEAPONSET_RAMPAGE") then
		ObjectBroadcastEventToUnits(self, "BeAfraidOfRampage", 250)
	end
end

function MakeMeAlert(self)
	ObjectEnterAlertState(self)
end

function BeEnraged(self)
	--Broadcast enraged to surrounding units.
	ObjectBroadcastEventToAllies(self, "BeingEnraged", 500)
end

function BecomeEnraged(self)
	ObjectSetEnragedState(self, true)
end

function StopEnraged(self)
	ObjectSetEnragedState(self, false)
end

function BecomeUncontrollablyAfraid(self, other)
	if not ObjectTestCanSufferFear(self) then
		return
	end

	ObjectEnterUncontrollableCowerState(self, other)
end

function BecomeAfraidOfRampage(self, other)
	if not ObjectTestCanSufferFear(self) then
		return
	end

	ObjectEnterCowerState(self, other)
end

function BecomeAfraidOfBalrog(self, other)
	if not ObjectTestCanSufferFear(self) then
		return
	end

	ObjectEnterCowerState(self, other)
end

function RadiateTerror(self, other)
	ObjectBroadcastEventToEnemies(self, "BeTerrified", 180)
end
	
function RadiateTerrorEx(self, other, terrorRange)
	ObjectBroadcastEventToEnemies(self, "BeTerrified", terrorRange)
end

function RadiateFearEx(self, other, terrorRange)
	ObjectBroadcastEventToEnemies( self, "BeAfraidOfPhial", 200 )
end

function BecomeTerrified(self, other)
	ObjectEnterRunAwayPanicState(self, other)
end

function BecomeAfraidOfGateDamaged(self, other)
	if not ObjectTestCanSufferFear(self) then
		return
	end

	ObjectEnterCowerState(self,other)
end


function ChantForUnit(self) -- Used by units to broadcast the chant event to their own side.
	ObjectBroadcastEventToAllies(self, "BeginChanting", 9999)
end

function StopChantForUnit(self) -- Used by units to stop the chant event to their own side.
	ObjectBroadcastEventToAllies(self, "StopChanting", 9999)
end

function BeginCheeringForGrond(self)
	ObjectSetChanting(self, true)
end

function StopCheeringForGrond(self)
	ObjectSetChanting(self, false)
end

function BeInMountedState(self)
    ObjectGrantUpgrade( self, "Upgrade_IsMounted" )
end

function BeNotInMountedState(self)
    ObjectRemoveUpgrade( self, "Upgrade_IsMounted" )
end

function OnMordorArcherCreated(self)
    ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )
    ObjectHideSubObjectPermanently( self, "ARROWFIRE", true )  
    -- helmet types
    ObjectHideSubObjectPermanently( self, "HELM1", true )
    ObjectHideSubObjectPermanently( self, "HELM2", true )
    ObjectHideSubObjectPermanently( self, "HELM3", true )
    ObjectHideSubObjectPermanently( self, "HELM4", true )
    ObjectHideSubObjectPermanently( self, "HELM5", true )
    ObjectHideSubObjectPermanently( self, "HELM6", true )
    ObjectHideSubObjectPermanently( self, "HELM7", true )
    ObjectHideSubObjectPermanently( self, "HELM8", true )
    ObjectHideSubObjectPermanently( self, "HELM9", true )
    ObjectHideSubObjectPermanently( self, "HELM10", true )
    ObjectHideSubObjectPermanently( self, "HELM11", true )
    ObjectHideSubObjectPermanently( self, "HELM12", true )
    ObjectHideSubObjectPermanently( self, "HELM13", true )
    ObjectHideSubObjectPermanently( self, "HELM14", true )
    ObjectHideSubObjectPermanently( self, "HELM15", true )
    ObjectHideSubObjectPermanently( self, "HELM16", true )
    ObjectHideSubObjectPermanently( self, "HELM17", true )
    ObjectHideSubObjectPermanently( self, "HELM18", true )
    ObjectHideSubObjectPermanently( self, "HELM19", true )
    ObjectHideSubObjectPermanently( self, "HELM20", true )    
    ObjectHideSubObjectPermanently( self, "HELM21", true )
    ObjectHideSubObjectPermanently( self, "HELM22", true )
    ObjectHideSubObjectPermanently( self, "HELM23", true )
    ObjectHideSubObjectPermanently( self, "HELM24", true )    
    ObjectHideSubObjectPermanently( self, "HELM25", true )
    ObjectHideSubObjectPermanently( self, "HELM26", true )    
    -- shield types
    ObjectHideSubObjectPermanently( self, "SHIELD1", true )
    ObjectHideSubObjectPermanently( self, "SHIELD2", true )
    ObjectHideSubObjectPermanently( self, "SHIELD3", true )
    ObjectHideSubObjectPermanently( self, "SHIELD4", true )
    ObjectHideSubObjectPermanently( self, "SHIELD5", true )
    ObjectHideSubObjectPermanently( self, "SHIELD6", true )
    ObjectHideSubObjectPermanently( self, "SHIELD7", true )
    ObjectHideSubObjectPermanently( self, "SHIELD8", true )
    -- bow types
    ObjectHideSubObjectPermanently( self, "BOW1", true )
    ObjectHideSubObjectPermanently( self, "BOW2", true )
    ObjectHideSubObjectPermanently( self, "BOW3", true )
    -- body types
    ObjectHideSubObjectPermanently( self, "ORC", true )
    ObjectHideSubObjectPermanently( self, "ORC1", true )
    ObjectHideSubObjectPermanently( self, "ORC2", true )
    ObjectHideSubObjectPermanently( self, "ORC3", true )
    
    local helm = GetRandomNumber()
    local shield = GetRandomNumber()
    local bow = GetRandomNumber()
    local body = GetRandomNumber()
    
    -- assign random helm
    if helm <= 0.04 then
        ObjectHideSubObjectPermanently( self, "HELM1", false )
    elseif helm <= 0.08 then
        ObjectHideSubObjectPermanently( self, "HELM2", false )
    elseif helm <= 0.12 then
        ObjectHideSubObjectPermanently( self, "HELM3", false )
    elseif helm <= 0.16 then
        ObjectHideSubObjectPermanently( self, "HELM4", false )
    elseif helm <= 0.18 then
        ObjectHideSubObjectPermanently( self, "HELM5", false )    
    elseif helm <= 0.22 then
        ObjectHideSubObjectPermanently( self, "HELM6", false )    
    elseif helm <= 0.25 then
        ObjectHideSubObjectPermanently( self, "HELM7", false )
    elseif helm <= 0.29 then
        ObjectHideSubObjectPermanently( self, "HELM8", false )        
    elseif helm <= 0.30 then
        ObjectHideSubObjectPermanently( self, "HELM9", false )
    elseif helm <= 0.36 then
        ObjectHideSubObjectPermanently( self, "HELM10", false )
    elseif helm <= 0.39 then
        ObjectHideSubObjectPermanently( self, "HELM11", false )
    elseif helm <= 0.40 then
        ObjectHideSubObjectPermanently( self, "HELM12", false )
    elseif helm <= 0.45 then
        ObjectHideSubObjectPermanently( self, "HELM13", false )
    elseif helm <= 0.50 then
        ObjectHideSubObjectPermanently( self, "HELM14", false )
    elseif helm <= 0.55 then
        ObjectHideSubObjectPermanently( self, "HELM15", false )
    elseif helm <= 0.60 then
        ObjectHideSubObjectPermanently( self, "HELM16", false )
    elseif helm <= 0.65 then
        ObjectHideSubObjectPermanently( self, "HELM17", false )
    elseif helm <= 0.70 then
        ObjectHideSubObjectPermanently( self, "HELM18", false )
    elseif helm <= 0.72 then
        ObjectHideSubObjectPermanently( self, "HELM19", false )
    elseif helm <= 0.76 then
        ObjectHideSubObjectPermanently( self, "HELM20", false )
    elseif helm <= 0.80 then
        ObjectHideSubObjectPermanently( self, "HELM21", false )
    elseif helm <= 0.85 then
        ObjectHideSubObjectPermanently( self, "HELM22", false )
    elseif helm <= 0.90 then
        ObjectHideSubObjectPermanently( self, "HELM23", false )
    elseif helm <= 0.92 then
        ObjectHideSubObjectPermanently( self, "HELM24", false )
    elseif helm <= 0.96 then
        ObjectHideSubObjectPermanently( self, "HELM25", false )
    else
        ObjectHideSubObjectPermanently( self, "HELM26", false )
    end
    --assign random shield
    if shield <= 0.063 then
        ObjectHideSubObjectPermanently( self, "SHIELD1", false )
    elseif shield <= 0.13 then
        ObjectHideSubObjectPermanently( self, "SHIELD2", false )
    elseif shield <= 0.19 then
        ObjectHideSubObjectPermanently( self, "SHIELD3", false )
    elseif shield <= 0.25 then
        ObjectHideSubObjectPermanently( self, "SHIELD4", false )
    elseif shield <= 0.31 then
        ObjectHideSubObjectPermanently( self, "SHIELD5", false )
    elseif shield <= 0.38 then
        ObjectHideSubObjectPermanently( self, "SHIELD6", false )
    elseif shield <= 0.44 then
        ObjectHideSubObjectPermanently( self, "SHIELD7", false )
    elseif shield <= 0.50 then
        ObjectHideSubObjectPermanently( self, "SHIELD8", false )
    else
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
    end
    -- assign random bow
    if bow <= 0.33 then
        ObjectHideSubObjectPermanently( self, "BOW1", false )
    elseif bow <= 0.66 then
        ObjectHideSubObjectPermanently( self, "BOW2", false )
    else
        ObjectHideSubObjectPermanently( self, "BOW3", false )
    end
    -- assign random body
    if body <= 0.25 then
        ObjectHideSubObjectPermanently( self, "ORC", false )
    elseif body <= 0.50 then
        ObjectHideSubObjectPermanently( self, "ORC1", false )
    elseif body <= 0.75 then
        ObjectHideSubObjectPermanently( self, "ORC2", false )
    else
        ObjectHideSubObjectPermanently( self, "ORC3", false )
    end
end

function OnMordorBannerCreated(self)
    ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
    -- helmet types
    ObjectHideSubObjectPermanently( self, "HELM1", true )
    ObjectHideSubObjectPermanently( self, "HELM2", true )
    ObjectHideSubObjectPermanently( self, "HELM3", true )
    ObjectHideSubObjectPermanently( self, "HELM4", true )
    ObjectHideSubObjectPermanently( self, "HELM5", true )
    ObjectHideSubObjectPermanently( self, "HELM6", true )
    ObjectHideSubObjectPermanently( self, "HELM7", true )
    ObjectHideSubObjectPermanently( self, "HELM8", true )
    ObjectHideSubObjectPermanently( self, "HELM9", true )
    ObjectHideSubObjectPermanently( self, "HELM10", true )
    ObjectHideSubObjectPermanently( self, "HELM11", true )
    ObjectHideSubObjectPermanently( self, "HELM12", true )
    ObjectHideSubObjectPermanently( self, "HELM13", true )
    ObjectHideSubObjectPermanently( self, "HELM14", true )
    ObjectHideSubObjectPermanently( self, "HELM15", true )
    ObjectHideSubObjectPermanently( self, "HELM16", true )
    ObjectHideSubObjectPermanently( self, "HELM17", true )
    ObjectHideSubObjectPermanently( self, "HELM18", true )
    ObjectHideSubObjectPermanently( self, "HELM19", true )
    ObjectHideSubObjectPermanently( self, "HELM20", true )    
    ObjectHideSubObjectPermanently( self, "HELM21", true )
    ObjectHideSubObjectPermanently( self, "HELM22", true )
    ObjectHideSubObjectPermanently( self, "HELM23", true )
    ObjectHideSubObjectPermanently( self, "HELM24", true )    
    -- weapon types
    ObjectHideSubObjectPermanently( self, "WEAP1", true )
    ObjectHideSubObjectPermanently( self, "WEAP2", true )
    ObjectHideSubObjectPermanently( self, "WEAP3", true )
    ObjectHideSubObjectPermanently( self, "WEAP4", true )
    ObjectHideSubObjectPermanently( self, "WEAP5", true )
    ObjectHideSubObjectPermanently( self, "WEAP6", true )
    
    -- assign random gear
    local helm = GetRandomNumber()
    local weapon = GetRandomNumber()    
    
    -- assign random helm
    if helm <= 0.04 then
        ObjectHideSubObjectPermanently( self, "HELM1", false )
    elseif helm <= 0.08 then
        ObjectHideSubObjectPermanently( self, "HELM2", false )
    elseif helm <= 0.12 then
        ObjectHideSubObjectPermanently( self, "HELM3", false )
    elseif helm <= 0.17 then
        ObjectHideSubObjectPermanently( self, "HELM4", false )
    elseif helm <= 0.21 then
        ObjectHideSubObjectPermanently( self, "HELM5", false )    
    elseif helm <= 0.25 then
        ObjectHideSubObjectPermanently( self, "HELM6", false )    
    elseif helm <= 0.29 then
        ObjectHideSubObjectPermanently( self, "HELM7", false )
    elseif helm <= 0.33 then
        ObjectHideSubObjectPermanently( self, "HELM8", false )        
    elseif helm <= 0.38 then
        ObjectHideSubObjectPermanently( self, "HELM9", false )
    elseif helm <= 0.42 then
        ObjectHideSubObjectPermanently( self, "HELM10", false )
    elseif helm <= 0.46 then
        ObjectHideSubObjectPermanently( self, "HELM11", false )
    elseif helm <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HELM12", false )
    elseif helm <= 0.54 then
        ObjectHideSubObjectPermanently( self, "HELM13", false )
    elseif helm <= 0.58 then
        ObjectHideSubObjectPermanently( self, "HELM14", false )
    elseif helm <= 0.63 then
        ObjectHideSubObjectPermanently( self, "HELM15", false )
    elseif helm <= 0.67 then
        ObjectHideSubObjectPermanently( self, "HELM16", false )
    elseif helm <= 0.71 then
        ObjectHideSubObjectPermanently( self, "HELM17", false )
    elseif helm <= 0.75 then
        ObjectHideSubObjectPermanently( self, "HELM18", false )
    elseif helm <= 0.79 then
        ObjectHideSubObjectPermanently( self, "HELM19", false )
    elseif helm <= 0.83 then
        ObjectHideSubObjectPermanently( self, "HELM20", false )
    elseif helm <= 0.88 then
        ObjectHideSubObjectPermanently( self, "HELM21", false )
    elseif helm <= 0.92 then
        ObjectHideSubObjectPermanently( self, "HELM22", false )
    elseif helm <= 0.96 then
        ObjectHideSubObjectPermanently( self, "HELM23", false )
    else
        ObjectHideSubObjectPermanently( self, "HELM24", false )
    end
    
    -- assign random weapon
    if weapon <= 0.17 then
        ObjectHideSubObjectPermanently( self, "WEAP1", false )
    elseif weapon <= 0.34 then
        ObjectHideSubObjectPermanently( self, "WEAP2", false )
    elseif weapon <= 0.51 then
        ObjectHideSubObjectPermanently( self, "WEAP3", false )
    elseif weapon <= 0.68 then
        ObjectHideSubObjectPermanently( self, "WEAP4", false )
    elseif weapon <= 0.85 then
        ObjectHideSubObjectPermanently( self, "WEAP5", false )
    else
        ObjectHideSubObjectPermanently( self, "WEAP6", false )
    end
end

function OnMordorFighter2Created(self)
    ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
    -- helmet types
    ObjectHideSubObjectPermanently( self, "HELM1", true )
    ObjectHideSubObjectPermanently( self, "HELM2", true )
    ObjectHideSubObjectPermanently( self, "HELM3", true )
    ObjectHideSubObjectPermanently( self, "HELM4", true )
    ObjectHideSubObjectPermanently( self, "HELM5", true )
    ObjectHideSubObjectPermanently( self, "HELM6", true )
    ObjectHideSubObjectPermanently( self, "HELM7", true )
    ObjectHideSubObjectPermanently( self, "HELM8", true )
    ObjectHideSubObjectPermanently( self, "HELM9", true )
    ObjectHideSubObjectPermanently( self, "HELM10", true )
    ObjectHideSubObjectPermanently( self, "HELM11", true )
    ObjectHideSubObjectPermanently( self, "HELM12", true )
    ObjectHideSubObjectPermanently( self, "HELM13", true )
    ObjectHideSubObjectPermanently( self, "HELM14", true )
    ObjectHideSubObjectPermanently( self, "HELM15", true )
    ObjectHideSubObjectPermanently( self, "HELM16", true )
    ObjectHideSubObjectPermanently( self, "HELM17", true )
    ObjectHideSubObjectPermanently( self, "HELM18", true )
    ObjectHideSubObjectPermanently( self, "HELM19", true )
    ObjectHideSubObjectPermanently( self, "HELM20", true )    
    ObjectHideSubObjectPermanently( self, "HELM21", true )
    ObjectHideSubObjectPermanently( self, "HELM22", true )
    ObjectHideSubObjectPermanently( self, "HELM23", true )
    ObjectHideSubObjectPermanently( self, "HELM24", true )    
    ObjectHideSubObjectPermanently( self, "HELM25", true )
    ObjectHideSubObjectPermanently( self, "HELM26", true )    
    -- weapon types
    ObjectHideSubObjectPermanently( self, "WEAP1", true )
    ObjectHideSubObjectPermanently( self, "WEAP2", true )
    ObjectHideSubObjectPermanently( self, "WEAP3", true )
    ObjectHideSubObjectPermanently( self, "WEAP4", true )
    ObjectHideSubObjectPermanently( self, "WEAP5", true )
    ObjectHideSubObjectPermanently( self, "WEAP6", true )
    -- shield types
    ObjectHideSubObjectPermanently( self, "SHIELD1", true )
    ObjectHideSubObjectPermanently( self, "SHIELD2", true )
    ObjectHideSubObjectPermanently( self, "SHIELD3", true )
    ObjectHideSubObjectPermanently( self, "SHIELD4", true )
    ObjectHideSubObjectPermanently( self, "SHIELD5", true )
    ObjectHideSubObjectPermanently( self, "SHIELD6", true )
    ObjectHideSubObjectPermanently( self, "SHIELD7", true )
    ObjectHideSubObjectPermanently( self, "SHIELD8", true )
    -- body types
    ObjectHideSubObjectPermanently( self, "ORC", true )
    ObjectHideSubObjectPermanently( self, "ORC1", true )
    ObjectHideSubObjectPermanently( self, "ORC2", true )
    ObjectHideSubObjectPermanently( self, "ORC3", true )
    -- spear types
    ObjectHideSubObjectPermanently( self, "SPR1", true )
    ObjectHideSubObjectPermanently( self, "SPR2", true )
    ObjectHideSubObjectPermanently( self, "SPR3", true )
    ObjectHideSubObjectPermanently( self, "SPR4", true )
    
    -- assign random gear
    local helm = GetRandomNumber()
    local weapon = GetRandomNumber()
    local shield = GetRandomNumber()    
    local body = GetRandomNumber()    
    local spear = GetRandomNumber()   
    
    -- assign random helm
    if helm <= 0.04 then
        ObjectHideSubObjectPermanently( self, "HELM1", false )
    elseif helm <= 0.08 then
        ObjectHideSubObjectPermanently( self, "HELM2", false )
    elseif helm <= 0.12 then
        ObjectHideSubObjectPermanently( self, "HELM3", false )
    elseif helm <= 0.16 then
        ObjectHideSubObjectPermanently( self, "HELM4", false )
    elseif helm <= 0.18 then
        ObjectHideSubObjectPermanently( self, "HELM5", false )    
    elseif helm <= 0.22 then
        ObjectHideSubObjectPermanently( self, "HELM6", false )    
    elseif helm <= 0.25 then
        ObjectHideSubObjectPermanently( self, "HELM7", false )
    elseif helm <= 0.29 then
        ObjectHideSubObjectPermanently( self, "HELM8", false )        
    elseif helm <= 0.30 then
        ObjectHideSubObjectPermanently( self, "HELM9", false )
    elseif helm <= 0.36 then
        ObjectHideSubObjectPermanently( self, "HELM10", false )
    elseif helm <= 0.39 then
        ObjectHideSubObjectPermanently( self, "HELM11", false )
    elseif helm <= 0.40 then
        ObjectHideSubObjectPermanently( self, "HELM12", false )
    elseif helm <= 0.45 then
        ObjectHideSubObjectPermanently( self, "HELM13", false )
    elseif helm <= 0.50 then
        ObjectHideSubObjectPermanently( self, "HELM14", false )
    elseif helm <= 0.55 then
        ObjectHideSubObjectPermanently( self, "HELM15", false )
    elseif helm <= 0.60 then
        ObjectHideSubObjectPermanently( self, "HELM16", false )
    elseif helm <= 0.65 then
        ObjectHideSubObjectPermanently( self, "HELM17", false )
    elseif helm <= 0.70 then
        ObjectHideSubObjectPermanently( self, "HELM18", false )
    elseif helm <= 0.72 then
        ObjectHideSubObjectPermanently( self, "HELM19", false )
    elseif helm <= 0.76 then
        ObjectHideSubObjectPermanently( self, "HELM20", false )
    elseif helm <= 0.80 then
        ObjectHideSubObjectPermanently( self, "HELM21", false )
    elseif helm <= 0.85 then
        ObjectHideSubObjectPermanently( self, "HELM22", false )
    elseif helm <= 0.90 then
        ObjectHideSubObjectPermanently( self, "HELM23", false )
    elseif helm <= 0.92 then
        ObjectHideSubObjectPermanently( self, "HELM24", false )
    elseif helm <= 0.96 then
        ObjectHideSubObjectPermanently( self, "HELM25", false )
    else
        ObjectHideSubObjectPermanently( self, "HELM26", false )
    end
    
    -- assign random weapon
    if weapon <= 0.17 then
        ObjectHideSubObjectPermanently( self, "WEAP1", false )
    elseif weapon <= 0.34 then
        ObjectHideSubObjectPermanently( self, "WEAP2", false )
    elseif weapon <= 0.51 then
        ObjectHideSubObjectPermanently( self, "WEAP3", false )
    elseif weapon <= 0.68 then
        ObjectHideSubObjectPermanently( self, "WEAP4", false )
    elseif weapon <= 0.85 then
        ObjectHideSubObjectPermanently( self, "WEAP5", false )
    else
        ObjectHideSubObjectPermanently( self, "WEAP6", false )
    end
    --assign random shield
    if shield <= 0.13 then
        ObjectHideSubObjectPermanently( self, "SHIELD1", false )
    elseif shield <= 0.25 then
        ObjectHideSubObjectPermanently( self, "SHIELD2", false )
    elseif shield <= 0.38 then
        ObjectHideSubObjectPermanently( self, "SHIELD3", false )
    elseif shield <= 0.50 then
        ObjectHideSubObjectPermanently( self, "SHIELD4", false )
    elseif shield <= 0.63 then
        ObjectHideSubObjectPermanently( self, "SHIELD5", false )
    elseif shield <= 0.75 then
        ObjectHideSubObjectPermanently( self, "SHIELD6", false )
    elseif shield <= 0.88 then
        ObjectHideSubObjectPermanently( self, "SHIELD7", false )
    else
        ObjectHideSubObjectPermanently( self, "SHIELD8", false )
    end

    -- assign random body
    if body <= 0.25 then
        ObjectHideSubObjectPermanently( self, "ORC", false )
    elseif body <= 0.50 then
        ObjectHideSubObjectPermanently( self, "ORC1", false )
    elseif body <= 0.75 then
        ObjectHideSubObjectPermanently( self, "ORC2", false )
    else
        ObjectHideSubObjectPermanently( self, "ORC3", false )
    end

    -- assign random spear
    if spear <= 0.25 then
        ObjectHideSubObjectPermanently( self, "SPR1", false )
    elseif spear <= 0.50 then
        ObjectHideSubObjectPermanently( self, "SPR2", false )
    elseif spear <= 0.75 then
        ObjectHideSubObjectPermanently( self, "SPR3", false )
    else
        ObjectHideSubObjectPermanently( self, "SPR4", false )
    end
end

function OnMordorFighter1Created(self)
    -- helmet types
    ObjectHideSubObjectPermanently( self, "HELM1", true )
    ObjectHideSubObjectPermanently( self, "HELM2", true )
    ObjectHideSubObjectPermanently( self, "HELM3", true )
    ObjectHideSubObjectPermanently( self, "HELM4", true )
    ObjectHideSubObjectPermanently( self, "HELM5", true )
    ObjectHideSubObjectPermanently( self, "HELM6", true )
    ObjectHideSubObjectPermanently( self, "HELM7", true )
    ObjectHideSubObjectPermanently( self, "HELM8", true )
    ObjectHideSubObjectPermanently( self, "HELM9", true )
    ObjectHideSubObjectPermanently( self, "HELM10", true )
    ObjectHideSubObjectPermanently( self, "HELM11", true )
    ObjectHideSubObjectPermanently( self, "HELM12", true )
    ObjectHideSubObjectPermanently( self, "HELM13", true )
    ObjectHideSubObjectPermanently( self, "HELM14", true )
    ObjectHideSubObjectPermanently( self, "HELM15", true )
    ObjectHideSubObjectPermanently( self, "HELM16", true )
    ObjectHideSubObjectPermanently( self, "HELM17", true )
    ObjectHideSubObjectPermanently( self, "HELM18", true )
    ObjectHideSubObjectPermanently( self, "HELM19", true )
    ObjectHideSubObjectPermanently( self, "HELM20", true )    
    ObjectHideSubObjectPermanently( self, "HELM21", true )
    ObjectHideSubObjectPermanently( self, "HELM22", true )
    ObjectHideSubObjectPermanently( self, "HELM23", true )
    ObjectHideSubObjectPermanently( self, "HELM24", true ) 
    ObjectHideSubObjectPermanently( self, "HELM25", true )
    ObjectHideSubObjectPermanently( self, "HELM26", true )    
    -- weapon types
    ObjectHideSubObjectPermanently( self, "WEAP1", true )
    ObjectHideSubObjectPermanently( self, "WEAP2", true )
    ObjectHideSubObjectPermanently( self, "WEAP3", true )
    ObjectHideSubObjectPermanently( self, "WEAP4", true )
    ObjectHideSubObjectPermanently( self, "WEAP5", true )
    ObjectHideSubObjectPermanently( self, "WEAP6", true )
    ObjectHideSubObjectPermanently( self, "WEAP7", true )
    ObjectHideSubObjectPermanently( self, "WEAP8", true )
    ObjectHideSubObjectPermanently( self, "WEAP9", true )
    ObjectHideSubObjectPermanently( self, "WEAP10", true )
    ObjectHideSubObjectPermanently( self, "WEAP11", true )
    ObjectHideSubObjectPermanently( self, "WEAP12", true )
    -- body types
    ObjectHideSubObjectPermanently( self, "ORC", true )
    ObjectHideSubObjectPermanently( self, "ORC1", true )
    ObjectHideSubObjectPermanently( self, "ORC2", true )
    ObjectHideSubObjectPermanently( self, "ORC3", true )
    
    -- assign random gear
    local helm = GetRandomNumber()
    local leftweapon = GetRandomNumber()
    local rightweapon = GetRandomNumber()
    local body = GetRandomNumber() 
    
    
    -- assign random helm
    if helm <= 0.04 then
        ObjectHideSubObjectPermanently( self, "HELM1", false )
    elseif helm <= 0.08 then
        ObjectHideSubObjectPermanently( self, "HELM2", false )
    elseif helm <= 0.12 then
        ObjectHideSubObjectPermanently( self, "HELM3", false )
    elseif helm <= 0.16 then
        ObjectHideSubObjectPermanently( self, "HELM4", false )
    elseif helm <= 0.18 then
        ObjectHideSubObjectPermanently( self, "HELM5", false )    
    elseif helm <= 0.22 then
        ObjectHideSubObjectPermanently( self, "HELM6", false )    
    elseif helm <= 0.25 then
        ObjectHideSubObjectPermanently( self, "HELM7", false )
    elseif helm <= 0.29 then
        ObjectHideSubObjectPermanently( self, "HELM8", false )        
    elseif helm <= 0.30 then
        ObjectHideSubObjectPermanently( self, "HELM9", false )
    elseif helm <= 0.36 then
        ObjectHideSubObjectPermanently( self, "HELM10", false )
    elseif helm <= 0.39 then
        ObjectHideSubObjectPermanently( self, "HELM11", false )
    elseif helm <= 0.40 then
        ObjectHideSubObjectPermanently( self, "HELM12", false )
    elseif helm <= 0.45 then
        ObjectHideSubObjectPermanently( self, "HELM13", false )
    elseif helm <= 0.50 then
        ObjectHideSubObjectPermanently( self, "HELM14", false )
    elseif helm <= 0.55 then
        ObjectHideSubObjectPermanently( self, "HELM15", false )
    elseif helm <= 0.60 then
        ObjectHideSubObjectPermanently( self, "HELM16", false )
    elseif helm <= 0.65 then
        ObjectHideSubObjectPermanently( self, "HELM17", false )
    elseif helm <= 0.70 then
        ObjectHideSubObjectPermanently( self, "HELM18", false )
    elseif helm <= 0.72 then
        ObjectHideSubObjectPermanently( self, "HELM19", false )
    elseif helm <= 0.76 then
        ObjectHideSubObjectPermanently( self, "HELM20", false )
    elseif helm <= 0.80 then
        ObjectHideSubObjectPermanently( self, "HELM21", false )
    elseif helm <= 0.85 then
        ObjectHideSubObjectPermanently( self, "HELM22", false )
    elseif helm <= 0.90 then
        ObjectHideSubObjectPermanently( self, "HELM23", false )
    elseif helm <= 0.92 then
        ObjectHideSubObjectPermanently( self, "HELM24", false )
    elseif helm <= 0.96 then
        ObjectHideSubObjectPermanently( self, "HELM25", false )
    else
        ObjectHideSubObjectPermanently( self, "HELM26", false )
    end
    
    -- assign random weapon right hand
    if rightweapon <= 0.1 then
        ObjectHideSubObjectPermanently( self, "WEAP1", false )
    elseif rightweapon <= 0.17 then
        ObjectHideSubObjectPermanently( self, "WEAP2", false )
    elseif rightweapon <= 0.25 then
        ObjectHideSubObjectPermanently( self, "WEAP3", false )
    elseif rightweapon <= 0.33 then
        ObjectHideSubObjectPermanently( self, "WEAP4", false )
    elseif rightweapon <= 0.42 then
        ObjectHideSubObjectPermanently( self, "WEAP5", false )
    else
        ObjectHideSubObjectPermanently( self, "WEAP6", false )
    end
    
        -- assign random weapon left hand
    if leftweapon <= 0.1 then
        ObjectHideSubObjectPermanently( self, "WEAP7", false )
    elseif leftweapon <= 0.17 then
        ObjectHideSubObjectPermanently( self, "WEAP8", false )
    elseif leftweapon <= 0.25 then
        ObjectHideSubObjectPermanently( self, "WEAP9", false )
    elseif leftweapon <= 0.33 then
        ObjectHideSubObjectPermanently( self, "WEAP10", false )
    elseif leftweapon <= 0.42 then
        ObjectHideSubObjectPermanently( self, "WEAP11", false )
    elseif leftweapon <= 0.55 then
        ObjectHideSubObjectPermanently( self, "WEAP12", false )
    else
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
    end

    -- assign random body
    if body <= 0.25 then
        ObjectHideSubObjectPermanently( self, "ORC", false )
    elseif body <= 0.50 then
        ObjectHideSubObjectPermanently( self, "ORC1", false )
    elseif body <= 0.75 then
        ObjectHideSubObjectPermanently( self, "ORC2", false )
    else
        ObjectHideSubObjectPermanently( self, "ORC3", false )
    end
end

-- PEASANT FUNCTIONS :D

-- peasant 1: polearms
function OnRohanPeasant1Created(self)
    --Hide Objects code
    --Hide FB
    ObjectHideSubObjectPermanently( self, "FORGED_SPEAR", true )
    --Hide Farm Weapons
    ObjectHideSubObjectPermanently( self, "SCYTHE", true )
    ObjectHideSubObjectPermanently( self, "PITCHFORK", true )
    --Hide Helmets
    ObjectHideSubObjectPermanently( self, "HELMET01", true )
    ObjectHideSubObjectPermanently( self, "HELMET02", true )
    ObjectHideSubObjectPermanently( self, "HELMET03", true )
    
    local helmet    =    GetRandomNumber()
    
    if helmet <= 0.25 then
        ObjectHideSubObjectPermanently( self, "HELMET01", false )
    elseif helmet <= 0.60 then
        ObjectHideSubObjectPermanently( self, "HELMET02", false )
    else
        ObjectHideSubObjectPermanently( self, "HELMET03", false )
    end
    -- Farm Weapon Random
    local weapon    =    GetRandomNumber()
    
    if weapon <= 0.50 then
        ObjectHideSubObjectPermanently( self, "SCYTHE", false )
    else
        ObjectHideSubObjectPermanently( self, "PITCHFORK", false )
    end
    
end

-- peasant 2: swords

function OnRohanPeasant2Created(self)
    --Hide Objects code
    --Hide FB
    ObjectHideSubObjectPermanently( self, "FORGED_SWORD", true )
    ObjectHideSubObjectPermanently( self, "FORGED_AXE", true )
    --Hide Farm Weapons
    ObjectHideSubObjectPermanently( self, "AXE", true )
    --Hide Hats
    ObjectHideSubObjectPermanently( self, "HAT", true )
    ObjectHideSubObjectPermanently( self, "DHAT", true )
    --Hide Drafted Headgear
    ObjectHideSubObjectPermanently( self, "DHAT", true )
    ObjectHideSubObjectPermanently( self, "DHELMET", true )
    --Hide Drafted Weapons
    ObjectHideSubObjectPermanently( self, "DAXE", true )
    --Hide Shield
    ObjectHideSubObjectPermanently( self, "SHIELDBACK", true )
    ObjectHideSubObjectPermanently( self, "SHIELD", true )
    
    -- Hat or no hat
    local hat    =    GetRandomNumber()
    
    if hat <= 0.75 then
        ObjectHideSubObjectPermanently( self, "HAT", false )
    else
        -- no hat
    end
    
    -- shield or no shield
    local shield    =    GetRandomNumber()
    
    if shield <= 0.85 then
        ObjectHideSubObjectPermanently( self, "SHIELD", false )
    else
        -- no shield
    end
    
    -- Farm Weapon Random
    local weapon    =    GetRandomNumber()
    
    if weapon <= 0.80 then
        ObjectHideSubObjectPermanently( self, "SICKLE", false )
    else
    -- bare hands
    end
    
    -- Hat or helmet for kid
    
    local head    =    GetRandomNumber()
    
    if head <= 0.75 then
        ObjectHideSubObjectPermanently( self, "DHELMET", false )
    else
        ObjectHideSubObjectPermanently( self, "DHAT", false )
    end
    
end

-- peasant 3: axes

function OnRohanPeasant3Created(self)
    --Hide Objects code
    --Hide FB
    ObjectHideSubObjectPermanently( self, "FORGED_SWORD", true )
    ObjectHideSubObjectPermanently( self, "FORGED_AXE", true )
    --Hide Farm Weapons
    ObjectHideSubObjectPermanently( self, "SICKLE", true )
    --Hide Hats
    ObjectHideSubObjectPermanently( self, "HAT", true )
    ObjectHideSubObjectPermanently( self, "DHAT", true )
    --Hide Drafted Headgear
    ObjectHideSubObjectPermanently( self, "DHAT", true )
    ObjectHideSubObjectPermanently( self, "DHELMET", true )
    --Hide Drafted Weapons
    ObjectHideSubObjectPermanently( self, "DSWORD", true )
    --Hide Shield
    ObjectHideSubObjectPermanently( self, "SHIELD", true )
    
    -- Hat or no hat
    local hat    =    GetRandomNumber()
    
    if hat <= 0.75 then
        ObjectHideSubObjectPermanently( self, "HAT", false )
    else
        -- no hat
    end
    
    -- Farm Weapon Random
    local weapon    =    GetRandomNumber()
    
    if weapon <= 0.80 then
        ObjectHideSubObjectPermanently( self, "AXE", false )
    else
    -- bare hands
    end
    
    -- Hat or helmet for drafted kid
    
    local head    =    GetRandomNumber()
    
    if head <= 0.75 then
        ObjectHideSubObjectPermanently( self, "DHELMET", false )
    else
        ObjectHideSubObjectPermanently( self, "DHAT", false )
    end
    
end

function OnMordorFighterCreated(self)
	ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
	ObjectHideSubObjectPermanently( self, "FORGED_BLADES", true )
	ObjectHideSubObjectPermanently( self, "HELM", true )

    local helmet         =    GetRandomNumber()

    if helmet <= 0.333 then
        ObjectHideSubObjectPermanently( self, "HELM", false )
     else
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
    end   

end

function MordorFighterBecomeUncontrollablyAfraid(self,other)
	local wasAfraid = ObjectTestModelCondition(self, "EMOTION_AFRAID")

	BecomeUncontrollablyAfraid(self,other)                 -- Call base function appropriate to many unit types
	
	-- Play unit-specific sound, but only when first entering state (not every time troll sends out fear message!)
	-- BecomeAfraidOfTroll may fail, don't play sound if we didn't enter fear state
		if ( not wasAfraid ) and ObjectTestModelCondition(self, "EMOTION_AFRAID") then
		ObjectPlaySound(self, "MordorFighterEntFear") 
	end
end

function MordorFighterBecomeAfraidOfPhial(self,other)
	local wasAfraid = ObjectTestModelCondition(self, "EMOTION_AFRAID")

	BecomeUncontrollablyAfraid(self,other)
	-- BecomeAfraidOfTroll(self,other)                 -- Call base function appropriate to many unit types
	
	-- Play unit-specific sound, but only when first entering state (not every time troll sends out fear message!)
	-- BecomeAfraidOfTroll may fail, don't play sound if we didn't enter fear state
--		if ( not wasAfraid ) and ObjectTestModelCondition(self, "EMOTION_AFRAID") then
--			ObjectPlaySound(self, "MordorFighterEntFear") 
--		end
end

function OnMordorCorsairCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Forged_Blade01", true )
end

function WildInfantryBecomeAfraidOfPhial(self,other)
	local wasAfraid = ObjectTestModelCondition(self, "EMOTION_AFRAID")
	BecomeUncontrollablyAfraid(self,other)
end


function ShelobBecomeAfraidOfPhial(self,other)
	local wasAfraid = ObjectTestModelCondition(self, "EMOTION_AFRAID")

	BecomeUncontrollablyAfraid(self,other)
	-- BecomeAfraidOfTroll(self,other)                 -- Call base function appropriate to many unit types
	
	-- Play unit-specific sound, but only when first entering state (not every time troll sends out fear message!)
	-- BecomeAfraidOfTroll may fail, don't play sound if we didn't enter fear state
--		if ( not wasAfraid ) and ObjectTestModelCondition(self, "EMOTION_AFRAID") then
--			ObjectPlaySound(self, "MordorFighterEntFear") 
--		end
end

function OnInfantryBannerCreated(self)
	ObjectHideSubObjectPermanently( self, "Glow", true )
end

function OnCavalryCreated(self)
	ObjectHideSubObjectPermanently( self, "Glow", true )
end

function OnGondorKingsguardCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Hammer1", true )
	ObjectHideSubObjectPermanently( self, "Glow", true )
	ObjectHideSubObjectPermanently( self, "Glow1", true )
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
    ObjectHideSubObjectPermanently( self, "BANNER01", true )
    ObjectHideSubObjectPermanently( self, "BANNER02", true )
    ObjectHideSubObjectPermanently( self, "BANNER03", true )
	local banner          =    GetRandomNumber()
	local head          =    GetRandomNumber()
    if head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    else
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
    end 

    if banner <= 0.4 then
        ObjectHideSubObjectPermanently( self, "BANNER01", false )
    elseif banner <= 0.8 then
        ObjectHideSubObjectPermanently( self, "BANNER02", false )
	else
        ObjectHideSubObjectPermanently( self, "BANNER03", false )
    end 

end

function OnGondorFighterCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Hammer1", true )
	ObjectHideSubObjectPermanently( self, "Glow", true )
	ObjectHideSubObjectPermanently( self, "Glow1", true )
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
    ObjectHideSubObjectPermanently( self, "BANNER01", true )
    ObjectHideSubObjectPermanently( self, "BANNER02", true )
    ObjectHideSubObjectPermanently( self, "BANNER03", true )
	local banner          =    GetRandomNumber()
	local head          =    GetRandomNumber()
    if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
	else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end 

    if banner <= 0.4 then
        ObjectHideSubObjectPermanently( self, "BANNER01", false )
    elseif banner <= 0.8 then
        ObjectHideSubObjectPermanently( self, "BANNER02", false )
	else
        ObjectHideSubObjectPermanently( self, "BANNER03", false )
    end 

end

function OnHobbitBounderCreated(self)
	ObjectHideSubObjectPermanently( self, "BAG01", true )
	ObjectHideSubObjectPermanently( self, "HAT01", true )
	ObjectHideSubObjectPermanently( self, "HAT02", true )
	ObjectHideSubObjectPermanently( self, "HAT03", true )
	ObjectHideSubObjectPermanently( self, "HAT04", true )
	ObjectHideSubObjectPermanently( self, "BODY01", true )
	ObjectHideSubObjectPermanently( self, "BODY02", true )
	ObjectHideSubObjectPermanently( self, "BODY03", true )
	ObjectHideSubObjectPermanently( self, "BODY04", true )
	ObjectHideSubObjectPermanently( self, "BODY05", true )
	ObjectHideSubObjectPermanently( self, "PIPE", true )
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
    ObjectHideSubObjectPermanently( self, "FOOT01", true )
    ObjectHideSubObjectPermanently( self, "FOOT02", true )
    ObjectHideSubObjectPermanently( self, "FOOT03", true )
	
	local head          =    GetRandomNumber()
	local bag          =    GetRandomNumber()
	local hat          =    GetRandomNumber()
	local body          =    GetRandomNumber()
	local pipe          =    GetRandomNumber()
	local feet          =    GetRandomNumber()
	
    if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
	else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end 
	
    if pipe <= 0.5 then
        ObjectHideSubObjectPermanently( self, "PIPE", false )
	else
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
    end 

    if bag <= 0.5 then
        ObjectHideSubObjectPermanently( self, "BAG01", false )
	else
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
    end 
	
    if hat <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HAT01", false )
    elseif hat <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HAT02", false )
    elseif hat <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HAT03", false )
	else
        ObjectHideSubObjectPermanently( self, "HAT04", false )
    end 
	
    if body <= 0.2 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
    elseif body <= 0.4 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
    elseif body <= 0.6 then
        ObjectHideSubObjectPermanently( self, "BODY03", false )
    elseif body <= 0.8 then
        ObjectHideSubObjectPermanently( self, "BODY04", false )
	else
        ObjectHideSubObjectPermanently( self, "BODY05", false )
    end 
	
    if feet <= 0.3 then
        ObjectHideSubObjectPermanently( self, "FOOT01", false )
    elseif feet <= 0.7 then
        ObjectHideSubObjectPermanently( self, "FOOT02", false )
	else
        ObjectHideSubObjectPermanently( self, "FOOT03", false )
    end 

end

function OnDancingHobbitCreated(self)
	ObjectHideSubObjectPermanently( self, "HAT01", true )
	ObjectHideSubObjectPermanently( self, "HAT02", true )
	ObjectHideSubObjectPermanently( self, "HAT03", true )
	ObjectHideSubObjectPermanently( self, "BODY01", true )
	ObjectHideSubObjectPermanently( self, "BODY02", true )
	ObjectHideSubObjectPermanently( self, "BODY03", true )
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
    ObjectHideSubObjectPermanently( self, "FOOT01", true )
    ObjectHideSubObjectPermanently( self, "FOOT02", true )
    ObjectHideSubObjectPermanently( self, "FOOT03", true )
	
	local head          =    GetRandomNumber()
	local hat          =    GetRandomNumber()
	local body          =    GetRandomNumber()
	local feet          =    GetRandomNumber()
	
    if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
	else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end 
	
    if hat <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HAT01", false )
    elseif hat <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HAT02", false )
	else
        ObjectHideSubObjectPermanently( self, "HAT03", false )
    end 
	
    if body <= 0.3 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
    elseif body <= 0.7 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
	else
        ObjectHideSubObjectPermanently( self, "BODY03", false )
    end 
	
    if feet <= 0.3 then
        ObjectHideSubObjectPermanently( self, "FOOT01", false )
    elseif feet <= 0.7 then
        ObjectHideSubObjectPermanently( self, "FOOT02", false )
	else
        ObjectHideSubObjectPermanently( self, "FOOT03", false )
    end 

end

function OnAragornCreated(self)
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl2" )
	ObjectHideSubObjectPermanently( self, "PLANE02", true )
end

function OnThorinCreated(self)
	ObjectHideSubObjectPermanently( self, "ORCRIST01", true )
	ObjectHideSubObjectPermanently( self, "SHEATH", true )
end

function OnAzogCreated(self)
	ObjectHideSubObjectPermanently( self, "PLANE02", true )
	ObjectHideSubObjectPermanently( self, "WEAPS", true)
	ObjectHideSubObjectPermanently( self, "ARMOR", true )
    ObjectHideSubObjectPermanently( self, "AZOGHA", true )
end

function OnBlackrootArcherCreated(self)
	-- ObjectHideSubObjectPermanently( self, "arrow", true )		-- This gets hidden pending the art being fixed.  it is the pre-new-archer-firing-pattern arrow
	ObjectHideSubObjectPermanently( self, "FireArowTip", true ) -- This gets hidden because the Fire Arrow upgrade turns it on.
	ObjectHideSubObjectPermanently( self, "ARMOR", true)
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
	ObjectHideSubObjectPermanently( self, "BODY01", true )
    ObjectHideSubObjectPermanently( self, "BODY02", true )
    ObjectHideSubObjectPermanently( self, "COIF", true )	
	
	local body          =    GetRandomNumber()
	local coif          =    GetRandomNumber()	
	local head          =    GetRandomNumber()

    if body <= 0.5 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY02", false )
    end

    if coif <= 0.5 then
        ObjectHideSubObjectPermanently( self, "COIF", false )
    else
		-- nothing
    end 	
	
    if head <= 0.35 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.65 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
	else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end  
end

function OnDainCreated(self)
	-- ObjectHideSubObjectPermanently( self, "arrow", true )		-- This gets hidden pending the art being fixed.  it is the pre-new-archer-firing-pattern arrow
	ObjectHideSubObjectPermanently( self, "PAULDRONS", true ) -- This gets hidden because the Fire Arrow upgrade turns it on.
	ObjectHideSubObjectPermanently( self, "CLOAK", true)
	local shield          =    GetRandomNumber()	
    if shield <= 0.5 then
        ObjectHideSubObjectPermanently( self, "PAULDRONS", false )
     else
        ObjectHideSubObjectPermanently( self, "CLOAK", false )
	end 
end

function OnGondorDolMaArmsCreated(self)
	-- ObjectHideSubObjectPermanently( self, "arrow", true )		-- This gets hidden pending the art being fixed.  it is the pre-new-archer-firing-pattern arrow
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FireArowTip", true ) -- This gets hidden because the Fire Arrow upgrade turns it on.
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
    ObjectHideSubObjectPermanently( self, "HELMETWINGA", true )
    ObjectHideSubObjectPermanently( self, "HELMETWINGB", true )
    ObjectHideSubObjectPermanently( self, "SASH01", true )
    ObjectHideSubObjectPermanently( self, "SASH02", true )
	local head          =    GetRandomNumber()
	local helmet          =    GetRandomNumber()
	local sash          =    GetRandomNumber()

    if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
	else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end  

    if sash <= 0.5 then
        ObjectHideSubObjectPermanently( self, "SASH01", false )
	else
        ObjectHideSubObjectPermanently( self, "SASH02", false )
    end  	

    if helmet <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HELMETWINGA", false )
	else
        ObjectHideSubObjectPermanently( self, "HELMETWINGB", false )
    end  	
end

function OnGondorArcherCreated(self)
	-- ObjectHideSubObjectPermanently( self, "arrow", true )		-- This gets hidden pending the art being fixed.  it is the pre-new-archer-firing-pattern arrow
	ObjectHideSubObjectPermanently( self, "FireArowTip", true ) -- This gets hidden because the Fire Arrow upgrade turns it on.
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
	local head          =    GetRandomNumber()
    if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
	else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end  	
end

function DragonStrikeDragonCreated(self)
	ObjectForbidPlayerCommands( self, true )
end

function OnLegolasCreated(self)
	-- ObjectHideSubObjectPermanently( self, "arrow02", true )		-- This gets hidden pending the art being fixed.  it is the pre-new-archer-firing-pattern arrow
	-- ObjectHideSubObjectPermanently( self, "arrow", true )		-- This gets hidden pending the art being fixed.  it is the pre-new-archer-firing-pattern arrow
end

function OnRohanArcherCreated(self)
	ObjectHideSubObjectPermanently( self, "FireArowTip", true ) -- yes, it's a typo in the art.
	-- ObjectHideSubObjectPermanently( self, "ArrowNock", true )
	-- ObjectHideSubObjectPermanently( self, "arrow", true )
end

function OnRohanWestfolderCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "HA", true )
end

function GondorFighterBecomeAfraid(self, other)
	local wasAfraid = ObjectTestModelCondition(self, "EMOTION_AFRAID")

	-- An object has a 100% chance to become afraid.
	-- An object has a 66% chance to be feared, 33% chance to run away.
	if GetRandomNumber() <= 0.67 then 
		ObjectEnterFearState(self, other, false) -- become afraid of other.
	else --if GetRandomNumber() > 0.67 then
		ObjectEnterRunAwayPanicState(self, other) -- run away.

	end
	
	if ( not wasAfraid ) and ObjectTestModelCondition(self, "EMOTION_AFRAID") then
		ObjectPlaySound(self, "GondorSoldierScream")	
	end
	
end


function GondorFighterBecomeAfraidOfGateDamaged(self, other)
	local wasAfraid = ObjectTestModelCondition(self, "EMOTION_AFRAID")

	BecomeAfraidOfGateDamaged(self,other)                 -- Call base function appropriate to many unit types
	
	-- Play unit-specific sound, but only when first entering state (not every time troll sends out fear message!)
	-- BecomeAfraidOfGateDamaged may fail, don't play sound if we didn't enter fear state
	
	if ( not wasAfraid ) and ObjectTestModelCondition(self, "EMOTION_AFRAID") then
		ObjectPlaySound(self, "GondorSoldierScream")	
	end
end

function GondorFighterRecoverFromTerror(self)
	-- Add recovery sound
	ObjectPlaySound(self, "GondorSoldierRecoverFromTerror")
end

function SpyMoving(self, other)
	print(ObjectDescription(self).." spying movement of "..ObjectDescription(other));
end

--function GandalfConsiderUsingDefensePower(self, other, delay, amount)
--	-- Put up the shield if a big attack is coming and we have time to block it
--	if tonumber(delay) > 1 then
--		if tonumber(amount) >= 100 then
--			ObjectDoSpecialPower(self, "SpecialPowerShieldBubble")
--			return
--		end
--	end
--	
--	-- Or, if we are being hit and there are alot of guys arround, do our cool pushback power
--	if tonumber(ObjectCountNearbyEnemies(self, 50)) >= 4 then
--		ObjectDoSpecialPower(self, "SpecialPowerTelekeneticPush")
--		return
--	end
--end

-- Added for 2.0 from DC patch ;;,;;
function GandalfUseDefensePower(self, other, delay, amount)
  ObjectDoSpecialPower(self, "SpecialPowerShieldBubble")
end

function GandalfTriggerWizardBlast(self)
	ObjectCreateAndFireTempWeapon(self, "GandalfWizardBlast")
end

--function SarumanConsiderUsingDefensePower(self, other, delay, amount)
--	-- Put up the shield if a big attack is coming and we have time to block it
--E4	if tonumber(delay) > 1 then
--E4		if tonumber(amount) >= 25 then
--E4			ObjectDoSpecialPower(self, "SpecialPowerShieldBubble")
--E4			return
--E4		end
--E4	end
--	
--	-- Or, if we are being hit and there are alot of guys arround, do our cool pushback power
--	if tonumber(ObjectCountNearbyEnemies(self, 50)) >= 4 then
--		ObjectDoSpecialPower(self, "SpecialPowerTelekeneticPush")
--		return
--	end
--end

function BalrogTriggerBreatheFire(self)
	ObjectCreateAndFireTempWeapon(self, "MordorBalrogBreath")
end

function OnNewRohirrimCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_BladeA", true )
	ObjectHideSubObjectPermanently( self, "Forged_BladeB", true )
	ObjectHideSubObjectPermanently( self, "Forged_BladeC", true )
	ObjectHideSubObjectPermanently( self, "SHIELD01", true )
	ObjectHideSubObjectPermanently( self, "SHIELD02", true )
	ObjectHideSubObjectPermanently( self, "SHIELD03", true )
	ObjectHideSubObjectPermanently( self, "SHIELD04", true )
	ObjectHideSubObjectPermanently( self, "SHIELD05", true )
	ObjectHideSubObjectPermanently( self, "SHIELD06", true )
	ObjectHideSubObjectPermanently( self, "SHIELD07", true )
	ObjectHideSubObjectPermanently( self, "FireArowTip", true )
    ObjectHideSubObjectPermanently( self, "HORSE01", true )
    ObjectHideSubObjectPermanently( self, "HORSE02", true )
    ObjectHideSubObjectPermanently( self, "HORSE03", true )
    ObjectHideSubObjectPermanently( self, "HORSE04", true )	
	ObjectHideSubObjectPermanently( self, "BODY01", true )
	ObjectHideSubObjectPermanently( self, "BODY02", true )
	ObjectHideSubObjectPermanently( self, "BODY03", true )
	ObjectHideSubObjectPermanently( self, "CLOAK01", true )
	ObjectHideSubObjectPermanently( self, "CLOAK02", true )
	ObjectHideSubObjectPermanently( self, "CLOAK03", true )
	ObjectHideSubObjectPermanently( self, "COIF", true )
	ObjectHideSubObjectPermanently( self, "HELM01", true )
	ObjectHideSubObjectPermanently( self, "HELM02", true )
	ObjectHideSubObjectPermanently( self, "HELM03", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM01", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM02", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM03", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM04", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM05", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM06", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM07", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM08", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM09", true )
	ObjectHideSubObjectPermanently( self, "HELMHEAD01", true )
	ObjectHideSubObjectPermanently( self, "HELMHEAD02", true )
	ObjectHideSubObjectPermanently( self, "NOHELM01", true )
	ObjectHideSubObjectPermanently( self, "NOHELM02", true )
	ObjectHideSubObjectPermanently( self, "LEGS01", true )
	ObjectHideSubObjectPermanently( self, "LEGS02", true )
	ObjectHideSubObjectPermanently( self, "LEGS03", true )

    local horse         =    GetRandomNumber()
    local shield 		=   GetRandomNumber()
    local head 		=   GetRandomNumber()
    local bodies 		=   GetRandomNumber()
    local cloaks 		=   GetRandomNumber()
    local legs 		=   GetRandomNumber()
	
    if bodies <= 0.4 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
    elseif bodies <= 0.8 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY03", false )
    end
	
    if cloaks <= 0.4 then
        ObjectHideSubObjectPermanently( self, "CLOAK01", false )
    elseif cloaks <= 0.8 then
        ObjectHideSubObjectPermanently( self, "CLOAK03", false )
     else
        ObjectHideSubObjectPermanently( self, "CLOAK02", false )
    end
	
    if legs <= 0.4 then
        ObjectHideSubObjectPermanently( self, "LEGS01", false )
	elseif legs <= 0.8 then
        ObjectHideSubObjectPermanently( self, "LEGS02", false )
     else
        ObjectHideSubObjectPermanently( self, "LEGS03", false )
    end	

    if horse <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HORSE01", false )
    elseif horse <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HORSE02", false )
    elseif horse <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HORSE03", false )
     else
        ObjectHideSubObjectPermanently( self, "HORSE04", false )
    end 
	
    if shield <= 0.1 then
        ObjectHideSubObjectPermanently( self, "SHIELD01", false )
    elseif shield <= 0.3 then
        ObjectHideSubObjectPermanently( self, "SHIELD02", false )
    elseif shield <= 0.4 then
        ObjectHideSubObjectPermanently( self, "SHIELD03", false )
    elseif shield <= 0.5 then
        ObjectHideSubObjectPermanently( self, "SHIELD04", false )
    elseif shield <= 0.7 then
        ObjectHideSubObjectPermanently( self, "SHIELD05", false )
    elseif shield <= 0.9 then
        ObjectHideSubObjectPermanently( self, "SHIELD06", false )
     else
        ObjectHideSubObjectPermanently( self, "SHIELD07", false )
    end
	
    if head <= 0.10 then
        ObjectHideSubObjectPermanently( self, "NOHELM01", false )
    elseif head <= 0.15 then
        ObjectHideSubObjectPermanently( self, "NOHELM02", false )
    elseif head <= 0.20 then
        ObjectHideSubObjectPermanently( self, "COIF", false )
    elseif head <= 0.25 then
        ObjectHideSubObjectPermanently( self, "NEWHELM01", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.30 then
        ObjectHideSubObjectPermanently( self, "NEWHELM02", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.35 then
        ObjectHideSubObjectPermanently( self, "NEWHELM03", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.40 then
        ObjectHideSubObjectPermanently( self, "NEWHELM04", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.45 then
        ObjectHideSubObjectPermanently( self, "NEWHELM05", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.50 then
        ObjectHideSubObjectPermanently( self, "NEWHELM06", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.55 then
        ObjectHideSubObjectPermanently( self, "NEWHELM07", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.58 then
        ObjectHideSubObjectPermanently( self, "NEWHELM08", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.60 then
        ObjectHideSubObjectPermanently( self, "NEWHELM09", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.65 then
        ObjectHideSubObjectPermanently( self, "NEWHELM01", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.70 then
        ObjectHideSubObjectPermanently( self, "NEWHELM02", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.75 then
        ObjectHideSubObjectPermanently( self, "NEWHELM04", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.80 then
        ObjectHideSubObjectPermanently( self, "NEWHELM05", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.85 then
        ObjectHideSubObjectPermanently( self, "NEWHELM06", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.90 then
        ObjectHideSubObjectPermanently( self, "NEWHELM07", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.95 then
        ObjectHideSubObjectPermanently( self, "NEWHELM08", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.99 then
        ObjectHideSubObjectPermanently( self, "NEWHELM09", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
	else
        ObjectHideSubObjectPermanently( self, "NEWHELM03", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    end
end

function OnRohirrimBannerCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_BladeA", true )
	ObjectHideSubObjectPermanently( self, "Forged_BladeB", true )
	ObjectHideSubObjectPermanently( self, "Forged_BladeC", true )
	ObjectHideSubObjectPermanently( self, "SHIELD01", true )
	ObjectHideSubObjectPermanently( self, "SHIELD02", true )
	ObjectHideSubObjectPermanently( self, "SHIELD03", true )
	ObjectHideSubObjectPermanently( self, "SHIELD04", true )
	ObjectHideSubObjectPermanently( self, "SHIELD05", true )
	ObjectHideSubObjectPermanently( self, "SHIELD06", true )
	ObjectHideSubObjectPermanently( self, "SHIELD07", true )
	ObjectHideSubObjectPermanently( self, "FireArowTip", true )
    ObjectHideSubObjectPermanently( self, "HORSE01", true )
    ObjectHideSubObjectPermanently( self, "HORSE02", true )
    ObjectHideSubObjectPermanently( self, "HORSE03", true )
    ObjectHideSubObjectPermanently( self, "HORSE04", true )	
	ObjectHideSubObjectPermanently( self, "BODY01", true )
	ObjectHideSubObjectPermanently( self, "BODY02", true )
	ObjectHideSubObjectPermanently( self, "BODY03", true )
	ObjectHideSubObjectPermanently( self, "CLOAK01", true )
	ObjectHideSubObjectPermanently( self, "CLOAK02", true )
	ObjectHideSubObjectPermanently( self, "CLOAK03", true )
	ObjectHideSubObjectPermanently( self, "COIF", true )
	ObjectHideSubObjectPermanently( self, "HELM01", true )
	ObjectHideSubObjectPermanently( self, "HELM02", true )
	ObjectHideSubObjectPermanently( self, "HELM03", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM01", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM02", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM03", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM04", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM05", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM06", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM07", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM08", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM09", true )
	ObjectHideSubObjectPermanently( self, "HELMHEAD01", true )
	ObjectHideSubObjectPermanently( self, "HELMHEAD02", true )
	ObjectHideSubObjectPermanently( self, "NOHELM01", true )
	ObjectHideSubObjectPermanently( self, "NOHELM02", true )
	ObjectHideSubObjectPermanently( self, "LEGS01", true )
	ObjectHideSubObjectPermanently( self, "LEGS02", true )
	ObjectHideSubObjectPermanently( self, "LEGS03", true )
	ObjectHideSubObjectPermanently( self, "FLAG01", true )
	ObjectHideSubObjectPermanently( self, "FLAG02", true )
	ObjectHideSubObjectPermanently( self, "FLAG03", true )	

    local horse         =    GetRandomNumber()
    local shield 		=   GetRandomNumber()
    local head 		=   GetRandomNumber()
    local bodies 		=   GetRandomNumber()
    local cloaks 		=   GetRandomNumber()
    local legs 		=   GetRandomNumber()
    local flags 		=   GetRandomNumber()	
	
    if flags <= 0.3 then
        ObjectHideSubObjectPermanently( self, "FLAG01", false )
    elseif flags <= 0.6 then
        ObjectHideSubObjectPermanently( self, "FLAG03", false )
     else
        ObjectHideSubObjectPermanently( self, "FLAG03", false )
    end
	
    if bodies <= 0.4 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
    elseif bodies <= 0.8 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY03", false )
    end
	
    if cloaks <= 0.4 then
        ObjectHideSubObjectPermanently( self, "CLOAK01", false )
    elseif cloaks <= 0.8 then
        ObjectHideSubObjectPermanently( self, "CLOAK03", false )
     else
        ObjectHideSubObjectPermanently( self, "CLOAK02", false )
    end
	
    if legs <= 0.4 then
        ObjectHideSubObjectPermanently( self, "LEGS01", false )
	elseif legs <= 0.8 then
        ObjectHideSubObjectPermanently( self, "LEGS02", false )
     else
        ObjectHideSubObjectPermanently( self, "LEGS03", false )
    end	

    if horse <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HORSE01", false )
    elseif horse <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HORSE02", false )
    elseif horse <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HORSE03", false )
     else
        ObjectHideSubObjectPermanently( self, "HORSE04", false )
    end 
	
    if shield <= 0.1 then
        ObjectHideSubObjectPermanently( self, "SHIELD01", false )
    elseif shield <= 0.3 then
        ObjectHideSubObjectPermanently( self, "SHIELD02", false )
    elseif shield <= 0.4 then
        ObjectHideSubObjectPermanently( self, "SHIELD03", false )
    elseif shield <= 0.5 then
        ObjectHideSubObjectPermanently( self, "SHIELD04", false )
    elseif shield <= 0.7 then
        ObjectHideSubObjectPermanently( self, "SHIELD05", false )
    elseif shield <= 0.9 then
        ObjectHideSubObjectPermanently( self, "SHIELD06", false )
     else
        ObjectHideSubObjectPermanently( self, "SHIELD07", false )
    end
	
    if head <= 0.10 then
        ObjectHideSubObjectPermanently( self, "NOHELM01", false )
    elseif head <= 0.15 then
        ObjectHideSubObjectPermanently( self, "NOHELM02", false )
    elseif head <= 0.20 then
        ObjectHideSubObjectPermanently( self, "COIF", false )
    elseif head <= 0.25 then
        ObjectHideSubObjectPermanently( self, "NEWHELM01", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.30 then
        ObjectHideSubObjectPermanently( self, "NEWHELM02", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.35 then
        ObjectHideSubObjectPermanently( self, "NEWHELM03", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.40 then
        ObjectHideSubObjectPermanently( self, "NEWHELM04", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.45 then
        ObjectHideSubObjectPermanently( self, "NEWHELM05", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.50 then
        ObjectHideSubObjectPermanently( self, "NEWHELM06", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.55 then
        ObjectHideSubObjectPermanently( self, "NEWHELM07", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.58 then
        ObjectHideSubObjectPermanently( self, "NEWHELM08", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.60 then
        ObjectHideSubObjectPermanently( self, "NEWHELM09", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.65 then
        ObjectHideSubObjectPermanently( self, "NEWHELM01", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.70 then
        ObjectHideSubObjectPermanently( self, "NEWHELM02", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.75 then
        ObjectHideSubObjectPermanently( self, "NEWHELM04", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.80 then
        ObjectHideSubObjectPermanently( self, "NEWHELM05", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.85 then
        ObjectHideSubObjectPermanently( self, "NEWHELM06", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.90 then
        ObjectHideSubObjectPermanently( self, "NEWHELM07", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.95 then
        ObjectHideSubObjectPermanently( self, "NEWHELM08", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.99 then
        ObjectHideSubObjectPermanently( self, "NEWHELM09", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
	else
        ObjectHideSubObjectPermanently( self, "NEWHELM03", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    end
end

function OnNewRohirrimArcherCreated(self)
	ObjectHideSubObjectPermanently( self, "SHIELD01", true )
	ObjectHideSubObjectPermanently( self, "SHIELD02", true )
	ObjectHideSubObjectPermanently( self, "SHIELD03", true )
	ObjectHideSubObjectPermanently( self, "SHIELD04", true )
	ObjectHideSubObjectPermanently( self, "SHIELD05", true )
	ObjectHideSubObjectPermanently( self, "SHIELD06", true )
	ObjectHideSubObjectPermanently( self, "SHIELD07", true )
	ObjectHideSubObjectPermanently( self, "SHIELD09", true )
	ObjectHideSubObjectPermanently( self, "SHIELD10", true )
	ObjectHideSubObjectPermanently( self, "SHIELD11", true )
	ObjectHideSubObjectPermanently( self, "SHIELD12", true )
	ObjectHideSubObjectPermanently( self, "SHIELD13", true )
	ObjectHideSubObjectPermanently( self, "SHIELD14", true )
	ObjectHideSubObjectPermanently( self, "SHIELD15", true )
	ObjectHideSubObjectPermanently( self, "SHIELD16", true )
	ObjectHideSubObjectPermanently( self, "SHIELD17", true )
	ObjectHideSubObjectPermanently( self, "SHIELD18", true )
	ObjectHideSubObjectPermanently( self, "SHIELD19", true )
	ObjectHideSubObjectPermanently( self, "SHIELD20", true )
	ObjectHideSubObjectPermanently( self, "SHIELD21", true )
	ObjectHideSubObjectPermanently( self, "SHIELD22", true )
	ObjectHideSubObjectPermanently( self, "QUIVERA", true )
	ObjectHideSubObjectPermanently( self, "QUIVERB", true )
	ObjectHideSubObjectPermanently( self, "FireArowTip", true )
    ObjectHideSubObjectPermanently( self, "HORSE01", true )
    ObjectHideSubObjectPermanently( self, "HORSE02", true )
    ObjectHideSubObjectPermanently( self, "HORSE03", true )
    ObjectHideSubObjectPermanently( self, "HORSE04", true )	
	ObjectHideSubObjectPermanently( self, "BODY01", true )
	ObjectHideSubObjectPermanently( self, "BODY02", true )
	ObjectHideSubObjectPermanently( self, "BODY03", true )
	ObjectHideSubObjectPermanently( self, "CLOAK01", true )
	ObjectHideSubObjectPermanently( self, "CLOAK02", true )
	ObjectHideSubObjectPermanently( self, "CLOAK03", true )
	ObjectHideSubObjectPermanently( self, "COIF", true )
	ObjectHideSubObjectPermanently( self, "HELM01", true )
	ObjectHideSubObjectPermanently( self, "HELM02", true )
	ObjectHideSubObjectPermanently( self, "HELM03", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM01", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM02", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM03", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM04", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM05", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM06", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM07", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM08", true )
	ObjectHideSubObjectPermanently( self, "NEWHELM09", true )
	ObjectHideSubObjectPermanently( self, "HELMHEAD01", true )
	ObjectHideSubObjectPermanently( self, "HELMHEAD02", true )
	ObjectHideSubObjectPermanently( self, "NOHELM01", true )
	ObjectHideSubObjectPermanently( self, "NOHELM02", true )
	ObjectHideSubObjectPermanently( self, "LEGS01", true )
	ObjectHideSubObjectPermanently( self, "LEGS02", true )
	ObjectHideSubObjectPermanently( self, "LEGS03", true )

    local horse         =    GetRandomNumber()
    local shield 		=   GetRandomNumber()
    local head 		=   GetRandomNumber()
    local bodies 		=   GetRandomNumber()
    local cloaks 		=   GetRandomNumber()
    local legs 		=   GetRandomNumber()
	
    if bodies <= 0.4 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
    elseif bodies <= 0.8 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY03", false )
    end
	
    if cloaks <= 0.4 then
        ObjectHideSubObjectPermanently( self, "CLOAK01", false )
    elseif cloaks <= 0.8 then
        ObjectHideSubObjectPermanently( self, "CLOAK03", false )
     else
        ObjectHideSubObjectPermanently( self, "CLOAK02", false )
    end
	
    if legs <= 0.4 then
        ObjectHideSubObjectPermanently( self, "LEGS01", false )
	elseif legs <= 0.8 then
        ObjectHideSubObjectPermanently( self, "LEGS02", false )
     else
        ObjectHideSubObjectPermanently( self, "LEGS03", false )
    end	

    if horse <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HORSE01", false )
    elseif horse <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HORSE02", false )
    elseif horse <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HORSE03", false )
     else
        ObjectHideSubObjectPermanently( self, "HORSE04", false )
    end 
	
    if shield <= 0.1 then
        ObjectHideSubObjectPermanently( self, "SHIELD01", false )
        ObjectHideSubObjectPermanently( self, "QUIVERB", false )
    elseif shield <= 0.12 then
        ObjectHideSubObjectPermanently( self, "SHIELD02", false )
        ObjectHideSubObjectPermanently( self, "QUIVERB", false )
    elseif shield <= 0.16 then
        ObjectHideSubObjectPermanently( self, "SHIELD03", false )
        ObjectHideSubObjectPermanently( self, "QUIVERB", false )
    elseif shield <= 0.20 then
        ObjectHideSubObjectPermanently( self, "SHIELD04", false )
        ObjectHideSubObjectPermanently( self, "QUIVERB", false )
    elseif shield <= 0.24 then
        ObjectHideSubObjectPermanently( self, "SHIELD05", false )
        ObjectHideSubObjectPermanently( self, "QUIVERB", false )
    elseif shield <= 0.32 then
        ObjectHideSubObjectPermanently( self, "SHIELD06", false )
        ObjectHideSubObjectPermanently( self, "QUIVERB", false )
    elseif shield <= 0.36 then
        ObjectHideSubObjectPermanently( self, "SHIELD07", false )
        ObjectHideSubObjectPermanently( self, "QUIVERB", false )
    elseif shield <= 0.42 then
        ObjectHideSubObjectPermanently( self, "SHIELD08", false )
        ObjectHideSubObjectPermanently( self, "QUIVERA", false )
    elseif shield <= 0.48 then
        ObjectHideSubObjectPermanently( self, "SHIELD09", false )
        ObjectHideSubObjectPermanently( self, "QUIVERA", false )
    elseif shield <= 0.52 then
        ObjectHideSubObjectPermanently( self, "SHIELD10", false )
        ObjectHideSubObjectPermanently( self, "QUIVERA", false )
    elseif shield <= 0.56 then
        ObjectHideSubObjectPermanently( self, "SHIELD11", false )
        ObjectHideSubObjectPermanently( self, "QUIVERA", false )
    elseif shield <= 0.62 then
        ObjectHideSubObjectPermanently( self, "SHIELD12", false )
        ObjectHideSubObjectPermanently( self, "QUIVERA", false )
    elseif shield <= 0.68 then
        ObjectHideSubObjectPermanently( self, "SHIELD13", false )
        ObjectHideSubObjectPermanently( self, "QUIVERA", false )
    elseif shield <= 0.72 then
        ObjectHideSubObjectPermanently( self, "SHIELD14", false )
        ObjectHideSubObjectPermanently( self, "QUIVERA", false )
    elseif shield <= 0.78 then
        ObjectHideSubObjectPermanently( self, "SHIELD15", false )
        ObjectHideSubObjectPermanently( self, "QUIVERA", false )
    elseif shield <= 0.84 then
        ObjectHideSubObjectPermanently( self, "SHIELD16", false )
        ObjectHideSubObjectPermanently( self, "QUIVERA", false )
    elseif shield <= 0.86 then
        ObjectHideSubObjectPermanently( self, "SHIELD17", false )
        ObjectHideSubObjectPermanently( self, "QUIVERA", false )
    elseif shield <= 0.88 then
        ObjectHideSubObjectPermanently( self, "SHIELD18", false )
        ObjectHideSubObjectPermanently( self, "QUIVERA", false )
    elseif shield <= 0.92 then
        ObjectHideSubObjectPermanently( self, "SHIELD19", false )
        ObjectHideSubObjectPermanently( self, "QUIVERA", false )
	elseif shield <= 0.96 then
        ObjectHideSubObjectPermanently( self, "SHIELD20", false )
        ObjectHideSubObjectPermanently( self, "QUIVERA", false )
    elseif shield <= 0.99 then
        ObjectHideSubObjectPermanently( self, "SHIELD21", false )
        ObjectHideSubObjectPermanently( self, "QUIVERA", false )
     else
        ObjectHideSubObjectPermanently( self, "SHIELD22", false )
        ObjectHideSubObjectPermanently( self, "QUIVERA", false )
    end
	
    if head <= 0.10 then
        ObjectHideSubObjectPermanently( self, "NOHELM01", false )
    elseif head <= 0.15 then
        ObjectHideSubObjectPermanently( self, "NOHELM02", false )
    elseif head <= 0.20 then
        ObjectHideSubObjectPermanently( self, "COIF", false )
    elseif head <= 0.25 then
        ObjectHideSubObjectPermanently( self, "NEWHELM01", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.30 then
        ObjectHideSubObjectPermanently( self, "NEWHELM02", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.35 then
        ObjectHideSubObjectPermanently( self, "NEWHELM03", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.40 then
        ObjectHideSubObjectPermanently( self, "NEWHELM04", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.45 then
        ObjectHideSubObjectPermanently( self, "NEWHELM05", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.50 then
        ObjectHideSubObjectPermanently( self, "NEWHELM06", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.55 then
        ObjectHideSubObjectPermanently( self, "NEWHELM07", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.58 then
        ObjectHideSubObjectPermanently( self, "NEWHELM08", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.60 then
        ObjectHideSubObjectPermanently( self, "NEWHELM09", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.65 then
        ObjectHideSubObjectPermanently( self, "NEWHELM01", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.70 then
        ObjectHideSubObjectPermanently( self, "NEWHELM02", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.75 then
        ObjectHideSubObjectPermanently( self, "NEWHELM04", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.80 then
        ObjectHideSubObjectPermanently( self, "NEWHELM05", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.85 then
        ObjectHideSubObjectPermanently( self, "NEWHELM06", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.90 then
        ObjectHideSubObjectPermanently( self, "NEWHELM07", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.95 then
        ObjectHideSubObjectPermanently( self, "NEWHELM08", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.99 then
        ObjectHideSubObjectPermanently( self, "NEWHELM09", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
	else
        ObjectHideSubObjectPermanently( self, "NEWHELM03", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    end
end

function OnRohirrimCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "SHIELD", true )
	ObjectHideSubObjectPermanently( self, "FireArowTip", true )
end

function OnHorsemenoftheMarkCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	
    ObjectHideSubObjectPermanently( self, "CLOAK01", true )
    ObjectHideSubObjectPermanently( self, "CLOAK02", true )
    ObjectHideSubObjectPermanently( self, "CLOAK03", true )
	
    ObjectHideSubObjectPermanently( self, "BODY01", true )
    ObjectHideSubObjectPermanently( self, "BODY02", true )
    ObjectHideSubObjectPermanently( self, "BODY03", true )
	
    ObjectHideSubObjectPermanently( self, "GUGEL01", true )
    ObjectHideSubObjectPermanently( self, "GUGEL02", true )
    ObjectHideSubObjectPermanently( self, "GUGEL03", true )
	
    ObjectHideSubObjectPermanently( self, "COIF01", true )
    ObjectHideSubObjectPermanently( self, "COIF02", true )
    ObjectHideSubObjectPermanently( self, "COIF03", true )	
	
    ObjectHideSubObjectPermanently( self, "SHIELDA", true )
    ObjectHideSubObjectPermanently( self, "SHIELDB", true )

    ObjectHideSubObjectPermanently( self, "HORSE01", true )
    ObjectHideSubObjectPermanently( self, "HORSE02", true )
    ObjectHideSubObjectPermanently( self, "HORSE03", true )
    ObjectHideSubObjectPermanently( self, "HORSE04", true )

    local horse         =    GetRandomNumber()
    local shield         =    GetRandomNumber()
    local cloak         =    GetRandomNumber()
    local head 			=   GetRandomNumber()
    local body 			=   GetRandomNumber()

    if horse <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HORSE01", false )
    elseif horse <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HORSE02", false )
    elseif horse <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HORSE03", false )
     else
        ObjectHideSubObjectPermanently( self, "HORSE04", false )
    end 

    if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "GUGEL01", false )
        ObjectHideSubObjectPermanently( self, "COIF01", false )
    elseif head <= 0.6 then
        ObjectHideSubObjectPermanently( self, "GUGEL02", false )
        ObjectHideSubObjectPermanently( self, "COIF02", false )
     else
        ObjectHideSubObjectPermanently( self, "GUGEL03", false )
        ObjectHideSubObjectPermanently( self, "COIF03", false )
    end 

    if cloak <= 0.3 then
        ObjectHideSubObjectPermanently( self, "CLOAK01", false )
    elseif cloak <= 0.6 then
        ObjectHideSubObjectPermanently( self, "CLOAK02", false )
     else
        ObjectHideSubObjectPermanently( self, "CLOAK03", false )
    end 
	
    if shield <= 0.5 then
        ObjectHideSubObjectPermanently( self, "SHIELDA", false )
     else
        ObjectHideSubObjectPermanently( self, "SHIELDB", false )
    end 

    if body <= 0.3 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
    elseif body <= 0.6 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY03", false )
    end 
end

function OnKingsguardCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FireArowTip", true )
	
    ObjectHideSubObjectPermanently( self, "HELMHEAD01", true )
    ObjectHideSubObjectPermanently( self, "HELMHEAD02", true )

    ObjectHideSubObjectPermanently( self, "QUIVERA", true )
    ObjectHideSubObjectPermanently( self, "QUIVERB", true )

    ObjectHideSubObjectPermanently( self, "HORSE01", true )
    ObjectHideSubObjectPermanently( self, "HORSE02", true )
    ObjectHideSubObjectPermanently( self, "HORSE03", true )
    ObjectHideSubObjectPermanently( self, "HORSE04", true )

    local horse         =    GetRandomNumber()
    local quiver         =    GetRandomNumber()
    local head 		=   GetRandomNumber()

    if horse <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HORSE01", false )
    elseif horse <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HORSE02", false )
    elseif horse <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HORSE03", false )
     else
        ObjectHideSubObjectPermanently( self, "HORSE04", false )
    end 

    if head <= 0.50 then
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
     else
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    end 

    if quiver <= 0.50 then
        ObjectHideSubObjectPermanently( self, "QUIVERA", false )
     else
        ObjectHideSubObjectPermanently( self, "QUIVERB", false )
    end 
end

function OnSummonedRohirrimCreated(self)
	ObjectGrantUpgrade( self, "Upgrade_RohanHeavyArmorForRohirrim" ) -- ;,;
	ObjectGrantUpgrade( self, "Upgrade_RohanHorseShield" )
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FireArowTip", true ) -- ;,; Added in v5.1
end

function OnGondorCavalryCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "HIPS", true )
	ObjectHideSubObjectPermanently( self, "SPAULDERS", true )
	ObjectHideSubObjectPermanently( self, "SHIELD", true )	

    ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
    ObjectHideSubObjectPermanently( self, "HORSE01", true )
    ObjectHideSubObjectPermanently( self, "HORSE02", true )
    ObjectHideSubObjectPermanently( self, "HORSE03", true )
    ObjectHideSubObjectPermanently( self, "HORSE04", true )

    local horse         =    GetRandomNumber()
    local head 		=   GetRandomNumber()

    if horse <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HORSE01", false )
    elseif horse <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HORSE02", false )
    elseif horse <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HORSE03", false )
     else
        ObjectHideSubObjectPermanently( self, "HORSE04", false )
    end 

    if head <= 0.33 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.66 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end 
end

function OnSnowbournCreated(self)
	ObjectHideSubObjectPermanently( self, "FB", true )
	ObjectHideSubObjectPermanently( self, "SHLD_01", true )
    ObjectHideSubObjectPermanently( self, "CLOAK01", true )
    ObjectHideSubObjectPermanently( self, "CLOAK02", true )
    ObjectHideSubObjectPermanently( self, "CLOAK03", true )
    ObjectHideSubObjectPermanently( self, "HORSE01", true )
    ObjectHideSubObjectPermanently( self, "HORSE02", true )
    ObjectHideSubObjectPermanently( self, "HORSE03", true )

    local horse         =    GetRandomNumber()
    local body 		=   GetRandomNumber()

    if horse <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HORSE01", false )
    elseif horse <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HORSE02", false )
     else
        ObjectHideSubObjectPermanently( self, "HORSE03", false )
    end 

    if body <= 0.3 then
        ObjectHideSubObjectPermanently( self, "CLOAK01", false )
    elseif body <= 0.7 then
        ObjectHideSubObjectPermanently( self, "CLOAK02", false )
     else
        ObjectHideSubObjectPermanently( self, "CLOAK03", false )
    end 
end

function OnRohanCatapultCreated(self)
	--ObjectHideSubObjectPermanently( self, "PROJECTILEROCK", true )
	--ObjectHideSubObjectPermanently( self, "HAY", true )
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "OBJECT02", true )
	
	ObjectHideSubObjectPermanently( self, "BODY01", true )
	ObjectHideSubObjectPermanently( self, "BODY02", true )
	ObjectHideSubObjectPermanently( self, "BODY02sleeve", true )
	ObjectHideSubObjectPermanently( self, "BODY03", true )
	ObjectHideSubObjectPermanently( self, "CLOAK01", true )
	ObjectHideSubObjectPermanently( self, "CLOAK02", true )
	ObjectHideSubObjectPermanently( self, "CLOAK03", true )
	ObjectHideSubObjectPermanently( self, "COIF", true )
	ObjectHideSubObjectPermanently( self, "HELM01", true )
	ObjectHideSubObjectPermanently( self, "HELM02", true )
	ObjectHideSubObjectPermanently( self, "HELM03", true )
	ObjectHideSubObjectPermanently( self, "HELMHEAD01", true )
	ObjectHideSubObjectPermanently( self, "HELMHEAD02", true )
	ObjectHideSubObjectPermanently( self, "NOHELM01", true )
	ObjectHideSubObjectPermanently( self, "NOHELM02", true )
	ObjectHideSubObjectPermanently( self, "LEGS01", true )
	ObjectHideSubObjectPermanently( self, "LEGS02", true )

	ObjectHideSubObjectPermanently( self, "BODY01B", true )
	ObjectHideSubObjectPermanently( self, "BODY02B", true )
	ObjectHideSubObjectPermanently( self, "BODY02sleeveB", true )
	ObjectHideSubObjectPermanently( self, "BODY03B", true )
	ObjectHideSubObjectPermanently( self, "CLOAK01B", true )
	ObjectHideSubObjectPermanently( self, "CLOAK02B", true )
	ObjectHideSubObjectPermanently( self, "CLOAK03B", true )
	ObjectHideSubObjectPermanently( self, "COIFB", true )
	ObjectHideSubObjectPermanently( self, "HELM01B", true )
	ObjectHideSubObjectPermanently( self, "HELM02B", true )
	ObjectHideSubObjectPermanently( self, "HELM03B", true )
	ObjectHideSubObjectPermanently( self, "HELMHEAD01B", true )
	ObjectHideSubObjectPermanently( self, "HELMHEAD02B", true )
	ObjectHideSubObjectPermanently( self, "NOHELM01B", true )
	ObjectHideSubObjectPermanently( self, "NOHELM02B", true )
	ObjectHideSubObjectPermanently( self, "LEGS01B", true )
	ObjectHideSubObjectPermanently( self, "LEGS02B", true )
	
    local head          =    GetRandomNumber()
    local legs          =    GetRandomNumber()
    local cloaks          =    GetRandomNumber()
    local bodies          =    GetRandomNumber()
    local headb          =    GetRandomNumber()
    local legsb          =    GetRandomNumber()
    local cloaksb          =    GetRandomNumber()
    local bodiesb          =    GetRandomNumber()
	
    if bodies <= 0.2 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
    elseif bodies <= 0.5 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
		elseif bodies <= 0.7 then
        ObjectHideSubObjectPermanently( self, "BODY02sleeve", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY03", false )
    end
    if cloaks <= 0.3 then
        ObjectHideSubObjectPermanently( self, "CLOAK01", false )
    elseif cloaks <= 0.6 then
        ObjectHideSubObjectPermanently( self, "CLOAK03", false )
     else
        ObjectHideSubObjectPermanently( self, "CLOAK02", false )
    end
    if legs <= 0.5 then
        ObjectHideSubObjectPermanently( self, "LEGS01", false )
     else
        ObjectHideSubObjectPermanently( self, "LEGS02", false )
    end	
    if head <= 0.1 then
        ObjectHideSubObjectPermanently( self, "NOHELM01", false )
    elseif head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "NOHELM02", false )
    elseif head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "COIF", false )
    elseif head <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HELM01", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HELM02", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.65 then
        ObjectHideSubObjectPermanently( self, "HELM03", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HELM01", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.9 then
        ObjectHideSubObjectPermanently( self, "HELM02", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
     else
        ObjectHideSubObjectPermanently( self, "HELM03", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    end
    if bodiesb <= 0.2 then
        ObjectHideSubObjectPermanently( self, "BODY01B", false )
    elseif bodiesb <= 0.5 then
        ObjectHideSubObjectPermanently( self, "BODY02B", false )
		elseif bodiesb <= 0.7 then
        ObjectHideSubObjectPermanently( self, "BODY02sleeveB", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY03B", false )
    end
    if cloaksb <= 0.3 then
        ObjectHideSubObjectPermanently( self, "CLOAK01B", false )
    elseif cloaksb <= 0.6 then
        ObjectHideSubObjectPermanently( self, "CLOAK03B", false )
     else
        ObjectHideSubObjectPermanently( self, "CLOAK02B", false )
    end
    if legsb <= 0.5 then
        ObjectHideSubObjectPermanently( self, "LEGS01B", false )
     else
        ObjectHideSubObjectPermanently( self, "LEGS02B", false )
    end	
    if headb <= 0.1 then
        ObjectHideSubObjectPermanently( self, "NOHELM01B", false )
    elseif headb <= 0.2 then
        ObjectHideSubObjectPermanently( self, "NOHELM02B", false )
    elseif headb <= 0.3 then
        ObjectHideSubObjectPermanently( self, "COIFB", false )
    elseif headb <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HELM01B", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01B", false )
    elseif headb <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HELM02B", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01B", false )
    elseif headb <= 0.65 then
        ObjectHideSubObjectPermanently( self, "HELM03B", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01B", false )
    elseif headb <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HELM01B", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02B", false )
    elseif headb <= 0.9 then
        ObjectHideSubObjectPermanently( self, "HELM02B", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02B", false )
     else
        ObjectHideSubObjectPermanently( self, "HELM03B", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02B", false )
    end
end

function OnYeomenPeasantCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "OBJECT02", true )
	ObjectHideSubObjectPermanently( self, "BODY01", true )
	ObjectHideSubObjectPermanently( self, "BODY02", true )
	ObjectHideSubObjectPermanently( self, "BODY03", true )
	ObjectHideSubObjectPermanently( self, "CLOAK01", true )
	ObjectHideSubObjectPermanently( self, "CLOAK02", true )
	ObjectHideSubObjectPermanently( self, "CLOAK03", true )
	ObjectHideSubObjectPermanently( self, "COIF", true )
	ObjectHideSubObjectPermanently( self, "HELM01", true )
	ObjectHideSubObjectPermanently( self, "HELM02", true )
	ObjectHideSubObjectPermanently( self, "HELM03", true )
	ObjectHideSubObjectPermanently( self, "HELMHEAD01", true )
	ObjectHideSubObjectPermanently( self, "HELMHEAD02", true )
	ObjectHideSubObjectPermanently( self, "NOHELM01", true )
	ObjectHideSubObjectPermanently( self, "NOHELM02", true )
	ObjectHideSubObjectPermanently( self, "LEGS01", true )
	ObjectHideSubObjectPermanently( self, "LEGS02", true )
	ObjectHideSubObjectPermanently( self, "SHIELD01", true )
	ObjectHideSubObjectPermanently( self, "SHIELD02", true )
	ObjectHideSubObjectPermanently( self, "SHIELD03", true )
	ObjectHideSubObjectPermanently( self, "SHIELD04", true )
	ObjectHideSubObjectPermanently( self, "SHIELD05", true )
	ObjectHideSubObjectPermanently( self, "SHIELD06", true )
	ObjectHideSubObjectPermanently( self, "SHIELD07", true )
    local head          =    GetRandomNumber()
    local shield          =    GetRandomNumber()
    local legs          =    GetRandomNumber()
    local cloaks          =    GetRandomNumber()
    local bodies          =    GetRandomNumber()
    if bodies <= 0.3 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
    elseif bodies <= 0.6 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY03", false )
    end
    if cloaks <= 0.3 then
        ObjectHideSubObjectPermanently( self, "CLOAK01", false )
    elseif cloaks <= 0.6 then
        ObjectHideSubObjectPermanently( self, "CLOAK03", false )
     else
        ObjectHideSubObjectPermanently( self, "CLOAK02", false )
    end
    if legs <= 0.5 then
        ObjectHideSubObjectPermanently( self, "LEGS01", false )
     else
        ObjectHideSubObjectPermanently( self, "LEGS02", false )
    end	
    if shield <= 0.1 then
        ObjectHideSubObjectPermanently( self, "SHIELD01", false )
    elseif shield <= 0.3 then
        ObjectHideSubObjectPermanently( self, "SHIELD02", false )
    elseif shield <= 0.4 then
        ObjectHideSubObjectPermanently( self, "SHIELD03", false )
    elseif shield <= 0.5 then
        ObjectHideSubObjectPermanently( self, "SHIELD04", false )
    elseif shield <= 0.7 then
        ObjectHideSubObjectPermanently( self, "SHIELD05", false )
    elseif shield <= 0.9 then
        ObjectHideSubObjectPermanently( self, "SHIELD06", false )
     else
        ObjectHideSubObjectPermanently( self, "SHIELD07", false )
    end
    if head <= 0.1 then
        ObjectHideSubObjectPermanently( self, "NOHELM01", false )
    elseif head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "NOHELM02", false )
    elseif head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "COIF", false )
    elseif head <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HELM01", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HELM02", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.65 then
        ObjectHideSubObjectPermanently( self, "HELM03", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HELM01", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.9 then
        ObjectHideSubObjectPermanently( self, "HELM02", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
     else
        ObjectHideSubObjectPermanently( self, "HELM03", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    end
end

function OnYeomenArcherRangeCreated(self)
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )
end

function OnYeomenPeasantArcherCreated(self)
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )
	ObjectHideSubObjectPermanently( self, "BODY01", true )
	ObjectHideSubObjectPermanently( self, "BODY02", true )
	ObjectHideSubObjectPermanently( self, "BODY03", true )
	ObjectHideSubObjectPermanently( self, "BODY01SLEEVE", true )
	ObjectHideSubObjectPermanently( self, "BODY02SLEEVE", true )
	ObjectHideSubObjectPermanently( self, "BODY03SLEEVE", true )
	ObjectHideSubObjectPermanently( self, "CLOAK01", true )
	ObjectHideSubObjectPermanently( self, "CLOAK02", true )
	ObjectHideSubObjectPermanently( self, "CLOAK03", true )
	ObjectHideSubObjectPermanently( self, "COIF", true )
	ObjectHideSubObjectPermanently( self, "HELM01", true )
	ObjectHideSubObjectPermanently( self, "HELM02", true )
	ObjectHideSubObjectPermanently( self, "HELM03", true )
	ObjectHideSubObjectPermanently( self, "HELMHEAD01", true )
	ObjectHideSubObjectPermanently( self, "HELMHEAD02", true )
	ObjectHideSubObjectPermanently( self, "NOHELM01", true )
	ObjectHideSubObjectPermanently( self, "NOHELM02", true )
	ObjectHideSubObjectPermanently( self, "LEGS01", true )
	ObjectHideSubObjectPermanently( self, "LEGS02", true )
    local head          =    GetRandomNumber()
    local shield          =    GetRandomNumber()
    local legs          =    GetRandomNumber()
    local cloaks          =    GetRandomNumber()
    local bodies          =    GetRandomNumber()
    if bodies <= 0.2 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
    elseif bodies <= 0.3 then
        ObjectHideSubObjectPermanently( self, "BODY01SLEEVE", false )
	elseif bodies <= 0.5 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
	elseif bodies <= 0.7 then
        ObjectHideSubObjectPermanently( self, "BODY02SLEEVE", false )
	elseif bodies <= 0.9 then
        ObjectHideSubObjectPermanently( self, "BODY03", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY03SLEEVE", false )
    end
    if cloaks <= 0.3 then
        ObjectHideSubObjectPermanently( self, "CLOAK01", false )
    elseif cloaks <= 0.6 then
        ObjectHideSubObjectPermanently( self, "CLOAK03", false )
     else
        ObjectHideSubObjectPermanently( self, "CLOAK02", false )
    end
    if legs <= 0.5 then
        ObjectHideSubObjectPermanently( self, "LEGS01", false )
     else
        ObjectHideSubObjectPermanently( self, "LEGS02", false )
    end	
    if head <= 0.1 then
        ObjectHideSubObjectPermanently( self, "NOHELM01", false )
    elseif head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "NOHELM02", false )
    elseif head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "COIF", false )
    elseif head <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HELM01", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HELM02", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.65 then
        ObjectHideSubObjectPermanently( self, "HELM03", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HELM01", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.9 then
        ObjectHideSubObjectPermanently( self, "HELM02", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
     else
        ObjectHideSubObjectPermanently( self, "HELM03", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    end
end

function OnDwarvenBattleWagonCreated(self)
	ObjectHideSubObjectPermanently( self, "dwarfHearth", true )
	ObjectHideSubObjectPermanently( self, "dwarfHearthFire", true )
	ObjectHideSubObjectPermanently( self, "Banner_L", true )
	ObjectHideSubObjectPermanently( self, "Glow", true )	
	ObjectHideSubObjectPermanently( self, "BLADES_L", true )
	ObjectHideSubObjectPermanently( self, "BLADES_R", true )
	ObjectHideSubObjectPermanently( self, "FLAGS", true )	
    ObjectHideSubObjectPermanently( self, "A_RAMHORN1", true )
    ObjectHideSubObjectPermanently( self, "A_RAMHORN2", true )
    ObjectHideSubObjectPermanently( self, "A_RAM", true )
    ObjectHideSubObjectPermanently( self, "A_RAM2", true )
    ObjectHideSubObjectPermanently( self, "A_RAM3", true )
    ObjectHideSubObjectPermanently( self, "A_A_RAMHORN1", true )
    ObjectHideSubObjectPermanently( self, "A_A_RAMHORN2", true )
    ObjectHideSubObjectPermanently( self, "A_A_RAM", true )
    ObjectHideSubObjectPermanently( self, "A_A_RAM2", true )
    ObjectHideSubObjectPermanently( self, "A_A_RAM3", true )
    ObjectHideSubObjectPermanently( self, "F_A_RAMHORN1", true )
    ObjectHideSubObjectPermanently( self, "F_A_RAMHORN2", true )
    ObjectHideSubObjectPermanently( self, "F_A_RAM", true )
    ObjectHideSubObjectPermanently( self, "F_A_RAM2", true )
    ObjectHideSubObjectPermanently( self, "F_A_RAM3", true )
    ObjectHideSubObjectPermanently( self, "C_A_RAMHORN1", true )
    ObjectHideSubObjectPermanently( self, "C_A_RAMHORN2", true )
    ObjectHideSubObjectPermanently( self, "C_A_RAM", true )
    ObjectHideSubObjectPermanently( self, "C_A_RAM2", true )
    ObjectHideSubObjectPermanently( self, "C_A_RAM3", true )
    local helmet         =    GetRandomNumber()
    local armor          =    GetRandomNumber()
    local helmet1         =    GetRandomNumber()
    local armor1          =    GetRandomNumber()
    local helmet2        =    GetRandomNumber()
    local armor2          =    GetRandomNumber()
    local helmet3         =    GetRandomNumber()
    local armor3          =    GetRandomNumber()
    if helmet <= 0.3 then
        ObjectHideSubObjectPermanently( self, "A_RAM", false )
    elseif helmet <= 0.6 then
        ObjectHideSubObjectPermanently( self, "A_RAM2", false )
     else
        ObjectHideSubObjectPermanently( self, "A_RAM3", false )
    end
    if armor <= 0.5 then
        ObjectHideSubObjectPermanently( self, "A_RAMHORN1", false )
     else
        ObjectHideSubObjectPermanently( self, "A_RAMHORN2", false )
    end	
    if helmet1 <= 0.3 then
        ObjectHideSubObjectPermanently( self, "A_A_RAM", false )
    elseif helmet1 <= 0.6 then
        ObjectHideSubObjectPermanently( self, "A_A_RAM2", false )
     else
        ObjectHideSubObjectPermanently( self, "A_A_RAM3", false )
    end
    if armor1 <= 0.5 then
        ObjectHideSubObjectPermanently( self, "A_A_RAMHORN1", false )
     else
        ObjectHideSubObjectPermanently( self, "A_A_RAMHORN2", false )
    end	
    if helmet2 <= 0.3 then
        ObjectHideSubObjectPermanently( self, "C_A_RAM", false )
    elseif helmet2 <= 0.6 then
        ObjectHideSubObjectPermanently( self, "C_A_RAM2", false )
     else
        ObjectHideSubObjectPermanently( self, "C_A_RAM3", false )
    end	
    if armor2 <= 0.5 then
        ObjectHideSubObjectPermanently( self, "C_A_RAMHORN1", false )
     else
        ObjectHideSubObjectPermanently( self, "C_A_RAMHORN2", false )
    end	
    if helmet3 <= 0.3 then
        ObjectHideSubObjectPermanently( self, "F_A_RAM", false )
    elseif helmet3 <= 0.6 then
        ObjectHideSubObjectPermanently( self, "F_A_RAM2", false )
     else
        ObjectHideSubObjectPermanently( self, "F_A_RAM3", false )
    end	
    if armor3 <= 0.5 then
        ObjectHideSubObjectPermanently( self, "F_A_RAMHORN1", false )
     else
        ObjectHideSubObjectPermanently( self, "F_A_RAMHORN2", false )
    end 
end

function OnEvilMenBlackRiderCreated(self)
	-- @todo place appropriate functionality here
end

function OnBallistaFortCreated(self)
	ObjectHideSubObjectPermanently( self, "MinedArrow", true )
    ObjectHideSubObjectPermanently( self, "HELMET01", true )
    ObjectHideSubObjectPermanently( self, "HELMET02", true )
end

function OnBallistaCreated(self)
	ObjectHideSubObjectPermanently( self, "MinedArrow", true )
    ObjectHideSubObjectPermanently( self, "O1_HELMET01", true )
    ObjectHideSubObjectPermanently( self, "O2_HELMET01", true )
    ObjectHideSubObjectPermanently( self, "O1_HELMET02", true )
    ObjectHideSubObjectPermanently( self, "O2_HELMET02", true )
end

function OnCatapultCreated(self)
	ObjectHideSubObjectPermanently( self, "PROJECTILEROCK", true )
	ObjectHideSubObjectPermanently( self, "FIREPLANE", true )
end

function OnTrebuchetCreated(self)
	ObjectHideSubObjectPermanently( self, "FIREPLANE", true )
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
	local head          =    GetRandomNumber()
    if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
	else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end    
end

function OnRevelerCreated(self)
	ObjectHideSubObjectPermanently( self, "FIREPLANE", true )
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "BODY01", true )
    ObjectHideSubObjectPermanently( self, "BODY02", true )
    ObjectHideSubObjectPermanently( self, "BODY03", true )
	local head          =    GetRandomNumber()
	local body          =    GetRandomNumber()
    if head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
	else
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
    end   
	
    if body <= 0.3 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
    elseif body <= 0.7 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
	else
        ObjectHideSubObjectPermanently( self, "BODY03", false )
    end    
end

function OnTrebuchetFortCreated(self)
	ObjectHideSubObjectPermanently( self, "FIREPLANE", true )
	ObjectHideSubObjectPermanently( self, "MANATARMSA", true )
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
	local head          =    GetRandomNumber()
    if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
	else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end    
end


function OnTrollSlingCreated(self)
	ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
end

function OnPorterCreated(self)
	ObjectHideSubObjectPermanently( self, "ARROWS", true )
	ObjectHideSubObjectPermanently( self, "BRAZIER", true )
	ObjectHideSubObjectPermanently( self, "BOWS", true )
	ObjectHideSubObjectPermanently( self, "TREBUCHET_FIRE", true )
	ObjectHideSubObjectPermanently( self, "SWORDS", true )
	ObjectHideSubObjectPermanently( self, "SHIELDS", true )
	ObjectHideSubObjectPermanently( self, "ARMOR", true )
	ObjectHideSubObjectPermanently( self, "BANNERS", true )
end

function OnEvilPorterCreated(self)
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )
	ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
	ObjectHideSubObjectPermanently( self, "ARROW_UPGRADE", true )
	ObjectHideSubObjectPermanently( self, "ARMOR_UPGRADE", true )
	ObjectHideSubObjectPermanently( self, "GOLD", true )
	ObjectHideSubObjectPermanently( self, "SWORD_UPGRADES", true )
end

function OnPeasantCreated(self)
	ObjectHideSubObjectPermanently( self, "BODY01", true )
	ObjectHideSubObjectPermanently( self, "BODY02", true )
	ObjectHideSubObjectPermanently( self, "BODY03", true )
	ObjectHideSubObjectPermanently( self, "BODY04", true )
	ObjectHideSubObjectPermanently( self, "BODY05", true )
	ObjectHideSubObjectPermanently( self, "BODY06", true )
	ObjectHideSubObjectPermanently( self, "CLOAK01", true )
	ObjectHideSubObjectPermanently( self, "CLOAK02", true )
	ObjectHideSubObjectPermanently( self, "CLOAK03", true )	
	ObjectHideSubObjectPermanently( self, "HELM01", true )
	ObjectHideSubObjectPermanently( self, "HELM02", true )
	ObjectHideSubObjectPermanently( self, "HELM03", true )
	ObjectHideSubObjectPermanently( self, "HELM04", true )
	ObjectHideSubObjectPermanently( self, "HELM05", true )
	ObjectHideSubObjectPermanently( self, "HELM06", true )
	ObjectHideSubObjectPermanently( self, "HELM07", true )
	ObjectHideSubObjectPermanently( self, "HELM08", true )
	ObjectHideSubObjectPermanently( self, "HELM09", true )
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
	ObjectHideSubObjectPermanently( self, "HEAD02", true )
	ObjectHideSubObjectPermanently( self, "HEAD03", true )
	ObjectHideSubObjectPermanently( self, "HEAD04", true )
	ObjectHideSubObjectPermanently( self, "HEAD05", true )
	ObjectHideSubObjectPermanently( self, "HEAD06", true )
	ObjectHideSubObjectPermanently( self, "CAP01", true )
	ObjectHideSubObjectPermanently( self, "CAP02", true )
	ObjectHideSubObjectPermanently( self, "CAP03", true )	
	ObjectHideSubObjectPermanently( self, "GUGEL01", true )
	ObjectHideSubObjectPermanently( self, "GUGEL02", true )
	ObjectHideSubObjectPermanently( self, "DRAFTSWORD", true )
	ObjectHideSubObjectPermanently( self, "SHEATH", true )
	ObjectHideSubObjectPermanently( self, "WEAP01", true )
	ObjectHideSubObjectPermanently( self, "WEAP02", true )
	ObjectHideSubObjectPermanently( self, "WEAP03", true )
	ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
	ObjectHideSubObjectPermanently( self, "SHIELDA", true )
	ObjectHideSubObjectPermanently( self, "SHIELDB", true )
	ObjectHideSubObjectPermanently( self, "Broom", true )

    local body    =    GetRandomNumber()
    
    if body <= 0.2 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
    elseif body <= 0.3 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
    elseif body <= 0.5 then
        ObjectHideSubObjectPermanently( self, "BODY03", false )
    elseif body <= 0.7 then
        ObjectHideSubObjectPermanently( self, "BODY04", false )
    elseif body <= 0.8 then
        ObjectHideSubObjectPermanently( self, "BODY05", false )			
	else
        ObjectHideSubObjectPermanently( self, "BODY06", false )	
    end
	
    local cloak    =    GetRandomNumber()
    
    if cloak <= 0.3 then
        ObjectHideSubObjectPermanently( self, "CLOAK01", false )
    elseif cloak <= 0.6	then
        ObjectHideSubObjectPermanently( self, "CLOAK02", false )
    elseif cloak <= 0.8 then
        ObjectHideSubObjectPermanently( self, "CLOAK03", false )		
	else
        -- no cloak	
    end

    local head    =    GetRandomNumber()
    
    if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
        ObjectHideSubObjectPermanently( self, "HEAD04", false )		--drafted model
    elseif head <= 0.6	then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
        ObjectHideSubObjectPermanently( self, "HEAD05", false )		--drafted model
    else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
        ObjectHideSubObjectPermanently( self, "HEAD06", false )		--drafted model
    end	
	
    local hat    =    GetRandomNumber()
    
    if hat <= 0.2 then
        ObjectHideSubObjectPermanently( self, "CAP01", false )
    elseif hat <= 0.3 then
        ObjectHideSubObjectPermanently( self, "CAP02", false )
    elseif hat <= 0.5 then
        ObjectHideSubObjectPermanently( self, "CAP03", false )
    elseif hat <= 0.7 then
        ObjectHideSubObjectPermanently( self, "GUGEL01", false )
		ObjectHideSubObjectPermanently( self, "HEAD01", true )
		ObjectHideSubObjectPermanently( self, "HEAD02", true )
		ObjectHideSubObjectPermanently( self, "HEAD03", true )		
    elseif hat <= 0.8 then
        ObjectHideSubObjectPermanently( self, "GUGEL02", false )	
		ObjectHideSubObjectPermanently( self, "HEAD01", true )
		ObjectHideSubObjectPermanently( self, "HEAD02", true )
		ObjectHideSubObjectPermanently( self, "HEAD03", true )		
	else
		-- no hat
    end
	
    local weapon    =    GetRandomNumber()
    
    if weapon <= 0.3 then
        ObjectHideSubObjectPermanently( self, "WEAP01", false )
    elseif weapon <= 0.6	then
        ObjectHideSubObjectPermanently( self, "WEAP02", false )
    else
        ObjectHideSubObjectPermanently( self, "WEAP03", false )
    end

    local shield    =    GetRandomNumber()
    
    if shield <= 0.5 then
        ObjectHideSubObjectPermanently( self, "SHIELDA", false )
    else
        ObjectHideSubObjectPermanently( self, "SHIELDB", false )
    end
	
    local helmet    =    GetRandomNumber()

    if helmet <= 0.2 then
        ObjectHideSubObjectPermanently( self, "HELM01", false )
    elseif helmet <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HELM02", false )
    elseif helmet <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HELM03", false )
    elseif helmet <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HELM04", false )
    elseif helmet <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HELM05", false )	
    elseif helmet <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HELM06", false )
    elseif helmet <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HELM07", false )
    elseif helmet <= 0.9 then
        ObjectHideSubObjectPermanently( self, "HELM08", false )		
	else
        ObjectHideSubObjectPermanently( self, "HELM09", false )	
    end
	
end

function OnDraftedPeasantCreated(self)
	ObjectGrantUpgrade( self, "Upgrade_Drafted" )
	ObjectHideSubObjectPermanently( self, "BODY01", true )
	ObjectHideSubObjectPermanently( self, "BODY02", true )
	ObjectHideSubObjectPermanently( self, "BODY03", true )
	ObjectHideSubObjectPermanently( self, "BODY04", true )
	ObjectHideSubObjectPermanently( self, "BODY05", true )
	ObjectHideSubObjectPermanently( self, "BODY06", true )
	ObjectHideSubObjectPermanently( self, "CLOAK01", true )
	ObjectHideSubObjectPermanently( self, "CLOAK02", true )
	ObjectHideSubObjectPermanently( self, "CLOAK03", true )	
	ObjectHideSubObjectPermanently( self, "HELM01", true )
	ObjectHideSubObjectPermanently( self, "HELM02", true )
	ObjectHideSubObjectPermanently( self, "HELM03", true )
	ObjectHideSubObjectPermanently( self, "HELM04", true )
	ObjectHideSubObjectPermanently( self, "HELM05", true )
	ObjectHideSubObjectPermanently( self, "HELM06", true )
	ObjectHideSubObjectPermanently( self, "HELM07", true )
	ObjectHideSubObjectPermanently( self, "HELM08", true )
	ObjectHideSubObjectPermanently( self, "HELM09", true )
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
	ObjectHideSubObjectPermanently( self, "HEAD02", true )
	ObjectHideSubObjectPermanently( self, "HEAD03", true )
	ObjectHideSubObjectPermanently( self, "CAP01", true )
	ObjectHideSubObjectPermanently( self, "CAP02", true )
	ObjectHideSubObjectPermanently( self, "CAP03", true )	
	ObjectHideSubObjectPermanently( self, "GUGEL01", true )
	ObjectHideSubObjectPermanently( self, "GUGEL02", true )
	--ObjectHideSubObjectPermanently( self, "DRAFTSWORD", true )
	--ObjectHideSubObjectPermanently( self, "SHEATH", true )
	ObjectHideSubObjectPermanently( self, "WEAP01", true )
	ObjectHideSubObjectPermanently( self, "WEAP02", true )
	ObjectHideSubObjectPermanently( self, "WEAP03", true )
	ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
	ObjectHideSubObjectPermanently( self, "SHIELDA", true )
	ObjectHideSubObjectPermanently( self, "SHIELDB", true )
	ObjectHideSubObjectPermanently( self, "Broom", true )

    local body    =    GetRandomNumber()
    
    if body <= 0.2 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
    elseif body <= 0.3 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
    elseif body <= 0.5 then
        ObjectHideSubObjectPermanently( self, "BODY03", false )
    elseif body <= 0.7 then
        ObjectHideSubObjectPermanently( self, "BODY04", false )
    elseif body <= 0.8 then
        ObjectHideSubObjectPermanently( self, "BODY05", false )			
	else
        ObjectHideSubObjectPermanently( self, "BODY06", false )	
    end
	
    local cloak    =    GetRandomNumber()
    
    if cloak <= 0.3 then
        ObjectHideSubObjectPermanently( self, "CLOAK01", false )
    elseif cloak <= 0.6	then
        ObjectHideSubObjectPermanently( self, "CLOAK02", false )
    elseif cloak <= 0.8 then
        ObjectHideSubObjectPermanently( self, "CLOAK03", false )		
	else
        -- no cloak	
    end

    local head    =    GetRandomNumber()
    
    if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
        ObjectHideSubObjectPermanently( self, "HEAD04", false )
    elseif head <= 0.6	then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
        ObjectHideSubObjectPermanently( self, "HEAD05", false )
    else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
        ObjectHideSubObjectPermanently( self, "HEAD06", false )
    end	
	
    local shield    =    GetRandomNumber()
    
    if shield <= 0.3 then
        ObjectHideSubObjectPermanently( self, "SHIELDA", false )
    else
        ObjectHideSubObjectPermanently( self, "SHIELDB", false )
    end
	
    local helmet    =    GetRandomNumber()

    if helmet <= 0.2 then
        ObjectHideSubObjectPermanently( self, "HELM01", false )
    elseif helmet <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HELM02", false )
    elseif helmet <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HELM03", false )
    elseif helmet <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HELM04", false )
    elseif helmet <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HELM05", false )	
    elseif helmet <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HELM06", false )
    elseif helmet <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HELM07", false )
    elseif helmet <= 0.9 then
        ObjectHideSubObjectPermanently( self, "HELM08", false )		
	else
        ObjectHideSubObjectPermanently( self, "HELM09", false )	
    end
	
end

function OnDraftTowerPeasantCreated(self)
	ObjectHideSubObjectPermanently( self, "BODY01", true )
	ObjectHideSubObjectPermanently( self, "BODY02", true )
	ObjectHideSubObjectPermanently( self, "BODY03", true )
	ObjectHideSubObjectPermanently( self, "BODY04", true )
	ObjectHideSubObjectPermanently( self, "BODY05", true )
	ObjectHideSubObjectPermanently( self, "BODY06", true )
	ObjectHideSubObjectPermanently( self, "CLOAK01", true )
	ObjectHideSubObjectPermanently( self, "CLOAK02", true )
	ObjectHideSubObjectPermanently( self, "CLOAK03", true )	
	ObjectHideSubObjectPermanently( self, "HELM01", true )
	ObjectHideSubObjectPermanently( self, "HELM02", true )
	ObjectHideSubObjectPermanently( self, "HELM03", true )
	ObjectHideSubObjectPermanently( self, "HELM04", true )
	ObjectHideSubObjectPermanently( self, "HELM05", true )
	ObjectHideSubObjectPermanently( self, "HELM06", true )
	ObjectHideSubObjectPermanently( self, "HELM07", true )
	ObjectHideSubObjectPermanently( self, "HELM08", true )
	ObjectHideSubObjectPermanently( self, "HELM09", true )
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
	ObjectHideSubObjectPermanently( self, "HEAD02", true )
	ObjectHideSubObjectPermanently( self, "HEAD03", true )
	ObjectHideSubObjectPermanently( self, "CAP01", true )
	ObjectHideSubObjectPermanently( self, "CAP02", true )
	ObjectHideSubObjectPermanently( self, "CAP03", true )	
	ObjectHideSubObjectPermanently( self, "GUGEL01", true )
	ObjectHideSubObjectPermanently( self, "GUGEL02", true )
	ObjectHideSubObjectPermanently( self, "DRAFTSWORD", true )
	ObjectHideSubObjectPermanently( self, "SHEATH", true )
	ObjectHideSubObjectPermanently( self, "WEAP01", true )
	ObjectHideSubObjectPermanently( self, "WEAP02", true )
	ObjectHideSubObjectPermanently( self, "WEAP03", true )
	ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
	ObjectHideSubObjectPermanently( self, "SHIELDA", true )
	ObjectHideSubObjectPermanently( self, "SHIELDB", true )
	ObjectHideSubObjectPermanently( self, "Broom", true )

    local body    =    GetRandomNumber()
    
    if body <= 0.2 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
    elseif body <= 0.3 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
    elseif body <= 0.5 then
        ObjectHideSubObjectPermanently( self, "BODY03", false )
    elseif body <= 0.7 then
        ObjectHideSubObjectPermanently( self, "BODY04", false )
    elseif body <= 0.8 then
        ObjectHideSubObjectPermanently( self, "BODY05", false )			
	else
        ObjectHideSubObjectPermanently( self, "BODY06", false )	
    end
	
    local cloak    =    GetRandomNumber()
    
    if cloak <= 0.3 then
        ObjectHideSubObjectPermanently( self, "CLOAK01", false )
    elseif cloak <= 0.6	then
        ObjectHideSubObjectPermanently( self, "CLOAK02", false )
    elseif cloak <= 0.8 then
        ObjectHideSubObjectPermanently( self, "CLOAK03", false )		
	else
        -- no cloak	
    end

    local head    =    GetRandomNumber()
    
    if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.6	then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
    else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end
	
    local weapon    =    GetRandomNumber()
    
    if weapon <= 0.3 then
        ObjectHideSubObjectPermanently( self, "WEAP01", false )
    elseif weapon <= 0.6	then
        ObjectHideSubObjectPermanently( self, "WEAP02", false )
    else
        ObjectHideSubObjectPermanently( self, "WEAP03", false )
    end
	
    local helmet    =    GetRandomNumber()

    if helmet <= 0.2 then
        ObjectHideSubObjectPermanently( self, "HELM01", false )
    elseif helmet <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HELM02", false )
    elseif helmet <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HELM03", false )
    elseif helmet <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HELM04", false )
    elseif helmet <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HELM05", false )	
    elseif helmet <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HELM06", false )
    elseif helmet <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HELM07", false )
    elseif helmet <= 0.9 then
        ObjectHideSubObjectPermanently( self, "HELM08", false )		
	else
        ObjectHideSubObjectPermanently( self, "HELM09", false )	
    end
	
end

function OnMordorSauronCreated(self)
	ObjectHideSubObjectPermanently( self, "SHARD01", true )
	ObjectHideSubObjectPermanently( self, "SHARD02", true )
	ObjectHideSubObjectPermanently( self, "SHARD03", true )
	ObjectHideSubObjectPermanently( self, "SHARD04", true )
	ObjectHideSubObjectPermanently( self, "SHARD05", true )
	ObjectHideSubObjectPermanently( self, "SHARD06", true )
	ObjectHideSubObjectPermanently( self, "SHARD07", true )
	ObjectHideSubObjectPermanently( self, "SHARD08", true )
	ObjectHideSubObjectPermanently( self, "SHARD09", true )
	ObjectHideSubObjectPermanently( self, "SHARD10", true )
	ObjectHideSubObjectPermanently( self, "SHARD11", true )
	ObjectHideSubObjectPermanently( self, "SHARD12", true )
	ObjectHideSubObjectPermanently( self, "SHARD13", true )
	ObjectHideSubObjectPermanently( self, "SHARD14", true )
	ObjectHideSubObjectPermanently( self, "SHARD15", true )
	ObjectHideSubObjectPermanently( self, "SHARD16", true )
	ObjectHideSubObjectPermanently( self, "SHARD17", true )
	ObjectHideSubObjectPermanently( self, "SHARD18", true )
	ObjectHideSubObjectPermanently( self, "SHARD19", true )
	ObjectHideSubObjectPermanently( self, "SHARD20", true )
	ObjectDoSpecialPower( self, "SpecialAbilitySauronDarkness" ) -- ;,;
end

function OnNoldorExilesCreated(self)
	ObjectHideSubObject( self, "ARROW", true )
	ObjectHideSubObject( self, "ARROWNOCKUP", true )
	ObjectHideSubObject( self, "QUIVERUP", true )
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )
	
	ObjectHideSubObjectPermanently( self, "BODY1", true )
	ObjectHideSubObjectPermanently( self, "BODY2", true )
	ObjectHideSubObjectPermanently( self, "BODY3", true )
	
	ObjectHideSubObjectPermanently( self, "HEAD1", true )
	ObjectHideSubObjectPermanently( self, "HEAD2", true )
	ObjectHideSubObjectPermanently( self, "HEAD3", true )
	
	ObjectHideSubObjectPermanently( self, "SASH1", true )
	ObjectHideSubObjectPermanently( self, "SASH2", true )
	ObjectHideSubObjectPermanently( self, "SASH3", true )
	

    local head         =    GetRandomNumber()
	local body         =    GetRandomNumber() 
	
	if body <= 0.333 then
        ObjectHideSubObjectPermanently( self, "BODY1", false )
		ObjectHideSubObjectPermanently( self, "SASH1", false )
    elseif body <= 0.666 then
        ObjectHideSubObjectPermanently( self, "BODY2", false )
		ObjectHideSubObjectPermanently( self, "SASH2", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY3", false )
		ObjectHideSubObjectPermanently( self, "SASH3", false )
    end 
	
	if head <= 0.333 then
        ObjectHideSubObjectPermanently( self, "HEAD1", false )
    elseif head <= 0.666 then
        ObjectHideSubObjectPermanently( self, "HEAD2", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD3", false )
    end 

end

function OnDunedainRangerCreated(self)

    ObjectHideSubObjectPermanently( self, "FireArowTip", true )
	ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
	ObjectHideSubObjectPermanently( self, "ARROWUP", true )
	ObjectHideSubObjectPermanently( self, "QUIVARROWUP", true )
	
	;------------------------HEADS UNHOODED
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
	ObjectHideSubObjectPermanently( self, "HEAD02", true )
	ObjectHideSubObjectPermanently( self, "HEAD03", true )
	ObjectHideSubObjectPermanently( self, "HEAD04", true )
	;------------------------HEADS HOODED
	ObjectHideSubObjectPermanently( self, "HEADHD01", true )
	ObjectHideSubObjectPermanently( self, "HEADHD02", true )
	ObjectHideSubObjectPermanently( self, "HEADHD03", true )
	ObjectHideSubObjectPermanently( self, "HEADHD04", true )
	;------------------------HEADS HOODED MASKED
	ObjectHideSubObjectPermanently( self, "HEADMSK01", true )	
	ObjectHideSubObjectPermanently( self, "HEADMSK02", true )
	ObjectHideSubObjectPermanently( self, "HEADMSK03", true )
	ObjectHideSubObjectPermanently( self, "HEADMSK04", true )
	;------------------------CLOAKS	
	ObjectHideSubObjectPermanently( self, "CLOAK01", true )
	ObjectHideSubObjectPermanently( self, "CLOAK02", true )
	ObjectHideSubObjectPermanently( self, "CLOAK03", true )
	ObjectHideSubObjectPermanently( self, "CLOAK04", true )
	ObjectHideSubObjectPermanently( self, "CLOAK05", true )
	;------------------------HOODS	
	ObjectHideSubObjectPermanently( self, "HOOD01", true )
	ObjectHideSubObjectPermanently( self, "HOOD02", true )
	ObjectHideSubObjectPermanently( self, "HOOD03", true )
	ObjectHideSubObjectPermanently( self, "HOOD04", true )
	ObjectHideSubObjectPermanently( self, "HOOD05", true )
	;------------------------BODY
	ObjectHideSubObjectPermanently( self, "BODY01", true )
	ObjectHideSubObjectPermanently( self, "BODY02", true )
	ObjectHideSubObjectPermanently( self, "BODY03", true )
	ObjectHideSubObjectPermanently( self, "BODY04", true )
	;------------------------ARMS COAT
	ObjectHideSubObjectPermanently( self, "ARMSA01", true )
	ObjectHideSubObjectPermanently( self, "ARMSA02", true )
	ObjectHideSubObjectPermanently( self, "ARMSA03", true )
	ObjectHideSubObjectPermanently( self, "ARMSA04", true )
	;------------------------ARMS SHIRT
	ObjectHideSubObjectPermanently( self, "ARMSB01", true )
	ObjectHideSubObjectPermanently( self, "ARMSB02", true )
	ObjectHideSubObjectPermanently( self, "ARMSB03", true )
	ObjectHideSubObjectPermanently( self, "ARMSB04", true )
	;------------------------EXTRA GEAR
	ObjectHideSubObjectPermanently( self, "BAG", true )
	ObjectHideSubObjectPermanently( self, "BEDROLL", true )
    
	local head	   =    GetRandomNumber()
	local head2	   =    GetRandomNumber()
    local body    =    GetRandomNumber()
	local cloak    =    GetRandomNumber()
	
	local gear   =    GetRandomNumber()
	
	if head <= 0.25 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.50 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
    elseif head <= 0.75 then
        ObjectHideSubObjectPermanently( self, "HEAD03", false )  		
    else
        ObjectHideSubObjectPermanently( self, "HEAD04", false )
    end
	
	if head2 <= 0.125 then
        ObjectHideSubObjectPermanently( self, "HEADHD01", false )
    elseif head2 <= 0.25 then
        ObjectHideSubObjectPermanently( self, "HEADHD02", false )
    elseif head2 <= 0.375 then
        ObjectHideSubObjectPermanently( self, "HEADHD03", false )
	elseif head2 <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEADHD04", false ) 
	elseif head2 <= 0.625 then
        ObjectHideSubObjectPermanently( self, "HEADMSK01", false )
	elseif head2 <= 0.75 then
        ObjectHideSubObjectPermanently( self, "HEADMSK02", false )
	elseif head2 <= 0.875 then
        ObjectHideSubObjectPermanently( self, "HEADMSK03", false )
    else
        ObjectHideSubObjectPermanently( self, "HEADMSK04", false )
    end

    if body <= 0.125 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
		ObjectHideSubObjectPermanently( self, "ARMSA01", false )
    elseif body <= 0.25 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
		ObjectHideSubObjectPermanently( self, "ARMSB01", false )
	elseif body <= 0.375 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
		ObjectHideSubObjectPermanently( self, "ARMSA02", false )
	elseif body <= 0.50 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
		ObjectHideSubObjectPermanently( self, "ARMSB02", false )
	elseif body <= 0.625 then
        ObjectHideSubObjectPermanently( self, "BODY03", false )
		ObjectHideSubObjectPermanently( self, "ARMSA03", false )
	elseif body <= 0.75 then
        ObjectHideSubObjectPermanently( self, "BODY03", false )
		ObjectHideSubObjectPermanently( self, "ARMSB03", false )	
    elseif body <= 0.875 then
        ObjectHideSubObjectPermanently( self, "BODY04", false )
		ObjectHideSubObjectPermanently( self, "ARMSA04", false )  		
    else
        ObjectHideSubObjectPermanently( self, "BODY04", false )
		ObjectHideSubObjectPermanently( self, "ARMSB04", false )
    end
	
	if cloak <= 0.20 then
        ObjectHideSubObjectPermanently( self, "CLOAK01", false )
        ObjectHideSubObjectPermanently( self, "HOOD01", false )
    elseif cloak <= 0.40 then
        ObjectHideSubObjectPermanently( self, "CLOAK02", false )
        ObjectHideSubObjectPermanently( self, "HOOD02", false )
	elseif cloak <= 0.60 then
        ObjectHideSubObjectPermanently( self, "CLOAK03", false )
        ObjectHideSubObjectPermanently( self, "HOOD03", false )
	elseif cloak <= 0.80 then
        ObjectHideSubObjectPermanently( self, "CLOAK04", false )
        ObjectHideSubObjectPermanently( self, "HOOD04", false )
     else
        ObjectHideSubObjectPermanently( self, "CLOAK05", false )
        ObjectHideSubObjectPermanently( self, "HOOD05", false )
    end 
	
	if gear <= 0.33 then
        ObjectHideSubObjectPermanently( self, "BAG", false )
    elseif gear <= 0.66 then
        ObjectHideSubObjectPermanently( self, "BEDROLL", false )
    else
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
    end
end

function OnDunedainOutriderCreated(self)

    ObjectHideSubObjectPermanently( self, "FireArowTip", true )
	ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
	ObjectHideSubObjectPermanently( self, "ARROWUP", true )
	ObjectHideSubObjectPermanently( self, "QUIVARROWUP", true )
	
	;------------------------HEADS
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
	ObjectHideSubObjectPermanently( self, "HEAD02", true )
	ObjectHideSubObjectPermanently( self, "HEAD03", true )
	ObjectHideSubObjectPermanently( self, "HEAD04", true )
	;------------------------CLOAKS	
	ObjectHideSubObjectPermanently( self, "CLOAK01", true )
	ObjectHideSubObjectPermanently( self, "CLOAK02", true )
	ObjectHideSubObjectPermanently( self, "CLOAK03", true )
	ObjectHideSubObjectPermanently( self, "CLOAK04", true )
	;------------------------BODY
	ObjectHideSubObjectPermanently( self, "BODY01", true )
	ObjectHideSubObjectPermanently( self, "BODY02", true )
	ObjectHideSubObjectPermanently( self, "BODY03", true )
	ObjectHideSubObjectPermanently( self, "BODY04", true )
	;------------------------HORSE
	ObjectHideSubObjectPermanently( self, "HORSE01", true )
	ObjectHideSubObjectPermanently( self, "HORSE02", true )
	ObjectHideSubObjectPermanently( self, "HORSE03", true )
	ObjectHideSubObjectPermanently( self, "HORSE04", true )
	;------------------------HORSE
	ObjectHideSubObjectPermanently( self, "BAG", true )
	ObjectHideSubObjectPermanently( self, "BEDROLL", true )
	ObjectHideSubObjectPermanently( self, "QUIVER", true )
	ObjectHideSubObjectPermanently( self, "SATCHEL", true )
    
	local head	   =    GetRandomNumber()
	local horse	   =    GetRandomNumber()
    local body    =    GetRandomNumber()
	local cloak    =    GetRandomNumber()
	
	local bag    =    GetRandomNumber()
	local bedroll    =    GetRandomNumber()
	local quiver    =    GetRandomNumber()
	local satchel    =    GetRandomNumber()
	
	if bag <= 0.50 then
        ObjectHideSubObjectPermanently( self, "BAG", false )
    else
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
    end
	if bedroll <= 0.50 then
        ObjectHideSubObjectPermanently( self, "BEDROLL", false )
    else
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
    end
	if quiver <= 0.50 then
        ObjectHideSubObjectPermanently( self, "QUIVER", false )
    else
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
    end
	if satchel <= 0.50 then
        ObjectHideSubObjectPermanently( self, "SATCHEL", false )
    else
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
    end
	
	if head <= 0.25 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.50 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
    elseif head <= 0.75 then
        ObjectHideSubObjectPermanently( self, "HEAD03", false )  		
    else
        ObjectHideSubObjectPermanently( self, "HEAD04", false )
    end
	
	if horse <= 0.25 then
        ObjectHideSubObjectPermanently( self, "HORSE01", false )
    elseif horse <= 0.50 then
        ObjectHideSubObjectPermanently( self, "HORSE02", false )
    elseif horse <= 0.75 then
        ObjectHideSubObjectPermanently( self, "HORSE03", false )  		
    else
        ObjectHideSubObjectPermanently( self, "HORSE04", false )
    end
	
	if body <= 0.25 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
    elseif body <= 0.50 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
    elseif body <= 0.75 then
        ObjectHideSubObjectPermanently( self, "BODY03", false )  		
    else
        ObjectHideSubObjectPermanently( self, "BODY04", false )
    end
	
	if cloak <= 0.25 then
        ObjectHideSubObjectPermanently( self, "CLOAK01", false )
    elseif cloak <= 0.50 then
        ObjectHideSubObjectPermanently( self, "CLOAK02", false )
    elseif cloak <= 0.75 then
        ObjectHideSubObjectPermanently( self, "CLOAK03", false )  		
    else
        ObjectHideSubObjectPermanently( self, "CLOAK04", false )
    end
end

function OnTookArcherCreated(self)
	ObjectHideSubObject( self, "ARROW", true )
	ObjectHideSubObjectPermanently( self, "ARROWNOCK_UP", true )
	ObjectHideSubObjectPermanently( self, "QUIVERUP", true )
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )
	
	ObjectHideSubObjectPermanently( self, "BODY01", true )
	ObjectHideSubObjectPermanently( self, "BODY02", true )
	ObjectHideSubObjectPermanently( self, "BODY03", true )
	
	ObjectHideSubObjectPermanently( self, "PIPE", true )
	
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
	ObjectHideSubObjectPermanently( self, "HEAD02", true )
	ObjectHideSubObjectPermanently( self, "HEAD03", true )
	
	ObjectHideSubObjectPermanently( self, "HAT01", true )
	ObjectHideSubObjectPermanently( self, "HAT02", true )
	ObjectHideSubObjectPermanently( self, "HAT03", true )
	
	ObjectHideSubObjectPermanently( self, "FOOT01", true )
    ObjectHideSubObjectPermanently( self, "FOOT02", true )
	
	ObjectHideSubObjectPermanently( self, "BAG01", true )
	

    local head         =    GetRandomNumber()
	local body         =    GetRandomNumber()
	local pipe         =    GetRandomNumber()
	local legs         =    GetRandomNumber()
	local bag         =    GetRandomNumber()
	local hat         =    GetRandomNumber()
	
	if bag <= 0.50 then
        ObjectHideSubObjectPermanently( self, "BAG01", false )
     else
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
    end   
	
	if pipe <= 0.50 then
        ObjectHideSubObjectPermanently( self, "PIPE", false )
     else
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
    end   
	
	if legs <= 0.50 then
        ObjectHideSubObjectPermanently( self, "FOOT01", false )
     else
        ObjectHideSubObjectPermanently( self, "FOOT02", false )
    end   
	
	if body <= 0.333 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
    elseif body <= 0.666 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY03", false )
    end 
	
	if head <= 0.333 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.666 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end 
	
	if hat <= 0.25 then
        ObjectHideSubObjectPermanently( self, "HAT01", false )
	elseif hat <= 0.50 then
        ObjectHideSubObjectPermanently( self, "HAT02", false )
	elseif hat <= 0.75 then
        ObjectHideSubObjectPermanently( self, "HAT03", false )
     else
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
    end 

end

function OnBreelandTownsguardCreated(self)
	ObjectHideSubObject( self, "ARROW", true )
	ObjectHideSubObject( self, "ARROWNOCKUP", true )
	ObjectHideSubObject( self, "QUIVERUP", true )
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )
	
	ObjectHideSubObjectPermanently( self, "BODY01", true )
	ObjectHideSubObjectPermanently( self, "BODY02", true )
	ObjectHideSubObjectPermanently( self, "BODY03", true )
	ObjectHideSubObjectPermanently( self, "BODY04", true )
	
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
	ObjectHideSubObjectPermanently( self, "HEAD02", true )
	ObjectHideSubObjectPermanently( self, "HEAD03", true )
	ObjectHideSubObjectPermanently( self, "HEAD04", true )
	ObjectHideSubObjectPermanently( self, "HEAD05", true )
	
	ObjectHideSubObjectPermanently( self, "HELMET01", true )
	ObjectHideSubObjectPermanently( self, "HELMET02", true )
	ObjectHideSubObjectPermanently( self, "HELMET03", true )
	
	ObjectHideSubObjectPermanently( self, "LEGS01", true )
    ObjectHideSubObjectPermanently( self, "LEGS02", true )
	

    local head         =    GetRandomNumber()
	local body         =    GetRandomNumber()
	local legs         =    GetRandomNumber()
	local helm         =    GetRandomNumber()
	
	if legs <= 0.50 then
        ObjectHideSubObjectPermanently( self, "LEGS01", false )
     else
        ObjectHideSubObjectPermanently( self, "LEGS02", false )
    end   
	
	if helm <= 0.333 then
        ObjectHideSubObjectPermanently( self, "HELMET01", false )
    elseif helm <= 0.666 then
        ObjectHideSubObjectPermanently( self, "HELMET02", false )
     else
        ObjectHideSubObjectPermanently( self, "HELMET03", false )
    end 
	
	if head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
    elseif head <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    elseif head <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HEAD04", false )
	else
        ObjectHideSubObjectPermanently( self, "HEAD05", false )
    end  
	
	if body <= 0.25 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
	elseif body <= 0.50 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
	elseif body <= 0.75 then
        ObjectHideSubObjectPermanently( self, "BODY03", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY04", false )
    end 

end

function OnRivendellWarriorCreated(self)
	ObjectHideSubObject( self, "ARROW", true )
	ObjectHideSubObject( self, "ARROWNOCKUP", true )
	ObjectHideSubObject( self, "QUIVERUP", true )
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )
	ObjectHideSubObjectPermanently( self, "BANNER01", true )
	ObjectHideSubObjectPermanently( self, "BANNER02", true )
	ObjectHideSubObjectPermanently( self, "BANNER03", true )
	
	ObjectHideSubObjectPermanently( self, "REGELF01", true )
	ObjectHideSubObjectPermanently( self, "REGELF02", true )
	ObjectHideSubObjectPermanently( self, "REGELF03", true )
	ObjectHideSubObjectPermanently( self, "REGELF04", true )
	ObjectHideSubObjectPermanently( self, "HAELF01", true )
	ObjectHideSubObjectPermanently( self, "HAELF02", true )
	ObjectHideSubObjectPermanently( self, "HAELF03", true )
	ObjectHideSubObjectPermanently( self, "HAELF04", true )

    ObjectHideSubObjectPermanently( self, "HEADBLACK", true )
	ObjectHideSubObjectPermanently( self, "HEADBLACKHA", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK1", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK1HA", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK2", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK2HA", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK3", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK3HA", true )
	ObjectHideSubObjectPermanently( self, "HEADBROWN", true )
    ObjectHideSubObjectPermanently( self, "HEADBROWN1", true )
    ObjectHideSubObjectPermanently( self, "HEADBROWN1HA", true )
	ObjectHideSubObjectPermanently( self, "HEADBROWNHA", true )
	ObjectHideSubObjectPermanently( self, "HEADGRAY", true )
    ObjectHideSubObjectPermanently( self, "HEADGRAY1", true )
    ObjectHideSubObjectPermanently( self, "HEADGRAY1HA", true )
	ObjectHideSubObjectPermanently( self, "HEADGRAYHA", true )

    local head         =    GetRandomNumber()
	local body         =    GetRandomNumber()
	local banner         =    GetRandomNumber()

    if head <= 0.1 then
        ObjectHideSubObjectPermanently( self, "HEADBLACK", false )
		ObjectHideSubObjectPermanently( self, "HEADBLACKHA", false )
    elseif head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "HEADBLACK1", false )
		ObjectHideSubObjectPermanently( self, "HEADBLACK1HA", false )
	elseif head <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HEADBLACK2", false )
		ObjectHideSubObjectPermanently( self, "HEADBLACK2HA", false )
	elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEADBLACK3", false )
		ObjectHideSubObjectPermanently( self, "HEADBLACK3HA", false )
	elseif head <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HEADBROWN", false )
		ObjectHideSubObjectPermanently( self, "HEADBROWNHA", false )
	elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEADBROWN1", false )
		ObjectHideSubObjectPermanently( self, "HEADBROWN1HA", false )
	elseif head <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HEADGRAY", false )
		ObjectHideSubObjectPermanently( self, "HEADGRAYHA", false )
     else
        ObjectHideSubObjectPermanently( self, "HEADGRAY1", false )
		ObjectHideSubObjectPermanently( self, "HEADGRAY1HA", false )
    end   
	
	if body <= 0.25 then
        ObjectHideSubObjectPermanently( self, "REGELF01", false )
		ObjectHideSubObjectPermanently( self, "HAELF01", false )
	elseif body <= 0.50 then
        ObjectHideSubObjectPermanently( self, "REGELF02", false )
		ObjectHideSubObjectPermanently( self, "HAELF02", false )
	elseif body <= 0.75 then
        ObjectHideSubObjectPermanently( self, "REGELF03", false )
		ObjectHideSubObjectPermanently( self, "HAELF03", false )
     else
        ObjectHideSubObjectPermanently( self, "REGELF04", false )
		ObjectHideSubObjectPermanently( self, "HAELF04", false )
    end 

	if banner <= 0.4 then
        ObjectHideSubObjectPermanently( self, "BANNER01", false )
	elseif banner <= 0.8 then
        ObjectHideSubObjectPermanently( self, "BANNER02", false )
     else
        ObjectHideSubObjectPermanently( self, "BANNER03", false )
    end 

end

function OnRivendellWarriorMountCreated(self)
	ObjectHideSubObject( self, "ARROW", true )
	ObjectHideSubObject( self, "ARROWNOCKUP", true )
	ObjectHideSubObject( self, "QUIVERUP", true )
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )

	ObjectHideSubObjectPermanently( self, "HORSE01", true )
	ObjectHideSubObjectPermanently( self, "HORSE02", true )
	ObjectHideSubObjectPermanently( self, "HORSE03", true )
	ObjectHideSubObjectPermanently( self, "HORSE04", true )
	
	ObjectHideSubObjectPermanently( self, "REGELF01", true )
	ObjectHideSubObjectPermanently( self, "REGELF02", true )
	ObjectHideSubObjectPermanently( self, "REGELF03", true )
	ObjectHideSubObjectPermanently( self, "REGELF04", true )
	ObjectHideSubObjectPermanently( self, "HAELF01", true )
	ObjectHideSubObjectPermanently( self, "HAELF02", true )
	ObjectHideSubObjectPermanently( self, "HAELF03", true )
	ObjectHideSubObjectPermanently( self, "HAELF04", true )

    ObjectHideSubObjectPermanently( self, "HEADBLACK", true )
	ObjectHideSubObjectPermanently( self, "HEADBLACKHA", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK1", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK1HA", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK2", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK2HA", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK3", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK3HA", true )
	ObjectHideSubObjectPermanently( self, "HEADBROWN", true )
    ObjectHideSubObjectPermanently( self, "HEADBROWN1", true )
    ObjectHideSubObjectPermanently( self, "HEADBROWN1HA", true )
	ObjectHideSubObjectPermanently( self, "HEADBROWNHA", true )
	ObjectHideSubObjectPermanently( self, "HEADGRAY", true )
    ObjectHideSubObjectPermanently( self, "HEADGRAY1", true )
    ObjectHideSubObjectPermanently( self, "HEADGRAY1HA", true )
	ObjectHideSubObjectPermanently( self, "HEADGRAYHA", true )

    local head         =    GetRandomNumber()
    local horse         =    GetRandomNumber()
	local body         =    GetRandomNumber()
	local banner         =    GetRandomNumber()

    if head <= 0.1 then
        ObjectHideSubObjectPermanently( self, "HEADBLACK", false )
		ObjectHideSubObjectPermanently( self, "HEADBLACKHA", false )
    elseif head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "HEADBLACK1", false )
		ObjectHideSubObjectPermanently( self, "HEADBLACK1HA", false )
	elseif head <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HEADBLACK2", false )
		ObjectHideSubObjectPermanently( self, "HEADBLACK2HA", false )
	elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEADBLACK3", false )
		ObjectHideSubObjectPermanently( self, "HEADBLACK3HA", false )
	elseif head <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HEADBROWN", false )
		ObjectHideSubObjectPermanently( self, "HEADBROWNHA", false )
	elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEADBROWN1", false )
		ObjectHideSubObjectPermanently( self, "HEADBROWN1HA", false )
	elseif head <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HEADGRAY", false )
		ObjectHideSubObjectPermanently( self, "HEADGRAYHA", false )
     else
        ObjectHideSubObjectPermanently( self, "HEADGRAY1", false )
		ObjectHideSubObjectPermanently( self, "HEADGRAY1HA", false )
    end   
	
	if body <= 0.25 then
        ObjectHideSubObjectPermanently( self, "REGELF01", false )
		ObjectHideSubObjectPermanently( self, "HAELF01", false )
	elseif body <= 0.50 then
        ObjectHideSubObjectPermanently( self, "REGELF02", false )
		ObjectHideSubObjectPermanently( self, "HAELF02", false )
	elseif body <= 0.75 then
        ObjectHideSubObjectPermanently( self, "REGELF03", false )
		ObjectHideSubObjectPermanently( self, "HAELF03", false )
     else
        ObjectHideSubObjectPermanently( self, "REGELF04", false )
		ObjectHideSubObjectPermanently( self, "HAELF04", false )
    end 

	if horse <= 0.25 then
        ObjectHideSubObjectPermanently( self, "HORSE01", false )
	elseif horse <= 0.50 then
        ObjectHideSubObjectPermanently( self, "HORSE02", false )
	elseif horse <= 0.75 then
        ObjectHideSubObjectPermanently( self, "HORSE03", false )
     else
        ObjectHideSubObjectPermanently( self, "HORSE04", false )
    end 
	
	if banner <= 0.4 then
        ObjectHideSubObjectPermanently( self, "BANNER01", false )
	elseif banner <= 0.8 then
        ObjectHideSubObjectPermanently( self, "BANNER02", false )
     else
        ObjectHideSubObjectPermanently( self, "BANNER03", false )
    end 

end

function OnLANoldorBannerCreated(self)
	ObjectHideSubObject( self, "ARROW", true )
	ObjectHideSubObject( self, "ARROWNOCKUP", true )
	ObjectHideSubObject( self, "QUIVERUP", true )
	ObjectHideSubObject( self, "SHEATHED", true )
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )

    -- ObjectHideSubObjectPermanently( self, "HEADBLACK", true )
	-- ObjectHideSubObjectPermanently( self, "HEADBLACKHA", true )
    -- ObjectHideSubObjectPermanently( self, "HEADBLACK1", true )
    -- ObjectHideSubObjectPermanently( self, "HEADBLACK1HA", true )
    -- ObjectHideSubObjectPermanently( self, "HEADBLACK2", true )
    -- ObjectHideSubObjectPermanently( self, "HEADBLACK2HA", true )
    -- ObjectHideSubObjectPermanently( self, "HEADBLACK3", true )
    -- ObjectHideSubObjectPermanently( self, "HEADBLACK3HA", true )
	-- ObjectHideSubObjectPermanently( self, "HEADBROWN", true )
    -- ObjectHideSubObjectPermanently( self, "HEADBROWN1", true )
    -- ObjectHideSubObjectPermanently( self, "HEADBROWN1HA", true )
	-- ObjectHideSubObjectPermanently( self, "HEADBROWNHA", true )
	-- ObjectHideSubObjectPermanently( self, "HEADGRAY", true )
    -- ObjectHideSubObjectPermanently( self, "HEADGRAY1", true )
    -- ObjectHideSubObjectPermanently( self, "HEADGRAY1HA", true )
	-- ObjectHideSubObjectPermanently( self, "HEADGRAYHA", true )

    -- local head         =    GetRandomNumber()

    -- if head <= 0.1 then
        -- ObjectHideSubObjectPermanently( self, "HEADBLACK", false )
    -- elseif head <= 0.2 then
        -- ObjectHideSubObjectPermanently( self, "HEADBLACK1", false )
	-- elseif head <= 0.4 then
        -- ObjectHideSubObjectPermanently( self, "HEADBLACKHA", false )
	-- elseif head <= 0.5 then
        -- ObjectHideSubObjectPermanently( self, "HEADBLACK1HA", false )
	-- elseif head <= 0.6 then
        -- ObjectHideSubObjectPermanently( self, "HEADBROWN", false )
	-- elseif head <= 0.7 then
        -- ObjectHideSubObjectPermanently( self, "HEADBROWNHA", false )
	-- elseif head <= 0.8 then
        -- ObjectHideSubObjectPermanently( self, "HEADGRAY", false )
     -- else
        -- ObjectHideSubObjectPermanently( self, "HEADGRAYHA", false )
    -- end

end

function OnLANoldorCreated(self)
	ObjectHideSubObject( self, "ARROW", true )
	ObjectHideSubObject( self, "ARROWNOCKUP", true )
	ObjectHideSubObject( self, "QUIVERUP", true )
	ObjectHideSubObject( self, "SHEATHED", true )
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )

    ObjectHideSubObjectPermanently( self, "HEADBLACK", true )
	ObjectHideSubObjectPermanently( self, "HEADBLACKHA", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK1", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK1HA", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK2", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK2HA", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK3", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK3HA", true )
	ObjectHideSubObjectPermanently( self, "HEADBROWN", true )
    ObjectHideSubObjectPermanently( self, "HEADBROWN1", true )
    ObjectHideSubObjectPermanently( self, "HEADBROWN1HA", true )
	ObjectHideSubObjectPermanently( self, "HEADBROWNHA", true )
	ObjectHideSubObjectPermanently( self, "HEADGRAY", true )
    ObjectHideSubObjectPermanently( self, "HEADGRAY1", true )
    ObjectHideSubObjectPermanently( self, "HEADGRAY1HA", true )
	ObjectHideSubObjectPermanently( self, "HEADGRAYHA", true )

    local head         =    GetRandomNumber()

    if head <= 0.1 then
        ObjectHideSubObjectPermanently( self, "HEADBLACK", false )
		ObjectHideSubObjectPermanently( self, "HEADBLACKHA", false )
    elseif head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "HEADBLACK1", false )
		ObjectHideSubObjectPermanently( self, "HEADBLACK1HA", false )
	elseif head <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HEADBLACK2", false )
		ObjectHideSubObjectPermanently( self, "HEADBLACK2HA", false )
	elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEADBLACK3", false )
		ObjectHideSubObjectPermanently( self, "HEADBLACK3HA", false )
	elseif head <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HEADBROWN", false )
		ObjectHideSubObjectPermanently( self, "HEADBROWNHA", false )
	elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEADBROWN1", false )
		ObjectHideSubObjectPermanently( self, "HEADBROWN1HA", false )
	elseif head <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HEADGRAY", false )
		ObjectHideSubObjectPermanently( self, "HEADGRAYHA", false )
     else
        ObjectHideSubObjectPermanently( self, "HEADGRAY1", false )
		ObjectHideSubObjectPermanently( self, "HEADGRAY1HA", false )
    end

end

function OnRivendellNoldorCreated(self)
	ObjectHideSubObject( self, "ARROW", true )
	ObjectHideSubObject( self, "ARROWNOCKUP", true )
	ObjectHideSubObject( self, "QUIVERUP", true )
	ObjectHideSubObject( self, "SHEATHED", true )

    ObjectHideSubObjectPermanently( self, "HEADBLACK", true )
	ObjectHideSubObjectPermanently( self, "HEADBLACKHA", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK1", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK1HA", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK2", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK2HA", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK3", true )
    ObjectHideSubObjectPermanently( self, "HEADBLACK3HA", true )
	ObjectHideSubObjectPermanently( self, "HEADBROWN", true )
    ObjectHideSubObjectPermanently( self, "HEADBROWN1", true )
    ObjectHideSubObjectPermanently( self, "HEADBROWN1HA", true )
	ObjectHideSubObjectPermanently( self, "HEADBROWNHA", true )
	ObjectHideSubObjectPermanently( self, "HEADGRAY", true )
    ObjectHideSubObjectPermanently( self, "HEADGRAY1", true )
    ObjectHideSubObjectPermanently( self, "HEADGRAY1HA", true )
	ObjectHideSubObjectPermanently( self, "HEADGRAYHA", true )

    local head         =    GetRandomNumber()

    if head <= 0.1 then
        ObjectHideSubObjectPermanently( self, "HEADBLACK", false )
		ObjectHideSubObjectPermanently( self, "HEADBLACKHA", false )
    elseif head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "HEADBLACK1", false )
		ObjectHideSubObjectPermanently( self, "HEADBLACK1HA", false )
	elseif head <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HEADBLACK2", false )
		ObjectHideSubObjectPermanently( self, "HEADBLACK2HA", false )
	elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEADBLACK3", false )
		ObjectHideSubObjectPermanently( self, "HEADBLACK3HA", false )
	elseif head <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HEADBROWN", false )
		ObjectHideSubObjectPermanently( self, "HEADBROWNHA", false )
	elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEADBROWN1", false )
		ObjectHideSubObjectPermanently( self, "HEADBROWN1HA", false )
	elseif head <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HEADGRAY", false )
		ObjectHideSubObjectPermanently( self, "HEADGRAYHA", false )
     else
        ObjectHideSubObjectPermanently( self, "HEADGRAY1", false )
		ObjectHideSubObjectPermanently( self, "HEADGRAY1HA", false )
    end

end



function OnLorienInfantryBannerCreated(self)
	ObjectHideSubObjectPermanently( self, "Glow", true )
    ObjectHideSubObjectPermanently( self, "HEAD1", true )
    ObjectHideSubObjectPermanently( self, "HEAD2", true )
    ObjectHideSubObjectPermanently( self, "HEAD3", true )
    ObjectHideSubObjectPermanently( self, "BANNER1", true )
    ObjectHideSubObjectPermanently( self, "BANNER2", true )
    ObjectHideSubObjectPermanently( self, "BANNER3", true )

    local helmet         =    GetRandomNumber()
	local banner         =    GetRandomNumber()	

    if helmet <= 0.333 then
        ObjectHideSubObjectPermanently( self, "HEAD1", false )
    elseif helmet <= 0.666 then
        ObjectHideSubObjectPermanently( self, "HEAD2", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD3", false )
    end  
	
    if banner <= 0.333 then
        ObjectHideSubObjectPermanently( self, "BANNER1", false )
    elseif banner <= 0.666 then
        ObjectHideSubObjectPermanently( self, "BANNER2", false )
     else
        ObjectHideSubObjectPermanently( self, "BANNER3", false )
    end  
end

function OnLorienWarriorCreated(self)
	ObjectHideSubObject( self, "ARROW", true )
	ObjectHideSubObject( self, "ARROWNOCK", true )
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )
	ObjectHideSubObjectPermanently( self, "MALLORNBOW", true )
	
	ObjectHideSubObjectPermanently( self, "BODY1", true )
	ObjectHideSubObjectPermanently( self, "BODY2", true )
	ObjectHideSubObjectPermanently( self, "BODY3", true )
	ObjectHideSubObjectPermanently( self, "BODYHA", true )
	ObjectHideSubObjectPermanently( self, "LEGSHA", true )
	
	ObjectHideSubObjectPermanently( self, "SASHHA", true )
	ObjectHideSubObjectPermanently( self, "CHESTSHASHHA", true )
	
	ObjectHideSubObjectPermanently( self, "SASH", true )

    ObjectHideSubObjectPermanently( self, "HEAD1", true )
    ObjectHideSubObjectPermanently( self, "HEAD2", true )
    ObjectHideSubObjectPermanently( self, "HEAD3", true )

    local helmet         =    GetRandomNumber()
	
	local body         =    GetRandomNumber()
	
	local sash         =    GetRandomNumber()

    if helmet <= 0.333 then
        ObjectHideSubObjectPermanently( self, "HEAD1", false )
    elseif helmet <= 0.666 then
        ObjectHideSubObjectPermanently( self, "HEAD2", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD3", false )
    end  

	if body <= 0.333 then
        ObjectHideSubObjectPermanently( self, "BODY1", false )
    elseif body <= 0.666 then
        ObjectHideSubObjectPermanently( self, "BODY2", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY3", false )
    end  

	if sash <= 0.5 then
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
     else
        ObjectHideSubObjectPermanently( self, "SASH", false )
    end	

end

function OnLorienStalkerCreated(self)
	ObjectHideSubObject( self, "ARROW", true )
	ObjectHideSubObject( self, "ARROWNOCK", true )
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
end

function OnLorienMarchwardenCreated(self)
	ObjectHideSubObject( self, "ARROW", true )
	ObjectHideSubObject( self, "ARROWNOCK", true )
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )
	ObjectHideSubObjectPermanently( self, "MALLORNBOW", true )
	
	ObjectHideSubObjectPermanently( self, "BODY1", true )
	ObjectHideSubObjectPermanently( self, "BODY2", true )
	ObjectHideSubObjectPermanently( self, "BODY3", true )
	ObjectHideSubObjectPermanently( self, "BODYHA", true )
	ObjectHideSubObjectPermanently( self, "CLOAKHA", true )
	
	ObjectHideSubObjectPermanently( self, "SHOULDER", true )
	ObjectHideSubObjectPermanently( self, "VAMBRACES", true )
	ObjectHideSubObjectPermanently( self, "HELMET", true )
	
	ObjectHideSubObjectPermanently( self, "SHAWL", true )

    ObjectHideSubObjectPermanently( self, "HEAD1", true )
    ObjectHideSubObjectPermanently( self, "HEAD2", true )
    ObjectHideSubObjectPermanently( self, "HEAD3", true )

    local helmet         =    GetRandomNumber()
	
	local body         =    GetRandomNumber()
	
	local sash         =    GetRandomNumber()

    if helmet <= 0.333 then
        ObjectHideSubObjectPermanently( self, "HEAD1", false )
    elseif helmet <= 0.666 then
        ObjectHideSubObjectPermanently( self, "HEAD2", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD3", false )
    end  

	if body <= 0.333 then
        ObjectHideSubObjectPermanently( self, "BODY1", false )
    elseif body <= 0.666 then
        ObjectHideSubObjectPermanently( self, "BODY2", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY3", false )
    end  

	if sash <= 0.5 then
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
     else
        ObjectHideSubObjectPermanently( self, "SHAWL", false )
    end		

end

function OnLorienSentinelCreated(self)
	ObjectHideSubObject( self, "ARROW", true )
	ObjectHideSubObject( self, "ARROWNOCK", true )
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )
	ObjectHideSubObjectPermanently( self, "MALLORNBOW", true )
	
	ObjectHideSubObjectPermanently( self, "BODY1", true )
	ObjectHideSubObjectPermanently( self, "BODY2", true )
	ObjectHideSubObjectPermanently( self, "BODY3", true )
	ObjectHideSubObjectPermanently( self, "BODYHA", true )
	
	ObjectHideSubObjectPermanently( self, "VAMBRACES", true )
	
	ObjectHideSubObjectPermanently( self, "SHAWL", true )

    ObjectHideSubObjectPermanently( self, "HEAD1", true )
    ObjectHideSubObjectPermanently( self, "HEAD2", true )
    ObjectHideSubObjectPermanently( self, "HEAD3", true )

    local helmet         =    GetRandomNumber()
	
	local body         =    GetRandomNumber()
	
	local sash         =    GetRandomNumber()

    if helmet <= 0.333 then
        ObjectHideSubObjectPermanently( self, "HEAD1", false )
    elseif helmet <= 0.666 then
        ObjectHideSubObjectPermanently( self, "HEAD2", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD3", false )
    end  

	if body <= 0.333 then
        ObjectHideSubObjectPermanently( self, "BODY1", false )
    elseif body <= 0.666 then
        ObjectHideSubObjectPermanently( self, "BODY2", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY3", false )
    end  

	if sash <= 0.5 then
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
     else
        ObjectHideSubObjectPermanently( self, "SHAWL", false )
    end		

end

function OnLorienNimrodelCreated(self)
	ObjectHideSubObject( self, "ARROW", true )
	ObjectHideSubObject( self, "ARROWNOCK", true )
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )
	ObjectHideSubObjectPermanently( self, "MALLORNBOW", true )
	ObjectHideSubObjectPermanently( self, "BODY1", true )
	ObjectHideSubObjectPermanently( self, "BODY2", true )
	ObjectHideSubObjectPermanently( self, "BODY3", true )
	ObjectHideSubObjectPermanently( self, "BODYHA", true )
	ObjectHideSubObjectPermanently( self, "SHOULDER", true )
	ObjectHideSubObjectPermanently( self, "VAMBRACES", true )
	ObjectHideSubObjectPermanently( self, "HELMET", true )
	ObjectHideSubObjectPermanently( self, "HOODHA", true )
	ObjectHideSubObjectPermanently( self, "SHAWL", true )
    ObjectHideSubObjectPermanently( self, "HEAD1", true )
    ObjectHideSubObjectPermanently( self, "HEAD2", true )
    ObjectHideSubObjectPermanently( self, "HEAD3", true )
	ObjectHideSubObjectPermanently( self, "HORSE1", true )
    ObjectHideSubObjectPermanently( self, "HORSE2", true )
    ObjectHideSubObjectPermanently( self, "HORSE3", true )
    ObjectHideSubObjectPermanently( self, "BANNER1", true )
    ObjectHideSubObjectPermanently( self, "BANNER2", true )
    ObjectHideSubObjectPermanently( self, "BANNER3", true )

	local banner         =    GetRandomNumber()
    local helmet         =    GetRandomNumber()
	local body         =    GetRandomNumber()
	local horse         =    GetRandomNumber()

    if helmet <= 0.333 then
        ObjectHideSubObjectPermanently( self, "HEAD1", false )
    elseif helmet <= 0.666 then
        ObjectHideSubObjectPermanently( self, "HEAD2", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD3", false )
    end  

	if body <= 0.333 then
        ObjectHideSubObjectPermanently( self, "BODY1", false )
    elseif body <= 0.666 then
        ObjectHideSubObjectPermanently( self, "BODY2", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY3", false )
    end  

	if horse <= 0.333 then
        ObjectHideSubObjectPermanently( self, "HORSE1", false )
    elseif horse <= 0.666 then
        ObjectHideSubObjectPermanently( self, "HORSE2", false )
     else
        ObjectHideSubObjectPermanently( self, "HORSE3", false )
    end  
	
    if banner <= 0.333 then
        ObjectHideSubObjectPermanently( self, "BANNER1", false )
    elseif banner <= 0.666 then
        ObjectHideSubObjectPermanently( self, "BANNER2", false )
     else
        ObjectHideSubObjectPermanently( self, "BANNER3", false )
    end 

end

function OnLorienSilverTreeCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )

    ObjectHideSubObjectPermanently( self, "HEAD1", true )
    ObjectHideSubObjectPermanently( self, "HEAD2", true )
    ObjectHideSubObjectPermanently( self, "HEAD3", true )
	

    local helmet         =    GetRandomNumber()

    if helmet <= 0.333 then
        ObjectHideSubObjectPermanently( self, "HEAD1", false )
    elseif helmet <= 0.666 then
        ObjectHideSubObjectPermanently( self, "HEAD2", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD3", false )
    end

end

function OnLorienNandorCreated(self)
	ObjectHideSubObject( self, "ARROW", true )
	ObjectHideSubObject( self, "ARROWNOCK", true )
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )

    ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
	
	ObjectHideSubObjectPermanently( self, "PLUME01", true )
    ObjectHideSubObjectPermanently( self, "PLUME02", true )
    ObjectHideSubObjectPermanently( self, "PLUME03", true )

    local helmet         =    GetRandomNumber()
	
	local plume         =    GetRandomNumber()

    if helmet <= 0.333 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif helmet <= 0.666 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end
	
	if plume <= 0.333 then
        ObjectHideSubObjectPermanently( self, "PLUME01", false )
    elseif plume <= 0.666 then
        ObjectHideSubObjectPermanently( self, "PLUME02", false )
     else
        ObjectHideSubObjectPermanently( self, "PLUME03", false )
    end

end

function OnRegularMirkwoodWarriorCreated(self)
    ObjectHideSubObjectPermanently( self, "QUIVERU", true )
    ObjectHideSubObjectPermanently( self, "ARROWNOCKU", true )
	ObjectHideSubObject( self, "ARROWNOCK", true )
    ObjectHideSubObjectPermanently( self, "SILVERPAULTOP", true )
    ObjectHideSubObjectPermanently( self, "SILVERPAULBOT", true )
    ObjectHideSubObjectPermanently( self, "SILVERBODY", true )
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )
    ObjectHideSubObjectPermanently( self, "SILVERHELMET", true )
    ObjectHideSubObjectPermanently( self, "HELMET", true )
    ObjectHideSubObjectPermanently( self, "HELMHED1", true )
    ObjectHideSubObjectPermanently( self, "HELMHED2", true )
    ObjectHideSubObjectPermanently( self, "HELMHED3", true )
    ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )

    local head         =    GetRandomNumber()

    if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
        ObjectHideSubObjectPermanently( self, "HELMET", false )
    elseif head <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
        ObjectHideSubObjectPermanently( self, "HELMET", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
        ObjectHideSubObjectPermanently( self, "HELMET", false )
    end  

end

function OnSilverBannerMirkwoodWarriorCreated(self)
    ObjectHideSubObjectPermanently( self, "ARROWHOLDERUP", true )
    ObjectHideSubObjectPermanently( self, "ARROWNOCKUP", true )
	ObjectHideSubObject( self, "ARROWNOCK", true )
    ObjectHideSubObjectPermanently( self, "PAULDRONTOP", true )
    ObjectHideSubObjectPermanently( self, "PAULDRONSBOTTOM", true )
    ObjectHideSubObjectPermanently( self, "BODY", true )
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )
    ObjectHideSubObjectPermanently( self, "SILVERHELMET", true )
    ObjectHideSubObjectPermanently( self, "HELMET", true )
    ObjectHideSubObjectPermanently( self, "HELMHED1", true )
    ObjectHideSubObjectPermanently( self, "HELMHED2", true )
    ObjectHideSubObjectPermanently( self, "HELMHED3", true )
    ObjectHideSubObjectPermanently( self, "HEAD1", true )
    ObjectHideSubObjectPermanently( self, "HEAD2", true )
    ObjectHideSubObjectPermanently( self, "HEAD3", true )

    local head         =    GetRandomNumber()

    if head <= 0.1 then
        ObjectHideSubObjectPermanently( self, "HEAD1", false )
    elseif head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD2", false )
    elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEAD3", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HELMHED1", false )
        ObjectHideSubObjectPermanently( self, "SILVERHELMET", false )
    elseif head <= 0.9 then
        ObjectHideSubObjectPermanently( self, "HELMHED2", false )
        ObjectHideSubObjectPermanently( self, "SILVERHELMET", false )
     else
        ObjectHideSubObjectPermanently( self, "HELMHED3", false )
        ObjectHideSubObjectPermanently( self, "SILVERHELMET", false )
    end  

end

-- each gender has 2 w3d files so I seperated local entries required
function OnRegularMirkwoodSilvanBannerCreated(self)
-- weapons, gear, e.t.c. objects
    ObjectHideSubObjectPermanently( self, "QUIVERHA", true )
    ObjectHideSubObjectPermanently( self, "ARROWQUIVUP", true )
    ObjectHideSubObjectPermanently( self, "ARROWNOCKUP", true )
    ObjectHideSubObjectPermanently( self, "BOW2", true )
    ObjectHideSubObjectPermanently( self, "BOW1", true )
	ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
-- regular ha objects
--  ObjectHideSubObjectPermanently( self, "BODYMHOODHA", true )
--	ObjectHideSubObjectPermanently( self, "LEGGUARDSM", true )
--  ObjectHideSubObjectPermanently( self, "ARMSMHA", true )
--  ObjectHideSubObjectPermanently( self, "HOODMHA", true )
--  ObjectHideSubObjectPermanently( self, "BODYMHA", true )
--  ObjectHideSubObjectPermanently( self, "HOODMDOWNHA", true )
-- female ha objects
--  ObjectHideSubObjectPermanently( self, "BODYFHOODHA", true )
--	ObjectHideSubObjectPermanently( self, "LEGGUARDSF", true )
--  ObjectHideSubObjectPermanently( self, "ARMSFHA", true )
--  ObjectHideSubObjectPermanently( self, "HOODFHA", true )
--  ObjectHideSubObjectPermanently( self, "BODYFHA", true )
--  ObjectHideSubObjectPermanently( self, "HOODFDOWNHA", true )
-- normal heads
    ObjectHideSubObjectPermanently( self, "HEADM1", true )
    ObjectHideSubObjectPermanently( self, "HEADM2", true )
    ObjectHideSubObjectPermanently( self, "HEADM3", true )
-- heads for hoods
    ObjectHideSubObjectPermanently( self, "HEADMHOOD1", true )
    ObjectHideSubObjectPermanently( self, "HEADMHOOD2", true )
    ObjectHideSubObjectPermanently( self, "HEADMHOOD3", true )
-- normal female heads
    ObjectHideSubObjectPermanently( self, "HEADF1", true )
    ObjectHideSubObjectPermanently( self, "HEADF2", true )
    ObjectHideSubObjectPermanently( self, "HEADF3", true )
-- female heads for hoods
    ObjectHideSubObjectPermanently( self, "HEADFHOOD1", true )
    ObjectHideSubObjectPermanently( self, "HEADFHOOD2", true )
    ObjectHideSubObjectPermanently( self, "HEADFHOOD3", true )
-- normal bodies
    ObjectHideSubObjectPermanently( self, "BODYM1", true )
    ObjectHideSubObjectPermanently( self, "BODYM2", true )
    ObjectHideSubObjectPermanently( self, "BODYM3", true )
    ObjectHideSubObjectPermanently( self, "HOODMDOWN1", true )
    ObjectHideSubObjectPermanently( self, "HOODMDOWN2", true )
    ObjectHideSubObjectPermanently( self, "HOODMDOWN3", true )
-- heads for bodies
    ObjectHideSubObjectPermanently( self, "BODYMHOOD1", true )
    ObjectHideSubObjectPermanently( self, "BODYMHOOD2", true )
    ObjectHideSubObjectPermanently( self, "BODYMHOOD3", true )
    ObjectHideSubObjectPermanently( self, "HOODM1", true )
    ObjectHideSubObjectPermanently( self, "HOODM2", true )
    ObjectHideSubObjectPermanently( self, "HOODM3", true )
-- normal female bodies
    ObjectHideSubObjectPermanently( self, "BODYF1", true )
    ObjectHideSubObjectPermanently( self, "BODYF2", true )
    ObjectHideSubObjectPermanently( self, "BODYF3", true )
    ObjectHideSubObjectPermanently( self, "HOODFDOWN1", true )
    ObjectHideSubObjectPermanently( self, "HOODFDOWN2", true )
    ObjectHideSubObjectPermanently( self, "HOODFDOWN3", true )
-- female heads for bodies
    ObjectHideSubObjectPermanently( self, "BODYFHOOD1", true )
    ObjectHideSubObjectPermanently( self, "BODYFHOOD2", true )
    ObjectHideSubObjectPermanently( self, "BODYFHOOD3", true )
    ObjectHideSubObjectPermanently( self, "HOODF1", true )
    ObjectHideSubObjectPermanently( self, "HOODF2", true )
    ObjectHideSubObjectPermanently( self, "HOODF3", true )

    local mheads1         =    GetRandomNumber()
    local fheads1         =    GetRandomNumber()
	
    local bow         =    GetRandomNumber()

    if mheads1 <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HEADM1", false )
        ObjectHideSubObjectPermanently( self, "HEADMHOOD1", false )
    elseif mheads1 <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEADM2", false )
        ObjectHideSubObjectPermanently( self, "HEADMHOOD2", false )
     else
        ObjectHideSubObjectPermanently( self, "HEADM3", false )
        ObjectHideSubObjectPermanently( self, "HEADMHOOD3", false )
    end  
	
    if fheads1 <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HEADF1", false )
        ObjectHideSubObjectPermanently( self, "HEADFHOOD1", false )
    elseif fheads1 <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEADF2", false )
        ObjectHideSubObjectPermanently( self, "HEADFHOOD2", false )
     else
        ObjectHideSubObjectPermanently( self, "HEADF3", false )
        ObjectHideSubObjectPermanently( self, "HEADFHOOD3", false )
    end 
end

-- each gender has 2 w3d files so I seperated local entries required
function OnRegularMirkwoodSilvanArcherCreated(self)
-- weapons, gear, e.t.c. objects
    ObjectHideSubObjectPermanently( self, "QUIVERHA", true )
    ObjectHideSubObjectPermanently( self, "ARROWQUIVUP", true )
    ObjectHideSubObjectPermanently( self, "ARROWNOCKUP", true )
    ObjectHideSubObjectPermanently( self, "BOW2", true )
    ObjectHideSubObjectPermanently( self, "BOW1", true )
	ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
-- regular ha objects
    ObjectHideSubObjectPermanently( self, "BODYMHOODHA", true )
	ObjectHideSubObjectPermanently( self, "LEGGUARDSM", true )
    ObjectHideSubObjectPermanently( self, "ARMSMHA", true )
    ObjectHideSubObjectPermanently( self, "HOODMHA", true )
    ObjectHideSubObjectPermanently( self, "BODYMHA", true )
    ObjectHideSubObjectPermanently( self, "HOODMDOWNHA", true )
-- female ha objects
    ObjectHideSubObjectPermanently( self, "BODYFHOODHA", true )
	ObjectHideSubObjectPermanently( self, "LEGGUARDSF", true )
    ObjectHideSubObjectPermanently( self, "ARMSFHA", true )
    ObjectHideSubObjectPermanently( self, "HOODFHA", true )
    ObjectHideSubObjectPermanently( self, "BODYFHA", true )
    ObjectHideSubObjectPermanently( self, "HOODFDOWNHA", true )
-- normal heads
    ObjectHideSubObjectPermanently( self, "HEADM1", true )
    ObjectHideSubObjectPermanently( self, "HEADM2", true )
    ObjectHideSubObjectPermanently( self, "HEADM3", true )
-- heads for hoods
    ObjectHideSubObjectPermanently( self, "HEADMHOOD1", true )
    ObjectHideSubObjectPermanently( self, "HEADMHOOD2", true )
    ObjectHideSubObjectPermanently( self, "HEADMHOOD3", true )
-- normal female heads
    ObjectHideSubObjectPermanently( self, "HEADF1", true )
    ObjectHideSubObjectPermanently( self, "HEADF2", true )
    ObjectHideSubObjectPermanently( self, "HEADF3", true )
-- female heads for hoods
    ObjectHideSubObjectPermanently( self, "HEADFHOOD1", true )
    ObjectHideSubObjectPermanently( self, "HEADFHOOD2", true )
    ObjectHideSubObjectPermanently( self, "HEADFHOOD3", true )
-- normal bodies
    ObjectHideSubObjectPermanently( self, "BODYM1", true )
    ObjectHideSubObjectPermanently( self, "BODYM2", true )
    ObjectHideSubObjectPermanently( self, "BODYM3", true )
    ObjectHideSubObjectPermanently( self, "HOODMDOWN1", true )
    ObjectHideSubObjectPermanently( self, "HOODMDOWN2", true )
    ObjectHideSubObjectPermanently( self, "HOODMDOWN3", true )
-- heads for bodies
    ObjectHideSubObjectPermanently( self, "BODYMHOOD1", true )
    ObjectHideSubObjectPermanently( self, "BODYMHOOD2", true )
    ObjectHideSubObjectPermanently( self, "BODYMHOOD3", true )
    ObjectHideSubObjectPermanently( self, "HOODM1", true )
    ObjectHideSubObjectPermanently( self, "HOODM2", true )
    ObjectHideSubObjectPermanently( self, "HOODM3", true )
-- normal female bodies
    ObjectHideSubObjectPermanently( self, "BODYF1", true )
    ObjectHideSubObjectPermanently( self, "BODYF2", true )
    ObjectHideSubObjectPermanently( self, "BODYF3", true )
    ObjectHideSubObjectPermanently( self, "HOODFDOWN1", true )
    ObjectHideSubObjectPermanently( self, "HOODFDOWN2", true )
    ObjectHideSubObjectPermanently( self, "HOODFDOWN3", true )
-- female heads for bodies
    ObjectHideSubObjectPermanently( self, "BODYFHOOD1", true )
    ObjectHideSubObjectPermanently( self, "BODYFHOOD2", true )
    ObjectHideSubObjectPermanently( self, "BODYFHOOD3", true )
    ObjectHideSubObjectPermanently( self, "HOODF1", true )
    ObjectHideSubObjectPermanently( self, "HOODF2", true )
    ObjectHideSubObjectPermanently( self, "HOODF3", true )

    local mheads1         =    GetRandomNumber()
    local mheads2         =    GetRandomNumber()
    local fheads1         =    GetRandomNumber()
    local fheads2         =    GetRandomNumber()
	
    local mbody1         =    GetRandomNumber()
    local mbody2         =    GetRandomNumber()	
    local fbody1         =    GetRandomNumber()	
    local fbody2         =    GetRandomNumber()
	
    local bow         =    GetRandomNumber()

    if mheads1 <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HEADM1", false )
    elseif mheads1 <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEADM2", false )
     else
        ObjectHideSubObjectPermanently( self, "HEADM3", false )
    end  
	
    if mheads2 <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HEADMHOOD1", false )
    elseif mheads2 <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEADMHOOD2", false )
     else
        ObjectHideSubObjectPermanently( self, "HEADMHOOD3", false )
    end 
	
    if fheads1 <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HEADF1", false )
    elseif fheads1 <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEADF2", false )
     else
        ObjectHideSubObjectPermanently( self, "HEADF3", false )
    end 
	
    if fheads2 <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HEADFHOOD1", false )
    elseif fheads2 <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEADFHOOD2", false )
     else
        ObjectHideSubObjectPermanently( self, "HEADFHOOD3", false )
    end 
	
    if mbody1 <= 0.4 then
        ObjectHideSubObjectPermanently( self, "BODYM1", false )
        ObjectHideSubObjectPermanently( self, "HOODMDOWN1", false )
    elseif mbody1 <= 0.7 then
        ObjectHideSubObjectPermanently( self, "BODYM2", false )
        ObjectHideSubObjectPermanently( self, "HOODMDOWN2", false )
     else
        ObjectHideSubObjectPermanently( self, "BODYM3", false )
        ObjectHideSubObjectPermanently( self, "HOODMDOWN3", false )
    end  
	
    if mbody2 <= 0.4 then
        ObjectHideSubObjectPermanently( self, "BODYMHOOD1", false )
        ObjectHideSubObjectPermanently( self, "HOODM1", false )
    elseif mbody2 <= 0.7 then
        ObjectHideSubObjectPermanently( self, "BODYMHOOD2", false )
        ObjectHideSubObjectPermanently( self, "HOODM2", false )
     else
        ObjectHideSubObjectPermanently( self, "BODYMHOOD3", false )
        ObjectHideSubObjectPermanently( self, "HOODM3", false )
    end  
	
    if fbody1 <= 0.4 then
        ObjectHideSubObjectPermanently( self, "BODYF1", false )
        ObjectHideSubObjectPermanently( self, "HOODFDOWN1", false )
    elseif fbody1 <= 0.7 then
        ObjectHideSubObjectPermanently( self, "BODYF2", false )
        ObjectHideSubObjectPermanently( self, "HOODFDOWN2", false )
     else
        ObjectHideSubObjectPermanently( self, "BODYF3", false )
        ObjectHideSubObjectPermanently( self, "HOODFDOWN3", false )
    end  
	
    if fbody2 <= 0.4 then
        ObjectHideSubObjectPermanently( self, "BODYFHOOD1", false )
        ObjectHideSubObjectPermanently( self, "HOODF1", false )
    elseif fbody2 <= 0.7 then
        ObjectHideSubObjectPermanently( self, "BODYFHOOD2", false )
        ObjectHideSubObjectPermanently( self, "HOODF2", false )
     else
        ObjectHideSubObjectPermanently( self, "BODYFHOOD3", false )
        ObjectHideSubObjectPermanently( self, "HOODF3", false )
    end  
	
    if bow <= 0.5 then
        ObjectHideSubObjectPermanently( self, "BOW1", false )
     else
        ObjectHideSubObjectPermanently( self, "BOW2", false )
    end 
end

function OnElvenWarriorCreated(self)
	ObjectHideSubObject( self, "ARROW", true )
	ObjectHideSubObject( self, "ARROWNOCK", true )
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )

    ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )

    local helmet         =    GetRandomNumber()

    if helmet <= 0.333 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif helmet <= 0.666 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end   

end

function OnIsengardLadderCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Glow", true )
	ObjectHideSubObjectPermanently( self, "HELMET01", true )
	ObjectHideSubObjectPermanently( self, "HELMET01X", true )
	ObjectHideSubObjectPermanently( self, "HELMET02", true )
	ObjectHideSubObjectPermanently( self, "HELMET02X", true )
	ObjectHideSubObjectPermanently( self, "HELMET03", true )
	ObjectHideSubObjectPermanently( self, "HELMET03X", true )
	ObjectHideSubObjectPermanently( self, "SHIELD01", true )
	ObjectHideSubObjectPermanently( self, "SHIELD02", true )
	ObjectHideSubObjectPermanently( self, "GAUNTLET_S", true )
	
    local helmet         =    GetRandomNumber()
    local helmet2         =    GetRandomNumber()
	
    if helmet <= 0.333 then
        ObjectHideSubObjectPermanently( self, "HELMET01", false )
    elseif helmet <= 0.666 then
        ObjectHideSubObjectPermanently( self, "HELMET02", false )
     else
        ObjectHideSubObjectPermanently( self, "HELMET03", false )
    end   

    if helmet2 <= 0.333 then
        ObjectHideSubObjectPermanently( self, "HELMET01X", false )
    elseif helmet2 <= 0.666 then
        ObjectHideSubObjectPermanently( self, "HELMET02X", false )
     else
        ObjectHideSubObjectPermanently( self, "HELMET03X", false )
    end   
end

function OnIsengardArcherCreated(self)
	ObjectHideSubObject( self, "ARROWNOCK", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )
	ObjectHideSubObjectPermanently( self, "HA", true )
	ObjectHideSubObjectPermanently( self, "HELMET01", true )
	ObjectHideSubObjectPermanently( self, "HELMET02", true )
	ObjectHideSubObjectPermanently( self, "QUIVERA", true )
	ObjectHideSubObjectPermanently( self, "QUIVERB", true )
	
    local quiver         =    GetRandomNumber()
    local helmet         =    GetRandomNumber()

    if quiver <= 0.5 then
        ObjectHideSubObjectPermanently( self, "QUIVERA", false )
    else
        ObjectHideSubObjectPermanently( self, "QUIVERB", false )
    end     
end

function OnIsengardBeserkerCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "TORCH", true )	
	ObjectHideSubObjectPermanently( self, "CREST", true )
	ObjectHideSubObjectPermanently( self, "ARROWS", true ) 
end


function OnIsengardFighterCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Glow", true )
	ObjectHideSubObjectPermanently( self, "HELMET01", true )
	ObjectHideSubObjectPermanently( self, "HELMET02", true )
	ObjectHideSubObjectPermanently( self, "SHIELD01", true )
	ObjectHideSubObjectPermanently( self, "SHIELD02", true )
	ObjectHideSubObjectPermanently( self, "GAUNTLET_S", true )
	
    local helmet         =    GetRandomNumber()
    local shield         =    GetRandomNumber()

    if helmet <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HELMET01", false )
    else
        ObjectHideSubObjectPermanently( self, "HELMET02", false )
    end   
	
    if shield <= 0.50 then
        ObjectHideSubObjectPermanently( self, "SHIELD01", false )
    else
        ObjectHideSubObjectPermanently( self, "SHIELD02", false )
    end   
end

function OnIsengardScoutCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Glow", true )
	ObjectHideSubObjectPermanently( self, "SHIELD01", true )
	ObjectHideSubObjectPermanently( self, "SHIELD02", true )
	ObjectHideSubObjectPermanently( self, "QUIVER01", true )
	ObjectHideSubObjectPermanently( self, "QUIVER02", true )

	local quiver         =    GetRandomNumber()
    local shield         =    GetRandomNumber()
	
    if quiver <= 0.5 then
        ObjectHideSubObjectPermanently( self, "QUIVER01", false )
    else
        ObjectHideSubObjectPermanently( self, "QUIVER02", false )
    end  
	
	if shield <= 0.333 then
        ObjectHideSubObjectPermanently( self, "SHIELD01", false )
    elseif shield <= 0.666 then
        ObjectHideSubObjectPermanently( self, "SHIELD02", false )
     else
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
    end   
end

function OnIsengardPikemanCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Glow", true )
	ObjectHideSubObjectPermanently( self, "HELMET01", true )
	ObjectHideSubObjectPermanently( self, "HELMET02", true )
	
    local helmet         =    GetRandomNumber()
    local shield         =    GetRandomNumber()

    if helmet <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HELMET01", false )
    else
        ObjectHideSubObjectPermanently( self, "HELMET02", false )
    end   
  
end

function OnIsengardBannerCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Glow", true )
	ObjectHideSubObjectPermanently( self, "FLAG01", true )
	ObjectHideSubObjectPermanently( self, "FLAG02", true )
	ObjectHideSubObjectPermanently( self, "FLAG03", true )

    local shield         =    GetRandomNumber()  
	
    if shield <= 0.333 then
        ObjectHideSubObjectPermanently( self, "FLAG01", false )
    elseif shield <= 0.666 then
        ObjectHideSubObjectPermanently( self, "FLAG02", false )
     else
        ObjectHideSubObjectPermanently( self, "FLAG03", false )
    end   
end

function OnIsengardWildmanCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Torch", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )
	ObjectHideSubObjectPermanently( self, "WILDMAN1", true )
	ObjectHideSubObjectPermanently( self, "WILDMAN2", true )
	ObjectHideSubObjectPermanently( self, "WILDMAN3", true )
	ObjectHideSubObjectPermanently( self, "WILDMAN4", true )

	ObjectHideSubObjectPermanently( self, "PICK", true )
	ObjectHideSubObjectPermanently( self, "SICKLE", true )
	ObjectHideSubObjectPermanently( self, "AXE", true )
	ObjectHideSubObjectPermanently( self, "ROHANAXE", true )
	ObjectHideSubObjectPermanently( self, "URUKSWORD", true )

	ObjectHideSubObjectPermanently( self, "FORKLEFT", true )
	ObjectHideSubObjectPermanently( self, "SICKLELEFT", true )
	ObjectHideSubObjectPermanently( self, "AXELEFT", true )
	ObjectHideSubObjectPermanently( self, "ROHANAXELEFT", true )
	ObjectHideSubObjectPermanently( self, "URUKSWORDLEFT", true )

    local body         =    GetRandomNumber()
    local weaponleft         =    GetRandomNumber()
    local weaponright         =    GetRandomNumber()

    if body <= 0.25 then
        ObjectHideSubObjectPermanently( self, "WILDMAN1", false )
    elseif body <= 0.50 then
        ObjectHideSubObjectPermanently( self, "WILDMAN2", false )
    elseif body <= 0.75 then
        ObjectHideSubObjectPermanently( self, "WILDMAN3", false )
     else
        ObjectHideSubObjectPermanently( self, "WILDMAN4", false )
    end  

    if weaponright <= 0.20 then
        ObjectHideSubObjectPermanently( self, "PICK", false )
    elseif weaponright <= 0.40 then
        ObjectHideSubObjectPermanently( self, "SICKLE", false )
    elseif weaponright <= 0.60 then
        ObjectHideSubObjectPermanently( self, "AXE", false )
    elseif weaponright <= 0.80 then
        ObjectHideSubObjectPermanently( self, "URUKSWORD", false )
     else
        ObjectHideSubObjectPermanently( self, "ROHANAXE", false )
    end  

    if weaponleft <= 0.20 then
        ObjectHideSubObjectPermanently( self, "FORKLEFT", false )
    elseif weaponleft <= 0.40 then
        ObjectHideSubObjectPermanently( self, "ROHANAXELEFT", false )
    elseif weaponleft <= 0.60 then
        ObjectHideSubObjectPermanently( self, "SICKLELEFT", false )
    elseif weaponleft <= 0.80 then
        ObjectHideSubObjectPermanently( self, "AXELEFT", false )
     else
        ObjectHideSubObjectPermanently( self, "URUKSWORDLEFT", false )
    end  

end

function OnIsengardWildmanAxeCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Torch", true )
	ObjectHideSubObjectPermanently( self, "FireArowTip", true )
	ObjectHideSubObjectPermanently( self, "WILDMAN1", true )
	ObjectHideSubObjectPermanently( self, "WILDMAN2", true )
	ObjectHideSubObjectPermanently( self, "WILDMAN3", true )
	ObjectHideSubObjectPermanently( self, "WILDMAN4", true )

	ObjectHideSubObjectPermanently( self, "AXELEFT", true )
	ObjectHideSubObjectPermanently( self, "ROHANAXELEFT", true )

    local body         =    GetRandomNumber()
    local weaponleft         =    GetRandomNumber()

    if body <= 0.25 then
        ObjectHideSubObjectPermanently( self, "WILDMAN1", false )
    elseif body <= 0.50 then
        ObjectHideSubObjectPermanently( self, "WILDMAN2", false )
    elseif body <= 0.75 then
        ObjectHideSubObjectPermanently( self, "WILDMAN3", false )
     else
        ObjectHideSubObjectPermanently( self, "WILDMAN4", false )
    end   

    if weaponleft <= 0.50 then
        ObjectHideSubObjectPermanently( self, "ROHANAXELEFT", false )
     else
        ObjectHideSubObjectPermanently( self, "AXELEFT", false )
    end  

end

function OnIsengardWildmanSpearCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Torch", true )
	ObjectHideSubObjectPermanently( self, "FireArowTip", true )
	ObjectHideSubObjectPermanently( self, "WILDMAN1", true )
	ObjectHideSubObjectPermanently( self, "WILDMAN2", true )
	ObjectHideSubObjectPermanently( self, "WILDMAN3", true )
	ObjectHideSubObjectPermanently( self, "WILDMAN4", true )

	ObjectHideSubObjectPermanently( self, "SCYTHE", true )
	ObjectHideSubObjectPermanently( self, "SPEAR", true )
	ObjectHideSubObjectPermanently( self, "PITCHFORK", true )

    local body         =    GetRandomNumber()
    local weaponleft         =    GetRandomNumber()

    if body <= 0.25 then
        ObjectHideSubObjectPermanently( self, "WILDMAN1", false )
    elseif body <= 0.50 then
        ObjectHideSubObjectPermanently( self, "WILDMAN2", false )
    elseif body <= 0.75 then
        ObjectHideSubObjectPermanently( self, "WILDMAN3", false )
     else
        ObjectHideSubObjectPermanently( self, "WILDMAN4", false )
    end   

    if weaponleft <= 0.30 then
        ObjectHideSubObjectPermanently( self, "SCYTHE", false )
    elseif weaponleft <= 0.60 then
        ObjectHideSubObjectPermanently( self, "SPEAR", false )
     else
        ObjectHideSubObjectPermanently( self, "PITCHFORK", false )
    end  

end

function OnWildSpiderRiderCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )
	ObjectHideSubObject( self, "ARROWNOCK", true )
end

function OnHaradrimArcherCreated(self)
	ObjectHideSubObjectPermanently( self, "FireArowTip", true )
	ObjectHideSubObjectPermanently( self, "BARB", true )
	ObjectHideSubObject( self, "ArrowNock", true )
end

function OnWildGoblinArcherCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )
	ObjectHideSubObjectPermanently( self, "SHIELD", true )
	ObjectHideSubObjectPermanently( self, "HELM01", true )
	ObjectHideSubObjectPermanently( self, "HELM02", true )
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
	ObjectHideSubObjectPermanently( self, "HEAD02", true )
	ObjectHideSubObjectPermanently( self, "HEAD03", true )
	
    local head          =    GetRandomNumber()
    local helmet          =    GetRandomNumber()
	
    if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end

	if helmet <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HELM01", false )
     else
        ObjectHideSubObjectPermanently( self, "HELM02", false )
    end  
end

function OnWildGoblinFighterCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )
	ObjectHideSubObjectPermanently( self, "SHIELD", true )
	ObjectHideSubObjectPermanently( self, "HELM01", true )
	ObjectHideSubObjectPermanently( self, "HELM02", true )
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
	ObjectHideSubObjectPermanently( self, "HEAD02", true )
	ObjectHideSubObjectPermanently( self, "HEAD03", true )
	
    local head          =    GetRandomNumber()
    local helmet          =    GetRandomNumber()
	
    if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end

	if helmet <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HELM01", false )
     else
        ObjectHideSubObjectPermanently( self, "HELM02", false )
    end  
end

function OnWildGoblinNeutralCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREAROWTIP", true )
	ObjectHideSubObjectPermanently( self, "SHIELD", true )
	ObjectHideSubObjectPermanently( self, "HELM01", true )
	ObjectHideSubObjectPermanently( self, "HELM02", true )
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
	ObjectHideSubObjectPermanently( self, "HEAD02", true )
	ObjectHideSubObjectPermanently( self, "HEAD03", true )
	
    local head          =    GetRandomNumber()
    local helmet          =    GetRandomNumber()
    local shield          =    GetRandomNumber()
	
    if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end

	if helmet <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HELM01", false )
     else
        ObjectHideSubObjectPermanently( self, "HELM02", false )
    end  

	if shield <= 0.3 then
        ObjectHideSubObjectPermanently( self, "SHIELD", false )
     else
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
    end  
end

function OnGarrisonableCreated(self)
	ObjectHideSubObjectPermanently( self, "GARRISON01", true )
	ObjectHideSubObjectPermanently( self, "GARRISON02", true )
	ObjectHideSubObjectPermanently( self, "KINGS", true )
	ObjectHideSubObjectPermanently( self, "EREDLUIN", true )
	ObjectHideSubObjectPermanently( self, "MORIA", true )
	ObjectHideSubObjectPermanently( self, "MITHRIM", true )
	ObjectHideSubObjectPermanently( self, "IRONHILLS", true )
end

function OnDwarvenDaleman(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FIREARROWTIP", true )
    ObjectHideSubObjectPermanently( self, "HEADA", true )
    ObjectHideSubObjectPermanently( self, "HEADB", true )
    ObjectHideSubObjectPermanently( self, "HEADC", true )
    ObjectHideSubObjectPermanently( self, "HEADD", true )
    ObjectHideSubObjectPermanently( self, "TASSLES", true )
    ObjectHideSubObjectPermanently( self, "HELMETB", true )
    ObjectHideSubObjectPermanently( self, "ARMOR", true )
    ObjectHideSubObjectPermanently( self, "SHLD01", true )
	
    local head          =    GetRandomNumber()
	
    if head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "HEADA", false )
    elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEADB", false )
    elseif head <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HEADC", false )
     else
        ObjectHideSubObjectPermanently( self, "HEADD", false )
    end   
end

function OnDwarvenLaketowner(self)
    ObjectHideSubObjectPermanently( self, "HEADA", true )
    ObjectHideSubObjectPermanently( self, "HEADB", true )
    ObjectHideSubObjectPermanently( self, "HEADC", true )
    ObjectHideSubObjectPermanently( self, "HEADD", true )
    ObjectHideSubObjectPermanently( self, "HELMETA", true )
    ObjectHideSubObjectPermanently( self, "ARMOR", true )
	
    local head          =    GetRandomNumber()
	
    if head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "HEADA", false )
    elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEADB", false )
    elseif head <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HEADC", false )
     else
        ObjectHideSubObjectPermanently( self, "HEADD", false )
    end   
end

function OnDwarvenDorwinion(self)
    ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
    ObjectHideSubObjectPermanently( self, "HEADA", true )
    ObjectHideSubObjectPermanently( self, "HEADB", true )
    ObjectHideSubObjectPermanently( self, "HEADC", true )
    ObjectHideSubObjectPermanently( self, "HELMETA", true )
    ObjectHideSubObjectPermanently( self, "HELMETB", true )
    ObjectHideSubObjectPermanently( self, "HELMETC", true )
    ObjectHideSubObjectPermanently( self, "HELMETD", true )
    ObjectHideSubObjectPermanently( self, "HELMETE1", true )
    ObjectHideSubObjectPermanently( self, "HELMETE2", true )
    ObjectHideSubObjectPermanently( self, "HELMETE3", true )
    ObjectHideSubObjectPermanently( self, "FEATHERA1", true )
    ObjectHideSubObjectPermanently( self, "FEATHERA2", true )
    ObjectHideSubObjectPermanently( self, "FEATHERA3", true )
    ObjectHideSubObjectPermanently( self, "FEATHERB1", true )
    ObjectHideSubObjectPermanently( self, "FEATHERB2", true )
    ObjectHideSubObjectPermanently( self, "FEATHERB3", true )
    ObjectHideSubObjectPermanently( self, "GLOVES01", true )
    ObjectHideSubObjectPermanently( self, "GLOVES02", true )
	
    local head          =    GetRandomNumber()
    local helmet          =    GetRandomNumber()
    local arms          =    GetRandomNumber()
	
    if arms <= 0.5 then
        ObjectHideSubObjectPermanently( self, "GLOVES01", false )
     else
        ObjectHideSubObjectPermanently( self, "GLOVES02", false )
    end 
	
    if head <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HEADA", false )
    elseif head <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HEADB", false )
     else
        ObjectHideSubObjectPermanently( self, "HEADC", false )
    end
	
    if helmet <= 0.037 then
        ObjectHideSubObjectPermanently( self, "HELMETA", false )
        ObjectHideSubObjectPermanently( self, "FEATHERA1", false )
    elseif helmet <= 0.074 then
        ObjectHideSubObjectPermanently( self, "HELMETA", false )
        ObjectHideSubObjectPermanently( self, "FEATHERA2", false )
    elseif helmet <= 0.111 then
        ObjectHideSubObjectPermanently( self, "HELMETA", false )
        ObjectHideSubObjectPermanently( self, "FEATHERA3", false )
    elseif helmet <= 0.148 then
        ObjectHideSubObjectPermanently( self, "HELMETA", false )
        ObjectHideSubObjectPermanently( self, "FEATHERB1", false )
    elseif helmet <= 0.185 then
        ObjectHideSubObjectPermanently( self, "HELMETA", false )
        ObjectHideSubObjectPermanently( self, "FEATHERB2", false )
    elseif helmet <= 0.222 then
        ObjectHideSubObjectPermanently( self, "HELMETA", false )
        ObjectHideSubObjectPermanently( self, "FEATHERB3", false )
    elseif helmet <= 0.259 then
        ObjectHideSubObjectPermanently( self, "HELMETB", false )
        ObjectHideSubObjectPermanently( self, "FEATHERA1", false )
    elseif helmet <= 0.296 then
        ObjectHideSubObjectPermanently( self, "HELMETB", false )
        ObjectHideSubObjectPermanently( self, "FEATHERA2", false )
    elseif helmet <= 0.333 then
        ObjectHideSubObjectPermanently( self, "HELMETB", false )
        ObjectHideSubObjectPermanently( self, "FEATHERA3", false )
    elseif helmet <= 0.37 then
        ObjectHideSubObjectPermanently( self, "HELMETB", false )
        ObjectHideSubObjectPermanently( self, "FEATHERB1", false )
    elseif helmet <= 0.407 then
        ObjectHideSubObjectPermanently( self, "HELMETB", false )
        ObjectHideSubObjectPermanently( self, "FEATHERB2", false )
    elseif helmet <= 0.444 then
        ObjectHideSubObjectPermanently( self, "HELMETB", false )
        ObjectHideSubObjectPermanently( self, "FEATHERB3", false )
    elseif helmet <= 0.481 then
        ObjectHideSubObjectPermanently( self, "HELMETC", false )
        ObjectHideSubObjectPermanently( self, "FEATHERA1", false )
    elseif helmet <= 0.518 then
        ObjectHideSubObjectPermanently( self, "HELMETC", false )
        ObjectHideSubObjectPermanently( self, "FEATHERA2", false )
    elseif helmet <= 0.555 then
        ObjectHideSubObjectPermanently( self, "HELMETC", false )
        ObjectHideSubObjectPermanently( self, "FEATHERA3", false )
    elseif helmet <= 0.592 then
        ObjectHideSubObjectPermanently( self, "HELMETC", false )
        ObjectHideSubObjectPermanently( self, "FEATHERB1", false )
    elseif helmet <= 0.629 then
        ObjectHideSubObjectPermanently( self, "HELMETC", false )
        ObjectHideSubObjectPermanently( self, "FEATHERB2", false )
    elseif helmet <= 0.666 then
        ObjectHideSubObjectPermanently( self, "HELMETC", false )
        ObjectHideSubObjectPermanently( self, "FEATHERB3", false )
    elseif helmet <= 0.703 then
        ObjectHideSubObjectPermanently( self, "HELMETD", false )
        ObjectHideSubObjectPermanently( self, "FEATHERA1", false )
    elseif helmet <= 0.74 then
        ObjectHideSubObjectPermanently( self, "HELMETD", false )
        ObjectHideSubObjectPermanently( self, "FEATHERA2", false )
    elseif helmet <= 0.777 then
        ObjectHideSubObjectPermanently( self, "HELMETD", false )
        ObjectHideSubObjectPermanently( self, "FEATHERA3", false )
    elseif helmet <= 0.814 then
        ObjectHideSubObjectPermanently( self, "HELMETD", false )
        ObjectHideSubObjectPermanently( self, "FEATHERB1", false )
    elseif helmet <= 0.851 then
        ObjectHideSubObjectPermanently( self, "HELMETD", false )
        ObjectHideSubObjectPermanently( self, "FEATHERB2", false )
    elseif helmet <= 0.888 then
        ObjectHideSubObjectPermanently( self, "HELMETD", false )
        ObjectHideSubObjectPermanently( self, "FEATHERB3", false )
    elseif helmet <= 0.925 then
        ObjectHideSubObjectPermanently( self, "HELMETE1", false )
    elseif helmet <= 0.962 then
        ObjectHideSubObjectPermanently( self, "HELMETE2", false )
     else
        ObjectHideSubObjectPermanently( self, "HELMETE3", false )
    end 
end

function OnDwarvenMenBannerCreated(self)
    ObjectHideSubObjectPermanently( self, "HEADA", true )
    ObjectHideSubObjectPermanently( self, "HEADB", true )
    ObjectHideSubObjectPermanently( self, "HEADC", true )
    ObjectHideSubObjectPermanently( self, "HEADD", true )
	
    local head          =    GetRandomNumber()
	
    if head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "HEADA", false )
    elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEADB", false )
    elseif head <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HEADC", false )
     else
        ObjectHideSubObjectPermanently( self, "HEADD", false )
    end  
end

function OnDwarvenZerkerCreated(self)
    ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
    ObjectHideSubObjectPermanently( self, "HEAD04", true )
	
    local head          =    GetRandomNumber()
	
    if head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
    elseif head <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD04", false )
    end   
end

function OnDwarvenRiderCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "IHSHIELDA", true )
	ObjectHideSubObjectPermanently( self, "IHSHIELDB", true )
    ObjectHideSubObjectPermanently( self, "HORNS1", true )
    ObjectHideSubObjectPermanently( self, "HORNS2", true )
    ObjectHideSubObjectPermanently( self, "BIGSHEEPA", true )
    ObjectHideSubObjectPermanently( self, "BIGSHEEPB", true )
    ObjectHideSubObjectPermanently( self, "BIGSHEEPC", true )
    ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
    ObjectHideSubObjectPermanently( self, "HEAD04", true )

    local helmet         =    GetRandomNumber()
    local head          =    GetRandomNumber()
    local armor          =    GetRandomNumber()
    local shield          =    GetRandomNumber()

    if helmet <= 0.3 then
        ObjectHideSubObjectPermanently( self, "BIGSHEEPA", false )
    elseif helmet <= 0.6 then
        ObjectHideSubObjectPermanently( self, "BIGSHEEPB", false )
     else
        ObjectHideSubObjectPermanently( self, "BIGSHEEPC", false )
    end   

    if head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
    elseif head <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD04", false )
    end   

    if armor <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HORNS1", false )
     else
        ObjectHideSubObjectPermanently( self, "HORNS2", false )
    end 

    if shield <= 0.5 then
        ObjectHideSubObjectPermanently( self, "IHSHIELDA", false )
     else
        ObjectHideSubObjectPermanently( self, "IHSHIELDB", false )
    end 	

end

function OnIronHillDemoCreated(self)
    ObjectHideSubObjectPermanently( self, "BIGSHEEPA_1", true )
    ObjectHideSubObjectPermanently( self, "BIGSHEEPA_2", true )
    ObjectHideSubObjectPermanently( self, "BIGSHEEPA_3", true )
    ObjectHideSubObjectPermanently( self, "BIGSHEEPA1", true )
    ObjectHideSubObjectPermanently( self, "BIGSHEEPA2", true )
    ObjectHideSubObjectPermanently( self, "BIGSHEEPA3", true )

    local helmet         =    GetRandomNumber()
    local armor          =    GetRandomNumber()

    if helmet <= 0.3 then
        ObjectHideSubObjectPermanently( self, "BIGSHEEPA_1", false )
    elseif helmet <= 0.6 then
        ObjectHideSubObjectPermanently( self, "BIGSHEEPA_2", false )
     else
        ObjectHideSubObjectPermanently( self, "BIGSHEEPA_3", false )
    end    

    if armor <= 0.3 then
        ObjectHideSubObjectPermanently( self, "BIGSHEEPA1", false )
    elseif armor <= 0.6 then
        ObjectHideSubObjectPermanently( self, "BIGSHEEPA2", false )
     else
        ObjectHideSubObjectPermanently( self, "BIGSHEEPA3", false )
    end  
end

function OnDwarvenGoatCreated(self)
    ObjectHideSubObjectPermanently( self, "HORNS1", true )
    ObjectHideSubObjectPermanently( self, "HORNS2", true )
    ObjectHideSubObjectPermanently( self, "BIGSHEEPA", true )
    ObjectHideSubObjectPermanently( self, "BIGSHEEPB", true )
    ObjectHideSubObjectPermanently( self, "BIGSHEEPC", true )

    local helmet         =    GetRandomNumber()
    local armor          =    GetRandomNumber()

    if helmet <= 0.3 then
        ObjectHideSubObjectPermanently( self, "BIGSHEEPA", false )
    elseif helmet <= 0.6 then
        ObjectHideSubObjectPermanently( self, "BIGSHEEPB", false )
     else
        ObjectHideSubObjectPermanently( self, "BIGSHEEPC", false )
    end    

    if armor <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HORNS1", false )
     else
        ObjectHideSubObjectPermanently( self, "HORNS2", false )
    end  	

end

function OnDwarvenGuardianCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Hammer1", true )
    ObjectHideSubObjectPermanently( self, "PAULDRONS01", true )
    ObjectHideSubObjectPermanently( self, "PAULDRONS02", true )
    ObjectHideSubObjectPermanently( self, "HELMET01", true )
    ObjectHideSubObjectPermanently( self, "HELMET02", true )
    ObjectHideSubObjectPermanently( self, "HELMET03", true )
    ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
    ObjectHideSubObjectPermanently( self, "HEAD04", true )
    ObjectHideSubObjectPermanently( self, "HEAD05", true )
    ObjectHideSubObjectPermanently( self, "HEAD06", true )
    ObjectHideSubObjectPermanently( self, "HEAD07", true )
    ObjectHideSubObjectPermanently( self, "HEAD08", true )

    local helmet         =    GetRandomNumber()
    local head          =    GetRandomNumber()
    local armor          =    GetRandomNumber()

    if helmet <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HELMET01", false )
     else
        ObjectHideSubObjectPermanently( self, "HELMET03", false )
    end   

    if head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
    elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    elseif head <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HEAD04", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEAD05", false )
    elseif head <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HEAD06", false )
    elseif head <= 0.9 then
        ObjectHideSubObjectPermanently( self, "HEAD07", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD08", false )
    end   

    if armor <= 0.5 then
        ObjectHideSubObjectPermanently( self, "PAULDRONS01", false )
     else
        ObjectHideSubObjectPermanently( self, "PAULDRONS02", false )
    end   

end

function OnDwarvenGrimHammerCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Hammer1", true )
    ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
    ObjectHideSubObjectPermanently( self, "HEAD04", true )
    ObjectHideSubObjectPermanently( self, "HEAD05", true )
    ObjectHideSubObjectPermanently( self, "HEAD06", true )
    ObjectHideSubObjectPermanently( self, "HEAD07", true )
    ObjectHideSubObjectPermanently( self, "HEAD08", true )
    ObjectHideSubObjectPermanently( self, "HEAD09", true )

    local head          =    GetRandomNumber()

    if head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
    elseif head <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEAD04", false )
    elseif head <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HEAD05", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEAD06", false )
    elseif head <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HEAD07", false )
	elseif head <= 0.9 then
        ObjectHideSubObjectPermanently( self, "HEAD08", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD09", false )
    end    

end

function OnDwarvenGuardianBannerCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Hammer1", true )
    ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )

    local head          =    GetRandomNumber()

     if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end
end

function OnMineLauncherCreated(self)
    ObjectHideSubObjectPermanently( self, "HELMET01", true )
    ObjectHideSubObjectPermanently( self, "HELMET02", true )
end

function OnIsengardMineCreated(self)
    ObjectHideSubObjectPermanently( self, "O1_HELMET01", true )
    ObjectHideSubObjectPermanently( self, "O2_HELMET01", true )
    ObjectHideSubObjectPermanently( self, "O1_HELMET02", true )
    ObjectHideSubObjectPermanently( self, "O2_HELMET02", true )
end

function OnIronHillsSpearsCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Hammer1", true )
    ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
    ObjectHideSubObjectPermanently( self, "HEAD04", true )
    ObjectHideSubObjectPermanently( self, "HEAD05", true )
    ObjectHideSubObjectPermanently( self, "HEAD06", true )
    ObjectHideSubObjectPermanently( self, "HEAD07", true )
    ObjectHideSubObjectPermanently( self, "HEAD08", true )
    ObjectHideSubObjectPermanently( self, "HEAD09", true )
    ObjectHideSubObjectPermanently( self, "HEAD10", true )
    ObjectHideSubObjectPermanently( self, "HEAD11", true )
    ObjectHideSubObjectPermanently( self, "HEAD12", true )

    local head          =    GetRandomNumber()
	
	    if head <= 0.1 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
	elseif head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    elseif head <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HEAD04", false )
	elseif head <= 0.45 then
        ObjectHideSubObjectPermanently( self, "HEAD05", false )
    elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEAD06", false )
	elseif head <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HEAD07", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEAD08", false )
	elseif head <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HEAD09", false )
    elseif head <= 0.85 then
        ObjectHideSubObjectPermanently( self, "HEAD10", false )
	elseif head <= 0.9 then
        ObjectHideSubObjectPermanently( self, "HEAD11", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD12", false )
    end
	
end  

function OnIronHillsWarriorsCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Hammer1", true )
    ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
    ObjectHideSubObjectPermanently( self, "HEAD04", true )
    ObjectHideSubObjectPermanently( self, "HEAD05", true )
    ObjectHideSubObjectPermanently( self, "HEAD06", true )
    ObjectHideSubObjectPermanently( self, "HEAD07", true )
    ObjectHideSubObjectPermanently( self, "HEAD08", true )
    ObjectHideSubObjectPermanently( self, "HEAD09", true )
    ObjectHideSubObjectPermanently( self, "HEAD10", true )
    ObjectHideSubObjectPermanently( self, "HEAD11", true )
    ObjectHideSubObjectPermanently( self, "HEAD12", true )

    local head          =    GetRandomNumber()
	
	    if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
	elseif head <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD04", false )
    end
	
end   

function OnIronHillsCbowsCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Hammer1", true )
    ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
    ObjectHideSubObjectPermanently( self, "HEAD04", true )
    ObjectHideSubObjectPermanently( self, "HEAD05", true )
    ObjectHideSubObjectPermanently( self, "HEAD06", true )
    ObjectHideSubObjectPermanently( self, "HEAD07", true )
    ObjectHideSubObjectPermanently( self, "HEAD08", true )

    local head          =    GetRandomNumber()
	
	    if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD04", false )
    elseif head <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HEAD04", false )
	elseif head <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HEAD04", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD04", false )
    end
	
end   

function OnDwarvenVWCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Hammer1", true )
end

function OnKhazadGuardCreated(self)
    ObjectHideSubObjectPermanently( self, "HEAD1", true )
    ObjectHideSubObjectPermanently( self, "HEAD2", true )
    ObjectHideSubObjectPermanently( self, "HEAD3", true )

    local head          =    GetRandomNumber()

     if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD1", false )
    elseif head <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HEAD2", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD3", false )
    end    

end

function OnEredLuinTraderCreated(self)
    ObjectHideSubObjectPermanently( self, "CLOAK1", true )
    ObjectHideSubObjectPermanently( self, "CLOAK2", true )
    ObjectHideSubObjectPermanently( self, "CLOAK3", true )
    ObjectHideSubObjectPermanently( self, "CLOAK4", true )
    ObjectHideSubObjectPermanently( self, "BODY1", true )
    ObjectHideSubObjectPermanently( self, "BODY2", true )
    ObjectHideSubObjectPermanently( self, "BODY3", true )
    ObjectHideSubObjectPermanently( self, "HEAD1", true )
    ObjectHideSubObjectPermanently( self, "HEAD2", true )
    ObjectHideSubObjectPermanently( self, "HEAD3", true )
	
	ObjectHideSubObjectPermanently( self, "BANNER1", true )
    ObjectHideSubObjectPermanently( self, "BANNER2", true )

    local body         =    GetRandomNumber()
    local head          =    GetRandomNumber()
    local cloak          =    GetRandomNumber()
	local banner          =    GetRandomNumber()
	
	if banner <= 0.5 then
        ObjectHideSubObjectPermanently( self, "BANNER1", false )
     else
        ObjectHideSubObjectPermanently( self, "BANNER2", false )
    end  

    if cloak <= 0.3 then
        ObjectHideSubObjectPermanently( self, "CLOAK1", false )
    elseif cloak <= 0.6 then
        ObjectHideSubObjectPermanently( self, "CLOAK2", false )
	elseif cloak <= 0.8 then
        ObjectHideSubObjectPermanently( self, "CLOAK3", false )
     else
        ObjectHideSubObjectPermanently( self, "CLOAK4", false )
    end   

    if body <= 0.3 then
        ObjectHideSubObjectPermanently( self, "BODY1", false )
    elseif body <= 0.6 then
        ObjectHideSubObjectPermanently( self, "BODY2", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY3", false )
    end    

     if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD1", false )
    elseif head <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HEAD2", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD3", false )
    end    

end

function OnEredLuinBannerCreated(self)
	ObjectHideSubObjectPermanently( self, "BANNER1", true )
    ObjectHideSubObjectPermanently( self, "BANNER2", true )

	local banner          =    GetRandomNumber()
	
	if banner <= 0.5 then
        ObjectHideSubObjectPermanently( self, "BANNER1", false )
     else
        ObjectHideSubObjectPermanently( self, "BANNER2", false )
    end 

end

function OnDwarvenCivCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Hammer1", true )
    ObjectHideSubObjectPermanently( self, "PAULDRONS01", true )
    ObjectHideSubObjectPermanently( self, "CLOAK1", true )
    ObjectHideSubObjectPermanently( self, "CLOAK2", true )
    ObjectHideSubObjectPermanently( self, "CLOAK3", true )
    ObjectHideSubObjectPermanently( self, "CLOAK4", true )
    ObjectHideSubObjectPermanently( self, "CIV1", true )
    ObjectHideSubObjectPermanently( self, "CIV2", true )
    ObjectHideSubObjectPermanently( self, "CIV3", true )
    ObjectHideSubObjectPermanently( self, "CIV4", true )
    ObjectHideSubObjectPermanently( self, "LEGS1", true )
    ObjectHideSubObjectPermanently( self, "LEGS2", true )
    ObjectHideSubObjectPermanently( self, "LEGS3", true )
    ObjectHideSubObjectPermanently( self, "LEGS4", true )

    local helmet         =    GetRandomNumber()
    local head          =    GetRandomNumber()
    local armor          =    GetRandomNumber()

    if helmet <= 0.3 then
        ObjectHideSubObjectPermanently( self, "CLOAK1", false )
    elseif helmet <= 0.6 then
        ObjectHideSubObjectPermanently( self, "CLOAK2", false )
	elseif helmet <= 0.8 then
        ObjectHideSubObjectPermanently( self, "CLOAK2", false )
     else
        ObjectHideSubObjectPermanently( self, "CLOAK4", false )
    end   

    if head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "CIV1", false )
        ObjectHideSubObjectPermanently( self, "LEGS1", false )
    elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "CIV2", false )
        ObjectHideSubObjectPermanently( self, "LEGS2", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "CIV3", false )
        ObjectHideSubObjectPermanently( self, "LEGS3", false )
     else
        ObjectHideSubObjectPermanently( self, "CIV4", false )
        ObjectHideSubObjectPermanently( self, "LEGS4", false )
    end   

    if armor <= 0.5 then
        ObjectHideSubObjectPermanently( self, "PAULDRONS01", false )
     else
        ObjectHideSubObjectPermanently( self, "PAULDRONS02", false )
    end   

end

function OnDwarvenArcherCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Hammer1", true )
    ObjectHideSubObjectPermanently( self, "PAULDRONS01", true )
    ObjectHideSubObjectPermanently( self, "PAULDRONS02", true )
    ObjectHideSubObjectPermanently( self, "AXE01", true )
    ObjectHideSubObjectPermanently( self, "AXE02", true )
    ObjectHideSubObjectPermanently( self, "AXE03", true )
    ObjectHideSubObjectPermanently( self, "QUIVERA", true )
    ObjectHideSubObjectPermanently( self, "QUIVERB", true )
    ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
    ObjectHideSubObjectPermanently( self, "HEAD04", true )
    ObjectHideSubObjectPermanently( self, "HEAD05", true )
    ObjectHideSubObjectPermanently( self, "HEAD06", true )
    ObjectHideSubObjectPermanently( self, "HEAD07", true )
    ObjectHideSubObjectPermanently( self, "HEAD08", true )

    local helmet         =    GetRandomNumber()
    local head          =    GetRandomNumber()
    local armor          =    GetRandomNumber()
    local weaponleft     =    GetRandomNumber()

    if helmet <= 0.5 then
        ObjectHideSubObjectPermanently( self, "QUIVERA", false )
     else
        ObjectHideSubObjectPermanently( self, "QUIVERB", false )
    end   

    if head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif helmet <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
    elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    elseif head <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HEAD04", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEAD05", false )
    elseif head <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HEAD06", false )
    elseif head <= 0.9 then
        ObjectHideSubObjectPermanently( self, "HEAD07", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD08", false )
    end   

    if armor <= 0.5 then
        ObjectHideSubObjectPermanently( self, "PAULDRONS01", false )
     else
        ObjectHideSubObjectPermanently( self, "PAULDRONS02", false )
    end   
	
    if weaponleft <= 0.30 then
        ObjectHideSubObjectPermanently( self, "AXE01", false )
    elseif weaponleft <= 0.60 then
        ObjectHideSubObjectPermanently( self, "AXE02", false )
     else
        ObjectHideSubObjectPermanently( self, "AXE03", false )
    end  

end

function CreateAHeroHideEverything(self)
	ObjectHideSubObjectPermanently( self, "SWORD", true )
	ObjectHideSubObjectPermanently( self, "BOW", true )
	ObjectHideSubObjectPermanently( self, "BOW_03", true )
	ObjectHideSubObjectPermanently( self, "BOW_04", true )
	ObjectHideSubObjectPermanently( self, "BOW_05", true )
	ObjectHideSubObjectPermanently( self, "TRUNK01", true )
	ObjectHideSubObjectPermanently( self, "STAFF_LIGHT", true )
	ObjectHideSubObjectPermanently( self, "OBJECT01", true )
	
	ObjectHideSubObjectPermanently( self, "SHIELD01", true )
	ObjectHideSubObjectPermanently( self, "SHIELD_01", true )
	ObjectHideSubObjectPermanently( self, "SPEAR", true )
	ObjectHideSubObjectPermanently( self, "SHIELD_B", true )
	ObjectHideSubObjectPermanently( self, "SHIELD_C", true )
	ObjectHideSubObjectPermanently( self, "SHIELD_D", true )
	ObjectHideSubObjectPermanently( self, "B_SHIELD", true )
	ObjectHideSubObjectPermanently( self, "WEAPON_A", true )
	ObjectHideSubObjectPermanently( self, "WEAPON_B", true )
	ObjectHideSubObjectPermanently( self, "WEAPON_C", true )
	ObjectHideSubObjectPermanently( self, "WEAPON_D", true )
	
	ObjectHideSubObjectPermanently( self, "WEAP_01", true )
	ObjectHideSubObjectPermanently( self, "WEAP_02", true )
	ObjectHideSubObjectPermanently( self, "WEAP_03", true )
	ObjectHideSubObjectPermanently( self, "WEAP_04", true )
	ObjectHideSubObjectPermanently( self, "WEAP_05", true )
	ObjectHideSubObjectPermanently( self, "WEAP_06", true )
	
	ObjectHideSubObjectPermanently( self, "AXE02", true )

	ObjectHideSubObjectPermanently( self, "AxeWeapon", true )
	ObjectHideSubObjectPermanently( self, "Belthronding", true )
	-- ObjectHideSubObjectPermanently( self, "Mithlondrecurve", true )
	ObjectHideSubObjectPermanently( self, "Dwarf_Axe01", true )
	ObjectHideSubObjectPermanently( self, "FireBrand", true )
	ObjectHideSubObjectPermanently( self, "FireBrand_SM", true )
	ObjectHideSubObjectPermanently( self, "FireBrand_FX01", true )
	ObjectHideSubObjectPermanently( self, "FireBrand_FX02", true )
	ObjectHideSubObjectPermanently( self, "Gurthang", true )
	ObjectHideSubObjectPermanently( self, "Gurthang_SM", true )
	ObjectHideSubObjectPermanently( self, "HeroOfTheWestShield", true )
	ObjectHideSubObjectPermanently( self, "HeroOfTheWestShield_SM", true )
	ObjectHideSubObjectPermanently( self, "MithlondBow", true )
	ObjectHideSubObjectPermanently( self, "TrollBane", true )
	ObjectHideSubObjectPermanently( self, "TrollBane_SM", true )
	ObjectHideSubObjectPermanently( self, "TrollBane_FX01", true )
	ObjectHideSubObjectPermanently( self, "TrollBane_FX02", true )
	ObjectHideSubObjectPermanently( self, "TrollMace", true )
	ObjectHideSubObjectPermanently( self, "TrollSword", true )
	ObjectHideSubObjectPermanently( self, "WestronSword", true )
	ObjectHideSubObjectPermanently( self, "WestronSword", true )
	ObjectHideSubObjectPermanently( self, "WestronSword_SM", true )
	ObjectHideSubObjectPermanently( self, "WizardStaff01", true )
	ObjectHideSubObjectPermanently( self, "WizStaff01_FX01", true )
	ObjectHideSubObjectPermanently( self, "WizStaff01_FX2", true )
	ObjectHideSubObjectPermanently( self, "WizStaff01_FX3", true )
	ObjectHideSubObjectPermanently( self, "WizStaff01_FX4", true )
	ObjectHideSubObjectPermanently( self, "WizardStaff02", true )
	ObjectHideSubObjectPermanently( self, "WizStaff02_FX1", true )
	ObjectHideSubObjectPermanently( self, "WizardStaff03", true )
	ObjectHideSubObjectPermanently( self, "WizStaff03_FX01", true )
	ObjectHideSubObjectPermanently( self, "WizStaff03_FX02", true )
	ObjectHideSubObjectPermanently( self, "WizardStaff04", true )
	ObjectHideSubObjectPermanently( self, "WizStaff04_FX01", true )
	ObjectHideSubObjectPermanently( self, "WizStaff04_FX02", true )
	ObjectHideSubObjectPermanently( self, "WizStaff04_FX03", true )
	ObjectHideSubObjectPermanently( self, "WizStaff04_FX04", true )
	ObjectHideSubObjectPermanently( self, "WizStaff04_FX05", true )
	ObjectHideSubObjectPermanently( self, "WizStaff04_FX06", true )
	ObjectHideSubObjectPermanently( self, "WizStaff04_FX07", true )
	ObjectHideSubObjectPermanently( self, "WizardSword", true )
	ObjectHideSubObjectPermanently( self, "CMSword01", true )
	ObjectHideSubObjectPermanently( self, "CMSword02", true )
	ObjectHideSubObjectPermanently( self, "CHEST_00", true )	
	ObjectHideSubObjectPermanently( self, "CHEST_01", true )	
	ObjectHideSubObjectPermanently( self, "CHEST_02", true )
	ObjectHideSubObjectPermanently( self, "CHEST_03", true)
	ObjectHideSubObjectPermanently( self, "CHEST_04", true )	
	ObjectHideSubObjectPermanently( self, "CHEST_05", true )
	ObjectHideSubObjectPermanently( self, "CHEST_06", true)
	ObjectHideSubObjectPermanently( self, "BOOT_00", true )
	ObjectHideSubObjectPermanently( self, "BOOT_01", true )
	ObjectHideSubObjectPermanently( self, "BOOT_02", true )
	ObjectHideSubObjectPermanently( self, "BOOT_03", true )
	ObjectHideSubObjectPermanently( self, "BOOT_04", true )
	ObjectHideSubObjectPermanently( self, "BOOT_05", true )
	ObjectHideSubObjectPermanently( self, "BOOT_06", true )
	ObjectHideSubObjectPermanently( self, "SHLD_00", true )
	ObjectHideSubObjectPermanently( self, "SHLD_01", true )
	ObjectHideSubObjectPermanently( self, "SHLD_02", true )
	ObjectHideSubObjectPermanently( self, "SHLD_03", true )
	ObjectHideSubObjectPermanently( self, "SHLD_04", true )
	ObjectHideSubObjectPermanently( self, "SHLD_05", true )
	ObjectHideSubObjectPermanently( self, "SHLD_06", true )
	ObjectHideSubObjectPermanently( self, "SHLD_07", true )
	ObjectHideSubObjectPermanently( self, "SHLD_08", true )
	ObjectHideSubObjectPermanently( self, "SHLD_09", true )
	ObjectHideSubObjectPermanently( self, "SLDR_00", true )
	ObjectHideSubObjectPermanently( self, "SLDR_01", true )
	ObjectHideSubObjectPermanently( self, "SLDR_02", true )
	ObjectHideSubObjectPermanently( self, "SLDR_03", true )
	ObjectHideSubObjectPermanently( self, "SLDR_04", true )
	ObjectHideSubObjectPermanently( self, "SLDR_05", true )
	ObjectHideSubObjectPermanently( self, "SLDR_06", true )
	ObjectHideSubObjectPermanently( self, "SLDR_07", true )
	ObjectHideSubObjectPermanently( self, "SLDR_08", true )	
	ObjectHideSubObjectPermanently( self, "Shield_1OG", true )
	ObjectHideSubObjectPermanently( self, "Shield_2OG", true )
	ObjectHideSubObjectPermanently( self, "HAIR_00", true )
	ObjectHideSubObjectPermanently( self, "HAIR_01", true )
	ObjectHideSubObjectPermanently( self, "HLMT_00", true )
	ObjectHideSubObjectPermanently( self, "HLMT_01", true )
	ObjectHideSubObjectPermanently( self, "HLMT_02", true )
	ObjectHideSubObjectPermanently( self, "HLMT_03", true )
	ObjectHideSubObjectPermanently( self, "HLMT_03A", true )
	ObjectHideSubObjectPermanently( self, "HLMT_03B", true )
	ObjectHideSubObjectPermanently( self, "HLMT_04", true )
	ObjectHideSubObjectPermanently( self, "HLMT_05", true )
	ObjectHideSubObjectPermanently( self, "HLMT_05A", true )
	ObjectHideSubObjectPermanently( self, "HLMT_05B", true )
	ObjectHideSubObjectPermanently( self, "HLMT_06", true )
	ObjectHideSubObjectPermanently( self, "HLMT_07", true )
	ObjectHideSubObjectPermanently( self, "HLMT_07_LOD1", true )
	ObjectHideSubObjectPermanently( self, "HLMT_08", true )
	ObjectHideSubObjectPermanently( self, "HLMT_09", true )
	ObjectHideSubObjectPermanently( self, "HLMT_10", true )
	ObjectHideSubObjectPermanently( self, "GNLT_00", true )
	ObjectHideSubObjectPermanently( self, "GNLT_01", true )
	ObjectHideSubObjectPermanently( self, "GNLT_02", true )
	ObjectHideSubObjectPermanently( self, "GNLT_03", true )
	ObjectHideSubObjectPermanently( self, "GNLT_04", true )
	ObjectHideSubObjectPermanently( self, "GNLT_05", true )
	ObjectHideSubObjectPermanently( self, "GNLT_06", true )
	ObjectHideSubObjectPermanently( self, "GNLT_07", true )
	ObjectHideSubObjectPermanently( self, "GNLT_08", true )
	ObjectHideSubObjectPermanently( self, "GHLT_08", true )
	ObjectHideSubObjectPermanently( self, "GNLT_09", true )
	ObjectHideSubObjectPermanently( self, "GNLT_09_LOD1", true )
	ObjectHideSubObjectPermanently( self, "GNLT_10", true )
	ObjectHideSubObjectPermanently( self, "SPR_01", true )
	ObjectHideSubObjectPermanently( self, "PAXE_01", true )
	ObjectHideSubObjectPermanently( self, "PAXE_01_LOD1", true )
	ObjectHideSubObjectPermanently( self, "SWRD_01", true )
	ObjectHideSubObjectPermanently( self, "SWRD_02", true )
	ObjectHideSubObjectPermanently( self, "SWRD_03", true )
	ObjectHideSubObjectPermanently( self, "SWRD_04", true )
	ObjectHideSubObjectPermanently( self, "SWRD_05", true )
	ObjectHideSubObjectPermanently( self, "SWD_01", true )
	ObjectHideSubObjectPermanently( self, "SWD_02", true )
	ObjectHideSubObjectPermanently( self, "SWD_03", true )
	ObjectHideSubObjectPermanently( self, "SWD_04", true )
	ObjectHideSubObjectPermanently( self, "STFF_05", true )
	ObjectHideSubObjectPermanently( self, "STFF_06", true )
	ObjectHideSubObjectPermanently( self, "objSLDR_01", true )
	ObjectHideSubObjectPermanently( self, "objSLDR_02", true )
	ObjectHideSubObjectPermanently( self, "objSLDR_03", true )
	ObjectHideSubObjectPermanently( self, "objHLMT_01", true )
	ObjectHideSubObjectPermanently( self, "objHLMT_02", true )
	ObjectHideSubObjectPermanently( self, "objHLMT_03", true )
	ObjectHideSubObjectPermanently( self, "objHLMT_04", true )	
	ObjectHideSubObjectPermanently( self, "Uruk_Sword_01", true )
	ObjectHideSubObjectPermanently( self, "Uruk_Sword_02", true )
	ObjectHideSubObjectPermanently( self, "Uruk_Sword_03", true )
	ObjectHideSubObjectPermanently( self, "TrollTree", true )
	ObjectHideSubObjectPermanently( self, "TrollHammer", true )
	ObjectHideSubObjectPermanently( self, "CLUB_01", true )
	ObjectHideSubObjectPermanently( self, "CLUB_02", true )
	ObjectHideSubObjectPermanently( self, "CLUB_03", true )
	ObjectHideSubObjectPermanently( self, "HMR_01", true )
	ObjectHideSubObjectPermanently( self, "HMR_02", true )
	ObjectHideSubObjectPermanently( self, "HMR_03", true )
	ObjectHideSubObjectPermanently( self, "HMR_04", true )
	ObjectHideSubObjectPermanently( self, "BEARD_04", true )
	ObjectHideSubObjectPermanently( self, "BEARD_03", true )
	ObjectHideSubObjectPermanently( self, "BEARD_02", true )
	ObjectHideSubObjectPermanently( self, "BEARD_01", true )
	ObjectHideSubObjectPermanently( self, "BODY_01", true )
	ObjectHideSubObjectPermanently( self, "BODY_02", true )
	ObjectHideSubObjectPermanently( self, "BODY_03", true )
	ObjectHideSubObjectPermanently( self, "BODY_04", true )
	ObjectHideSubObjectPermanently( self, "BODY_05", true )
	ObjectHideSubObjectPermanently( self, "BODY_06", true )
	ObjectHideSubObjectPermanently( self, "BODY_07", true )
	ObjectHideSubObjectPermanently( self, "BODY_08", true )
	ObjectHideSubObjectPermanently( self, "AUX_01", true )
	ObjectHideSubObjectPermanently( self, "AUX_02", true )
	ObjectHideSubObjectPermanently( self, "AUX_03", true )
	ObjectHideSubObjectPermanently( self, "AUX_04", true )
	ObjectHideSubObjectPermanently( self, "AUX_05", true )
	ObjectHideSubObjectPermanently( self, "AUX_06", true )
	ObjectHideSubObjectPermanently( self, "AUX_07", true )
	ObjectHideSubObjectPermanently( self, "AUX_07", true )
	ObjectHideSubObjectPermanently( self, "AUX_08", true )
	ObjectHideSubObjectPermanently( self, "AUX_09", true )
	ObjectHideSubObjectPermanently( self, "AXE_01", true )
	ObjectHideSubObjectPermanently( self, "AXE_02", true )
	ObjectHideSubObjectPermanently( self, "AXE_03", true )
	ObjectHideSubObjectPermanently( self, "AXE_04", true )
	ObjectHideSubObjectPermanently( self, "AXE_05", true )
	ObjectHideSubObjectPermanently( self, "AXE_06", true )
	ObjectHideSubObjectPermanently( self, "AXE_07", true )
	ObjectHideSubObjectPermanently( self, "AXE_08", true )
	ObjectHideSubObjectPermanently( self, "AXE_09", true )
	ObjectHideSubObjectPermanently( self, "AXE_10", true )
	ObjectHideSubObjectPermanently( self, "AXE_11", true )
	ObjectHideSubObjectPermanently( self, "AXE_12", true )
	ObjectHideSubObjectPermanently( self, "AXE_13", true )
	ObjectHideSubObjectPermanently( self, "AXE_14", true )
	ObjectHideSubObjectPermanently( self, "AXE_15", true )
	ObjectHideSubObjectPermanently( self, "AXE_16", true )
	ObjectHideSubObjectPermanently( self, "AXE_17", true )
	ObjectHideSubObjectPermanently( self, "AXE_18", true )
	ObjectHideSubObjectPermanently( self, "BARREL", true )
	ObjectHideSubObjectPermanently( self, "OBJECT02", true )	-- Barrel on the Orc Raider
	ObjectHideSubObjectPermanently( self, "ARROW", true )
	ObjectHideSubObjectPermanently( self, "PLANE02", true )	
end

function OnCreateAHeroFunctions(self)
	CreateAHeroHideEverything(self)
end

function OnEvilShipCreated(self)
	ObjectHideSubObjectPermanently( self, "CAULDRON", true )
	ObjectHideSubObjectPermanently( self, "CAULDRON_FIRE", true )
	ObjectHideSubObjectPermanently( self, "CAULDRON_TOP", true )
	ObjectHideSubObjectPermanently( self, "CROWSNEST", true )
	ObjectHideSubObjectPermanently( self, "FLAG", true )
	ObjectHideSubObjectPermanently( self, "BANNER", true )
end

function OnGoodShipCreated(self)
	ObjectHideSubObjectPermanently( self, "UG_FLAMING_01", true )
	ObjectHideSubObjectPermanently( self, "UG_FLAMING_02", true )
	ObjectHideSubObjectPermanently( self, "UG_FLAMING_FIRE", true )
	ObjectHideSubObjectPermanently( self, "UG_ARMOR", true )
	ObjectHideSubObjectPermanently( self, "BANNER", true )
end

function OnShipWrightCreated(self)
	ObjectHideSubObjectPermanently( self, "GoodPart_A", true )
	ObjectHideSubObjectPermanently( self, "GoodPart_B", true )
	ObjectHideSubObjectPermanently( self, "EvilPart_A", true )
	ObjectHideSubObjectPermanently( self, "EvilPart_B", true )
end

function OnDormitoryBuildVariation(self,variation)

	local var = tonumber(variation)

	if var == 1 then
		ObjectSetGeometryActive( self, "VersionOne", true )
		ObjectSetGeometryActive( self, "VersionTwo", false )
	elseif var == 2 then
		ObjectSetGeometryActive( self, "VersionOne", false )
		ObjectSetGeometryActive( self, "VersionTwo", true )
	end

end

function OnEredLuinCitadelCreated(self)
	ObjectHideSubObjectPermanently( self, "AXE02", true )
	ObjectHideSubObjectPermanently( self, "AXE1", true )
	ObjectHideSubObjectPermanently( self, "EUGUARD", true )	
	ObjectHideSubObjectPermanently( self, "EUHEAD", true )
	ObjectHideSubObjectPermanently( self, "HA", true )
end

function OnEreborFortressCreated(self)
	ObjectHideSubObjectPermanently( self, "MOAT", true )
	ObjectHideSubObjectPermanently( self, "WATER", true )
	ObjectHideSubObjectPermanently( self, "BRAZIERS", true )	
	ObjectHideSubObjectPermanently( self, "BANNERS", true )
	ObjectHideSubObjectPermanently( self, "BANNERS2", true )
	ObjectHideSubObjectPermanently( self, "STATUESA", true )
	ObjectHideSubObjectPermanently( self, "STATUESB", true )
	ObjectHideSubObjectPermanently( self, "BUNKERICE", true )
	ObjectHideSubObjectPermanently( self, "HORNS", true )
	ObjectHideSubObjectPermanently( self, "FORTS", true )
	ObjectHideSubObjectPermanently( self, "ARKENA", true )
	ObjectHideSubObjectPermanently( self, "ARKENB", true )
	ObjectSetGeometryActive( self, "HighTowerGeom", false )
end

function OnRohanFortressCreated(self)
	ObjectHideSubObjectPermanently( self, "BANNERS01", true )
	ObjectHideSubObjectPermanently( self, "BELL", true )
	ObjectHideSubObjectPermanently( self, "HORNS", true )	
	ObjectHideSubObjectPermanently( self, "MEADHALL_A", true )
	ObjectHideSubObjectPermanently( self, "MEADHALL_B", true )
	ObjectHideSubObjectPermanently( self, "HASTY", true )
	ObjectHideSubObjectPermanently( self, "STATUESB", true )
	ObjectSetGeometryActive( self, "HighTowerGeom", false )
end

function OnFortressCreated(self)
	ObjectHideSubObjectPermanently( self, "DBFBANNER", true )	
	ObjectSetGeometryActive( self, "HighTowerGeom", false )
end

function OnMinasMorgulCreated(self)
	ObjectHideSubObjectPermanently( self, "CYLINDER01", true )	
	ObjectSetGeometryActive( self, "HighTowerGeom", false )
end

function OnGateWatcherBuilt(self)
	ObjectDoSpecialPower(self, "SpecialAbilityGateWatchersFear")
end	

function NeutralGollum_RingStealerDamaged(self,other)

	if ObjectHasUpgrade( other, "Upgrade_RingHero" ) == 0 then
		ObjectChangeAllegianceFromNonPlayablePlayer( self, other )
	end
	
end

function NeutralGollum_RingStealerSlaughtered(self,other)
	ObjectRemoveUpgrade( other, "Upgrade_RingHero" )
end

function OnNecromancerStatueCreated(self)
	ObjectDoSpecialPower(self, "SpecialAbilityGateWatchersFear")
end

function OnKennelWolfCreated(self) --
	ObjectGrantUpgrade( self, "Upgrade_AngmarSpikedCollar" )
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "Glow", true )
end

function BecomeDismounted(self) --
    ObjectRemoveUpgrade( self, "Upgrade_AITriggerMount" )
    ObjectGrantUpgrade( self, "Upgrade_AITriggerDismount" )
end

function BecomeMounted(self) --
    ObjectRemoveUpgrade( self, "Upgrade_AITriggerDismount" )
    ObjectGrantUpgrade( self, "Upgrade_AITriggerMount" )
end

function OnGundabadOrcCreated(self)

    ObjectHideSubObjectPermanently( self, "WEAPON1", true )
    ObjectHideSubObjectPermanently( self, "WEAPON2", true )
    ObjectHideSubObjectPermanently( self, "WEAPON3", true )

    ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
    ObjectHideSubObjectPermanently( self, "FORGED_BLADES", true )

    local weapon         =    GetRandomNumber()

    if weapon <= 0.333 then
        ObjectHideSubObjectPermanently( self, "WEAPON1", false )
    elseif weapon <= 0.666 then
        ObjectHideSubObjectPermanently( self, "WEAPON2", false )
     else
        ObjectHideSubObjectPermanently( self, "WEAPON3", false )
    end
end 

function OnBodyGuardOrcCreated(self)
    ObjectHideSubObjectPermanently( self, "HEADS01", true )
    ObjectHideSubObjectPermanently( self, "HEADS02", true )

    ObjectHideSubObjectPermanently( self, "SHDLR1", true )
    ObjectHideSubObjectPermanently( self, "SHDLR2", true )
    ObjectHideSubObjectPermanently( self, "SHDLR3", true )
    ObjectHideSubObjectPermanently( self, "AXEA", true )
    ObjectHideSubObjectPermanently( self, "SWORDB", true )
    ObjectHideSubObjectPermanently( self, "SWORDA", true )

    ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
    ObjectHideSubObjectPermanently( self, "FORGED_BLADES", true )
    ObjectHideSubObjectPermanently( self, "FireArowTip", true )

    local heads          =    GetRandomNumber()
    local armor          =    GetRandomNumber()
    local weapon          =    GetRandomNumber()

    if heads <= 0.500 then
        ObjectHideSubObjectPermanently( self, "HEADS01", false )
     else
        ObjectHideSubObjectPermanently( self, "HEADS02", false )
    end  

    if armor <= 0.2 then
        ObjectHideSubObjectPermanently( self, "SHDLR1", false )
    elseif armor <= 0.3 then
        ObjectHideSubObjectPermanently( self, "SHDLR2", false )
	elseif armor <= 0.5 then
        ObjectHideSubObjectPermanently( self, "SHDLR3", false )
    else
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
    end

    if weapon <= 0.3 then
        ObjectHideSubObjectPermanently( self, "SWORDA", false )
    elseif weapon <= 0.6 then
        ObjectHideSubObjectPermanently( self, "SWORDB", false )
    else
        ObjectHideSubObjectPermanently( self, "AXEA", false )
    end
end

function OnDolGuldurOrcCreated(self)

    ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
    ObjectHideSubObjectPermanently( self, "FORGED_BLADES", true )
    ObjectHideSubObjectPermanently( self, "FORGED_BLADE01", true )
    ObjectHideSubObjectPermanently( self, "FireArowTip", true )
end

function OnDolGuldurOrcOldCreated(self)

    ObjectHideSubObjectPermanently( self, "s1", true )
    ObjectHideSubObjectPermanently( self, "s2", true )
    ObjectHideSubObjectPermanently( self, "s3", true )

    ObjectHideSubObjectPermanently( self, "h1", true )
    ObjectHideSubObjectPermanently( self, "h2", true )
    ObjectHideSubObjectPermanently( self, "h3", true )
    ObjectHideSubObjectPermanently( self, "h4", true )
    ObjectHideSubObjectPermanently( self, "h5", true )

    ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
    ObjectHideSubObjectPermanently( self, "FORGED_BLADES", true )
    ObjectHideSubObjectPermanently( self, "FireArowTip", true )

    local helmet         =    GetRandomNumber()
    local armor          =    GetRandomNumber()

    if helmet <= 0.333 then
        ObjectHideSubObjectPermanently( self, "s1", false )
    elseif helmet <= 0.666 then
        ObjectHideSubObjectPermanently( self, "s2", false )
     else
        ObjectHideSubObjectPermanently( self, "s3", false )
    end   

    if armor <= 0.200 then
        ObjectHideSubObjectPermanently( self, "h1", false )
    elseif armor <= 0.400 then
        ObjectHideSubObjectPermanently( self, "h2", false )
    elseif armor <= 0.600 then
        ObjectHideSubObjectPermanently( self, "h3", false )
    elseif armor <= 0.800 then
        ObjectHideSubObjectPermanently( self, "h4", false )
    else
        ObjectHideSubObjectPermanently( self, "h5", false )
    end
end

function OnMountainOrcCreated(self)

    ObjectHideSubObjectPermanently( self, "BEARD01", true )
    ObjectHideSubObjectPermanently( self, "BEARD02", true )
    ObjectHideSubObjectPermanently( self, "BEARD03", true )
    ObjectHideSubObjectPermanently( self, "BEARD04", true )
	
    ObjectHideSubObjectPermanently( self, "HAIR01", true )
    ObjectHideSubObjectPermanently( self, "HAIR02", true )
    ObjectHideSubObjectPermanently( self, "HAIR03", true )
	
    ObjectHideSubObjectPermanently( self, "FUR_L", true )
    ObjectHideSubObjectPermanently( self, "FUR_R", true )
	
    ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
	
    ObjectHideSubObjectPermanently( self, "CHIPS", true )
    ObjectHideSubObjectPermanently( self, "BODY01", true )
    ObjectHideSubObjectPermanently( self, "BODY02", true )
	
    ObjectHideSubObjectPermanently( self, "HELMET01", true )
    ObjectHideSubObjectPermanently( self, "HELMET02", true )
    ObjectHideSubObjectPermanently( self, "HELMET03", true )

    ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
    ObjectHideSubObjectPermanently( self, "FireArowTip", true )

    local helmet         =    GetRandomNumber()
    local hair         =    GetRandomNumber()
    local fur         =    GetRandomNumber()
    local beard         =    GetRandomNumber()
    local head         =    GetRandomNumber()
    local armor          =    GetRandomNumber()
    local chips          =    GetRandomNumber()

    if fur <= 0.3 then
        ObjectHideSubObjectPermanently( self, "FUR_L", false )
    elseif fur <= 0.5 then
        ObjectHideSubObjectPermanently( self, "FUR_R", false )
    elseif fur <= 0.8 then
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
     else
        ObjectHideSubObjectPermanently( self, "FUR_L", false )
        ObjectHideSubObjectPermanently( self, "FUR_R", false )
    end 
	
    if hair <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HAIR01", false )
    elseif hair <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HAIR02", false )
    elseif hair <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HAIR03", false )
     else
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
    end 
	
    if helmet <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HELMET01", false )
    elseif helmet <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HELMET02", false )
     else
        ObjectHideSubObjectPermanently( self, "HELMET03", false )
    end 
	
    if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end 
	
    if armor <= 0.5 then
        ObjectHideSubObjectPermanently( self, "CHIPS", false )
     else
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
    end 
	
    if chips <= 0.5 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY02", false )
    end 

    if beard <= 0.3 then
        ObjectHideSubObjectPermanently( self, "BEARD01", false )
    elseif beard <= 0.5 then
        ObjectHideSubObjectPermanently( self, "BEARD02", false )
    elseif beard <= 0.8 then
        ObjectHideSubObjectPermanently( self, "BEARD03", false )
    elseif beard <= 0.8 then
        ObjectHideSubObjectPermanently( self, "BEARD04", false )
     else
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
    end 	
end

function OnGondorPGCreated(self)

    ObjectHideSubObjectPermanently( self, "BODY01", true )
    ObjectHideSubObjectPermanently( self, "BODY02", true )
    ObjectHideSubObjectPermanently( self, "BODY03", true )
    ObjectHideSubObjectPermanently( self, "BODY04", true )
    ObjectHideSubObjectPermanently( self, "HORSE01", true )
    ObjectHideSubObjectPermanently( self, "HORSE02", true )
    ObjectHideSubObjectPermanently( self, "HORSE03", true )
    ObjectHideSubObjectPermanently( self, "HORSE04", true )
    ObjectHideSubObjectPermanently( self, "HORSE05", true )
    ObjectHideSubObjectPermanently( self, "HORSE06", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD01", true )

    ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )

    local head         =    GetRandomNumber()
    local horse         =    GetRandomNumber()
    local body 		=   GetRandomNumber()

    if head <= 0.500 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
    end 

    if horse <= 0.2 then
        ObjectHideSubObjectPermanently( self, "HORSE01", false )
    elseif horse <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HORSE02", false )
    elseif horse <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HORSE03", false )
    elseif horse <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HORSE04", false )
    elseif horse <= 0.9 then
        ObjectHideSubObjectPermanently( self, "HORSE05", false )
     else
        ObjectHideSubObjectPermanently( self, "HORSE06", false )
    end   

    if body <= 0.3 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
    elseif body <= 0.6 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
    elseif body <= 0.8 then
        ObjectHideSubObjectPermanently( self, "BODY03", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY04", false )
    end  
 
end 

function OnIthilienRangerCreated(self)

    ObjectHideSubObjectPermanently( self, "FireArowTip", true )
	
	;------------------------HEADS UNHOODED
	ObjectHideSubObjectPermanently( self, "HEAD1", true )
	ObjectHideSubObjectPermanently( self, "HEAD2", true )
	ObjectHideSubObjectPermanently( self, "HEAD3", true )
	ObjectHideSubObjectPermanently( self, "HEAD4", true )
	ObjectHideSubObjectPermanently( self, "HEAD5", true )
	ObjectHideSubObjectPermanently( self, "HEAD6", true )
	;------------------------HEADS HOODED
	ObjectHideSubObjectPermanently( self, "HEADHD1", true )
	ObjectHideSubObjectPermanently( self, "HEADHD2", true )
	ObjectHideSubObjectPermanently( self, "HEADHD3", true )
	ObjectHideSubObjectPermanently( self, "HEADHD4", true )
	ObjectHideSubObjectPermanently( self, "HEADHD5", true )
	ObjectHideSubObjectPermanently( self, "HEADHD6", true )
	;------------------------HEADS HOODED MASKED
	ObjectHideSubObjectPermanently( self, "HEADHDMSK1", true )	
	ObjectHideSubObjectPermanently( self, "HEADHDMSK2", true )
	ObjectHideSubObjectPermanently( self, "HEADHDMSK3", true )
	ObjectHideSubObjectPermanently( self, "HEADHDMSK4", true )
	ObjectHideSubObjectPermanently( self, "HEADHDMSK5", true )
	ObjectHideSubObjectPermanently( self, "HEADHDMSK6", true )
	;------------------------CLOAKS	
	ObjectHideSubObjectPermanently( self, "CLOAK1", true )
	ObjectHideSubObjectPermanently( self, "CLOAK2", true )
	ObjectHideSubObjectPermanently( self, "CLOAK3", true )
	ObjectHideSubObjectPermanently( self, "CLOAK4", true )
	ObjectHideSubObjectPermanently( self, "CLOAK5", true )
	ObjectHideSubObjectPermanently( self, "CLOAK6", true )
	ObjectHideSubObjectPermanently( self, "CLOAK7", true )
	;------------------------HOODS	
	ObjectHideSubObjectPermanently( self, "HOOD1", true )
	ObjectHideSubObjectPermanently( self, "HOOD2", true )
	ObjectHideSubObjectPermanently( self, "HOOD3", true )
	ObjectHideSubObjectPermanently( self, "HOOD4", true )
	ObjectHideSubObjectPermanently( self, "HOOD5", true )
	ObjectHideSubObjectPermanently( self, "HOOD6", true )
	ObjectHideSubObjectPermanently( self, "HOOD7", true )
	;------------------------HOODSDOWN	
	ObjectHideSubObjectPermanently( self, "HOODDOWN1", true )
	ObjectHideSubObjectPermanently( self, "HOODDOWN2", true )
	ObjectHideSubObjectPermanently( self, "HOODDOWN3", true )
	ObjectHideSubObjectPermanently( self, "HOODDOWN4", true )
	ObjectHideSubObjectPermanently( self, "HOODDOWN5", true )
	ObjectHideSubObjectPermanently( self, "HOODDOWN6", true )
	ObjectHideSubObjectPermanently( self, "HOODDOWN7", true )
	;------------------------BODY
	ObjectHideSubObjectPermanently( self, "BODY1", true )
	ObjectHideSubObjectPermanently( self, "BODY2", true )
	ObjectHideSubObjectPermanently( self, "BODY3", true )
	ObjectHideSubObjectPermanently( self, "BODY4", true )
	ObjectHideSubObjectPermanently( self, "BODY5", true )
	ObjectHideSubObjectPermanently( self, "BODY6", true )
	ObjectHideSubObjectPermanently( self, "BODY7", true )
	;------------------------ARMS
	ObjectHideSubObjectPermanently( self, "ARMS1", true )
	ObjectHideSubObjectPermanently( self, "ARMS2", true )
	ObjectHideSubObjectPermanently( self, "ARMS3", true )
	;------------------------LEGS
	ObjectHideSubObjectPermanently( self, "LEGS1", true )
	ObjectHideSubObjectPermanently( self, "LEGS2", true )
	;------------------------TABARDS
	ObjectHideSubObjectPermanently( self, "TABARD1", true )
	ObjectHideSubObjectPermanently( self, "TABARD2", true )
	ObjectHideSubObjectPermanently( self, "TABARD3", true )
	;------------------------QUIVERS
	ObjectHideSubObjectPermanently( self, "QUIVER1", true )
	ObjectHideSubObjectPermanently( self, "QUIVER2", true )
	ObjectHideSubObjectPermanently( self, "QUIVER3", true )
    
    ObjectHideSubObjectPermanently( self, "RANGER", true )
    ObjectHideSubObjectPermanently( self, "RANGERHOOD", true )
    ObjectHideSubObjectPermanently( self, "RANGERTABARD", true )
    ObjectHideSubObjectPermanently( self, "RANGERHOODTABAR", true )
    
	local head	   =    GetRandomNumber()
	local head2	   =    GetRandomNumber()
    local body    =    GetRandomNumber()
	local cloak    =    GetRandomNumber()
	local leg    =    GetRandomNumber()
	local arm    =    GetRandomNumber()
	
	local quiver    =    GetRandomNumber()
	local tabard   =    GetRandomNumber()
	
	if head <= 0.15 then
        ObjectHideSubObjectPermanently( self, "HEAD1", false )
    elseif head <= 0.30 then
        ObjectHideSubObjectPermanently( self, "HEAD2", false )
    elseif head <= 0.45 then
        ObjectHideSubObjectPermanently( self, "HEAD3", false )
	elseif head <= 0.60 then
        ObjectHideSubObjectPermanently( self, "HEAD4", false ) 
	elseif head <= 0.75 then
        ObjectHideSubObjectPermanently( self, "HEAD5", false )  		
    else
        ObjectHideSubObjectPermanently( self, "HEAD6", false )
    end
	
	if head2 <= 0.08 then
        ObjectHideSubObjectPermanently( self, "HEADHD1", false )
    elseif head <= 0.16 then
        ObjectHideSubObjectPermanently( self, "HEADHD2", false )
    elseif head <= 0.24 then
        ObjectHideSubObjectPermanently( self, "HEADHD3", false )
	elseif head <= 0.32 then
        ObjectHideSubObjectPermanently( self, "HEADHD4", false ) 
	elseif head <= 0.40 then
        ObjectHideSubObjectPermanently( self, "HEADHD5", false ) 
	elseif head <= 0.48 then
        ObjectHideSubObjectPermanently( self, "HEADHD6", false )  
	elseif head <= 0.56 then
        ObjectHideSubObjectPermanently( self, "HEADHDMSK1", false )
	elseif head <= 0.64 then
        ObjectHideSubObjectPermanently( self, "HEADHDMSK2", false )
	elseif head <= 0.72 then
        ObjectHideSubObjectPermanently( self, "HEADHDMSK3", false )
	elseif head <= 0.80 then
        ObjectHideSubObjectPermanently( self, "HEADHDMSK4", false )
	elseif head <= 0.88 then
        ObjectHideSubObjectPermanently( self, "HEADHDMSK5", false )
    else
        ObjectHideSubObjectPermanently( self, "HEADHDMSK6", false )
    end

    if body <= 0.14 then
        ObjectHideSubObjectPermanently( self, "BODY1", false )
    elseif body <= 0.28 then
        ObjectHideSubObjectPermanently( self, "BODY2", false )
    elseif body <= 0.42 then
        ObjectHideSubObjectPermanently( self, "BODY3", false )
	elseif body <= 0.56 then
        ObjectHideSubObjectPermanently( self, "BODY4", false ) 
	elseif body <= 0.70 then
        ObjectHideSubObjectPermanently( self, "BODY5", false )
	elseif body <= 0.84 then
        ObjectHideSubObjectPermanently( self, "BODY6", false )    		
    else
        ObjectHideSubObjectPermanently( self, "BODY7", false )
    end
	
	if cloak <= 0.14 then
        ObjectHideSubObjectPermanently( self, "CLOAK1", false )
        ObjectHideSubObjectPermanently( self, "HOOD1", false )
		ObjectHideSubObjectPermanently( self, "HOODDOWN1", false )
    elseif cloak <= 0.28 then
        ObjectHideSubObjectPermanently( self, "CLOAK2", false )
        ObjectHideSubObjectPermanently( self, "HOOD2", false )
		ObjectHideSubObjectPermanently( self, "HOODDOWN2", false )
	elseif cloak <= 0.42 then
        ObjectHideSubObjectPermanently( self, "CLOAK3", false )
        ObjectHideSubObjectPermanently( self, "HOOD3", false )
		ObjectHideSubObjectPermanently( self, "HOODDOWN3", false )
	elseif cloak <= 0.56 then
        ObjectHideSubObjectPermanently( self, "CLOAK4", false )
        ObjectHideSubObjectPermanently( self, "HOOD4", false )
		ObjectHideSubObjectPermanently( self, "HOODDOWN4", false )
	elseif cloak <= 0.70 then
        ObjectHideSubObjectPermanently( self, "CLOAK5", false )
        ObjectHideSubObjectPermanently( self, "HOOD5", false )
		ObjectHideSubObjectPermanently( self, "HOODDOWN5", false )
	elseif cloak <= 0.86 then
        ObjectHideSubObjectPermanently( self, "CLOAK6", false )
        ObjectHideSubObjectPermanently( self, "HOOD6", false )
		ObjectHideSubObjectPermanently( self, "HOODDOWN6", false )
     else
        ObjectHideSubObjectPermanently( self, "CLOAK7", false )
        ObjectHideSubObjectPermanently( self, "HOOD7", false )
		ObjectHideSubObjectPermanently( self, "HOODDOWN7", false )
    end  
	
	if arm <= 0.333 then
        ObjectHideSubObjectPermanently( self, "ARMS1", false )
    elseif arm <= 0.666 then
        ObjectHideSubObjectPermanently( self, "ARMS2", false )
    else
        ObjectHideSubObjectPermanently( self, "ARMS3", false )
    end
	
	if quiver <= 0.333 then
        ObjectHideSubObjectPermanently( self, "QUIVER1", false )
    elseif quiver <= 0.666 then
        ObjectHideSubObjectPermanently( self, "QUIVER2", false )
    else
        ObjectHideSubObjectPermanently( self, "QUIVER3", false )
    end
	
	if tabard <= 0.20 then
        ObjectHideSubObjectPermanently( self, "TABARD1", false )
    elseif tabard <= 0.40 then
        ObjectHideSubObjectPermanently( self, "TABARD2", false )
	elseif tabard <= 0.60 then
        ObjectHideSubObjectPermanently( self, "TABARD3", false )
    else
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
    end
	
	if leg <= 0.5 then
        ObjectHideSubObjectPermanently( self, "LEGS1", false )
     else
        ObjectHideSubObjectPermanently( self, "LEGS2", false )
    end
end

function OnGreyCompanyCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "FireArowTip", true )
	
	ObjectHideSubObjectPermanently( self, "HEADHELM1A", true )
	ObjectHideSubObjectPermanently( self, "HEADHELM1B", true )
	ObjectHideSubObjectPermanently( self, "HEADHELM2A", true )
	ObjectHideSubObjectPermanently( self, "HEADHELM2B", true )
	ObjectHideSubObjectPermanently( self, "HEADHELM3A", true )
	ObjectHideSubObjectPermanently( self, "HEADHELM3B", true )
	ObjectHideSubObjectPermanently( self, "HEADHELM4A", true )
	ObjectHideSubObjectPermanently( self, "HEADHELM4B", true )
	
	ObjectHideSubObjectPermanently( self, "BODY1", true )
	ObjectHideSubObjectPermanently( self, "BODY2", true )
	ObjectHideSubObjectPermanently( self, "BODY3", true )
	ObjectHideSubObjectPermanently( self, "BODY4", true )
	
	ObjectHideSubObjectPermanently( self, "HOODHELM", true )
	ObjectHideSubObjectPermanently( self, "HLMTHOOD", true )
	
	ObjectHideSubObjectPermanently( self, "HELM", true )

    local head         =    GetRandomNumber()
    local helmet         =    GetRandomNumber()
    local body 		=   GetRandomNumber()

    if head <= 0.125 then
        ObjectHideSubObjectPermanently( self, "HEADHELM1A", false )
    elseif head <= 0.25 then
        ObjectHideSubObjectPermanently( self, "HEADHELM1B", false )
	elseif head <= 0.375 then
        ObjectHideSubObjectPermanently( self, "HEADHELM2A", false )
	elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEADHELM2B", false )
	elseif head <= 0.625 then
        ObjectHideSubObjectPermanently( self, "HEADHELM3A", false )
	elseif head <= 0.75 then
        ObjectHideSubObjectPermanently( self, "HEADHELM3B", false )
	elseif head <= 0.875 then
        ObjectHideSubObjectPermanently( self, "HEADHELM4A", false )
    else
        ObjectHideSubObjectPermanently( self, "HEADHELM4B", false )
    end

    if body <= 0.25 then
        ObjectHideSubObjectPermanently( self, "BODY1", false )
    elseif body <= 0.50 then
        ObjectHideSubObjectPermanently( self, "BODY2", false )
	elseif body <= 0.75 then
        ObjectHideSubObjectPermanently( self, "BODY3", false )
    else
        ObjectHideSubObjectPermanently( self, "BODY4", false )
    end

    if helmet <= 0.600 then
        ObjectHideSubObjectPermanently( self, "HOODHELM", false )
        ObjectHideSubObjectPermanently( self, "HLMTHOOD", false )
    else
        ObjectHideSubObjectPermanently( self, "HELM", false )
    end
end

function OnNimrodelCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "SHIELD", true )
	ObjectHideSubObjectPermanently( self, "FireArowTip", true )
	ObjectHideSubObjectPermanently( self, "BODYLA1", true )
	ObjectHideSubObjectPermanently( self, "BODYLA2", true )
	ObjectHideSubObjectPermanently( self, "BODYLA3", true )
	ObjectHideSubObjectPermanently( self, "HEADLA1", true )
	ObjectHideSubObjectPermanently( self, "HEADLA2", true )
	ObjectHideSubObjectPermanently( self, "HEADLA3", true )
	ObjectHideSubObjectPermanently( self, "HORSE1", true )
	ObjectHideSubObjectPermanently( self, "HORSE2", true )
	ObjectHideSubObjectPermanently( self, "HORSE3", true )

    local head         =    GetRandomNumber()
    local horse         =    GetRandomNumber()
    local body 		=   GetRandomNumber()

    if horse <= 0.400 then
        ObjectHideSubObjectPermanently( self, "HORSE1", false )
    elseif horse <= 0.800 then
        ObjectHideSubObjectPermanently( self, "HORSE2", false )
    else
        ObjectHideSubObjectPermanently( self, "HORSE3", false )
    end

    if head <= 0.400 then
        ObjectHideSubObjectPermanently( self, "HEADLA1", false )
    elseif head <= 0.800 then
        ObjectHideSubObjectPermanently( self, "HEADLA1", false )
    else
        ObjectHideSubObjectPermanently( self, "HEADLA1", false )
    end

    if body <= 0.400 then
        ObjectHideSubObjectPermanently( self, "BODYLA1", false )
    elseif body <= 0.800 then
        ObjectHideSubObjectPermanently( self, "BODYLA2", false )
    else
        ObjectHideSubObjectPermanently( self, "BODYLA3", false )
    end
end

function OnDolAmrothKnightCreated(self)

    ObjectHideSubObjectPermanently( self, "M_LANCE01", true )
    ObjectHideSubObjectPermanently( self, "M_LANCE02", true )
    ObjectHideSubObjectPermanently( self, "SHIELD01", true )
    ObjectHideSubObjectPermanently( self, "SHIELD02", true )
    ObjectHideSubObjectPermanently( self, "SHIELD03", true )
    ObjectHideSubObjectPermanently( self, "SHIELD04", true )
    ObjectHideSubObjectPermanently( self, "SHIELD05", true )
    ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
    ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )

    local weapon         =    GetRandomNumber()
    local head         =    GetRandomNumber()
    local shield         =    GetRandomNumber()

    if weapon <= 0.5 then
        ObjectHideSubObjectPermanently( self, "M_LANCE01", false )
     else
        ObjectHideSubObjectPermanently( self, "M_LANCE02", false )
    end
	
    if head <= 0.03 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end 

    if shield <= 0.2 then
        ObjectHideSubObjectPermanently( self, "SHIELD01", false )
    elseif shield <= 0.4 then
        ObjectHideSubObjectPermanently( self, "SHIELD02", false )
    elseif shield <= 0.6 then
        ObjectHideSubObjectPermanently( self, "SHIELD03", false )
    elseif shield <= 0.8 then
        ObjectHideSubObjectPermanently( self, "SHIELD04", false )
     else
        ObjectHideSubObjectPermanently( self, "SHIELD05", false )
    end
end 

function OnVariagCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "SHIELD3", true )
    ObjectHideSubObjectPermanently( self, "SHIELD2", true )
    ObjectHideSubObjectPermanently( self, "SHIELD1", true )

    ObjectHideSubObjectPermanently( self, "HELMETGOLD", true )
    ObjectHideSubObjectPermanently( self, "HELMETGOLD1", true )
    ObjectHideSubObjectPermanently( self, "HELMETSILVER", true )
    ObjectHideSubObjectPermanently( self, "HELMETSILVER1", true )

    ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
    ObjectHideSubObjectPermanently( self, "HEAD04", true )
    ObjectHideSubObjectPermanently( self, "HEAD05", true )
    ObjectHideSubObjectPermanently( self, "HEAD06", true )
    ObjectHideSubObjectPermanently( self, "HEAD07", true )
    ObjectHideSubObjectPermanently( self, "HEAD08", true )
    ObjectHideSubObjectPermanently( self, "HEAD09", true )

    local helmet         =    GetRandomNumber()
    local head          =    GetRandomNumber()
    local shield          =    GetRandomNumber()

    if helmet <= 0.2 then
        ObjectHideSubObjectPermanently( self, "HELMETGOLD", false )
    elseif helmet <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HELMETGOLD1", false )
    elseif helmet <= 0.8 then
        ObjectHideSubObjectPermanently( self, "HELMETSILVER", false )
     else
        ObjectHideSubObjectPermanently( self, "HELMETSILVER1", false )
    end  

    if shield <= 0.3 then
        ObjectHideSubObjectPermanently( self, "SHIELD3", false )
    elseif shield <= 0.6 then
        ObjectHideSubObjectPermanently( self, "SHIELD2", false )
     else
        ObjectHideSubObjectPermanently( self, "SHIELD1", false )
    end  

    if head <= 0.1 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
    elseif head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    elseif head <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HEAD04", false )
    elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEAD05", false )
    elseif head <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HEAD06", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEAD07", false )
    elseif head <= 0.9 then
        ObjectHideSubObjectPermanently( self, "HEAD08", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD09", false )
    end   
end

function OnGoblinTownFighterCreated(self)
    ObjectHideSubObjectPermanently( self, "ARMORA", true )
    ObjectHideSubObjectPermanently( self, "ARMORAB", true )
    ObjectHideSubObjectPermanently( self, "ARMORB", true )
    ObjectHideSubObjectPermanently( self, "GOBLIN", true )
    ObjectHideSubObjectPermanently( self, "GOBLINA", true )
    ObjectHideSubObjectPermanently( self, "GOBLINB", true )
    ObjectHideSubObjectPermanently( self, "WEAP1", true )
    ObjectHideSubObjectPermanently( self, "WEAP2", true )
    ObjectHideSubObjectPermanently( self, "WEAP3", true )
    ObjectHideSubObjectPermanently( self, "WEAP4", true )
    ObjectHideSubObjectPermanently( self, "WEAP5", true )
    ObjectHideSubObjectPermanently( self, "WEAP6", true )

    local helmet         =    GetRandomNumber()
    local head          =    GetRandomNumber()
    local shield          =    GetRandomNumber()

    if helmet <= 0.2 then
        ObjectHideSubObjectPermanently( self, "ARMORA", false )
    elseif helmet <= 0.4 then
        ObjectHideSubObjectPermanently( self, "ARMORB", false )
    elseif helmet <= 0.8 then
        ObjectHideSubObjectPermanently( self, "ARMORAB", false )
     else
        ObjectHideSubObjectPermanently( self, "ARMORA", false )
        ObjectHideSubObjectPermanently( self, "ARMORAB", false )
        ObjectHideSubObjectPermanently( self, "ARMORB", false )
    end  

    if shield <= 0.3 then
        ObjectHideSubObjectPermanently( self, "GOBLIN", false )
    elseif shield <= 0.6 then
        ObjectHideSubObjectPermanently( self, "GOBLINA", false )
     else
        ObjectHideSubObjectPermanently( self, "GOBLINB", false )
    end  

    if head <= 0.10 then
        ObjectHideSubObjectPermanently( self, "WEAP1", false )
    elseif head <= 0.20 then
        ObjectHideSubObjectPermanently( self, "WEAP2", false )
    elseif head <= 0.30 then
        ObjectHideSubObjectPermanently( self, "WEAP3", false )
    elseif head <= 0.37 then
        ObjectHideSubObjectPermanently( self, "WEAP4", false )
    elseif head <= 0.40 then
        ObjectHideSubObjectPermanently( self, "WEAP5", false )
    elseif head <= 0.50 then
        ObjectHideSubObjectPermanently( self, "WEAP6", false )

    elseif head <= 0.60 then
        ObjectHideSubObjectPermanently( self, "WEAP1", false )
        ObjectHideSubObjectPermanently( self, "WEAP3", false )
    elseif head <= 0.70 then
        ObjectHideSubObjectPermanently( self, "WEAP2", false )
        ObjectHideSubObjectPermanently( self, "WEAP6", false )
    elseif head <= 0.80 then
        ObjectHideSubObjectPermanently( self, "WEAP3", false )
        ObjectHideSubObjectPermanently( self, "WEAP6", false )
    elseif head <= 0.90 then
        ObjectHideSubObjectPermanently( self, "WEAP2", false )
        ObjectHideSubObjectPermanently( self, "WEAP4", false )
     else
        ObjectHideSubObjectPermanently( self, "WEAP1", false )
        ObjectHideSubObjectPermanently( self, "WEAP6", false )
    end   
end

function OnGoblinTownFighterMillCreated(self)
    ObjectHideSubObjectPermanently( self, "ARMORA", true )
    ObjectHideSubObjectPermanently( self, "ARMORAB", true )
    ObjectHideSubObjectPermanently( self, "ARMORB", true )
    ObjectHideSubObjectPermanently( self, "ARMORA1", true )
    ObjectHideSubObjectPermanently( self, "ARMORAB1", true )
    ObjectHideSubObjectPermanently( self, "ARMORB1", true )
    ObjectHideSubObjectPermanently( self, "GOBLIN", true )
    ObjectHideSubObjectPermanently( self, "GOBLINA", true )
    ObjectHideSubObjectPermanently( self, "GOBLINB", true )
    ObjectHideSubObjectPermanently( self, "GOBLIN1", true )
    ObjectHideSubObjectPermanently( self, "GOBLINA1", true )
    ObjectHideSubObjectPermanently( self, "GOBLINB1", true )

    local helmet         =    GetRandomNumber()
    local head          =    GetRandomNumber()
    local shield          =    GetRandomNumber()
    local body          =    GetRandomNumber()

    if helmet <= 0.2 then
        ObjectHideSubObjectPermanently( self, "ARMORA", false )
    elseif helmet <= 0.4 then
        ObjectHideSubObjectPermanently( self, "ARMORB", false )
    elseif helmet <= 0.8 then
        ObjectHideSubObjectPermanently( self, "ARMORAB", false )
     else
        ObjectHideSubObjectPermanently( self, "ARMORA", false )
        ObjectHideSubObjectPermanently( self, "ARMORAB", false )
        ObjectHideSubObjectPermanently( self, "ARMORB", false )
    end  

    if shield <= 0.3 then
        ObjectHideSubObjectPermanently( self, "GOBLIN", false )
    elseif shield <= 0.6 then
        ObjectHideSubObjectPermanently( self, "GOBLINA", false )
     else
        ObjectHideSubObjectPermanently( self, "GOBLINB", false )
    end  
	
    if body <= 0.3 then
        ObjectHideSubObjectPermanently( self, "GOBLIN1", false )
    elseif body <= 0.6 then
        ObjectHideSubObjectPermanently( self, "GOBLINA1", false )
     else
        ObjectHideSubObjectPermanently( self, "GOBLINB1", false )
    end  

    if head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "ARMORA1", false )
    elseif head <= 0.4 then
        ObjectHideSubObjectPermanently( self, "ARMORB1", false )
    elseif head <= 0.8 then
        ObjectHideSubObjectPermanently( self, "ARMORAB1", false )
     else
        ObjectHideSubObjectPermanently( self, "ARMORA1", false )
        ObjectHideSubObjectPermanently( self, "ARMORAB1", false )
        ObjectHideSubObjectPermanently( self, "ARMORB1", false )
    end  
end

function OnLamedonCreated(self)
    ObjectHideSubObjectPermanently( self, "HEAD01", true )
	ObjectHideSubObjectPermanently( self, "HEAD02", true )
	ObjectHideSubObjectPermanently( self, "HEAD03", true )
    ObjectHideSubObjectPermanently( self, "ARMOR", true )
    ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
    ObjectHideSubObjectPermanently( self, "SHIELDA", true )
    ObjectHideSubObjectPermanently( self, "SHIELDB", true )
    ObjectHideSubObjectPermanently( self, "SASH01", true )
    ObjectHideSubObjectPermanently( self, "SASH02", true )
    ObjectHideSubObjectPermanently( self, "SASH03", true )
	ObjectHideSubObjectPermanently( self, "GEAR", true)

    local head          =    GetRandomNumber()
	local sash          =    GetRandomNumber()
    local shield          =    GetRandomNumber()


    if shield <= 0.2 then
        ObjectHideSubObjectPermanently( self, "SHIELDA", false )
     else
        ObjectHideSubObjectPermanently( self, "SHIELDB", false )
    end  
	
	if sash <= 0.2 then
        ObjectHideSubObjectPermanently( self, "SASH01", false )
     elseif sash <= 0.4 then
        ObjectHideSubObjectPermanently( self, "SASH02", false )
     else
        ObjectHideSubObjectPermanently( self, "SASH03", false )
    end  

    if head <= 0.03 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end   
end

function OnLinhirCreated(self)
	ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
    ObjectHideSubObjectPermanently( self, "ARMOR", true )
    ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
    ObjectHideSubObjectPermanently( self, "BODY01", true )
    ObjectHideSubObjectPermanently( self, "BODY02", true )
    ObjectHideSubObjectPermanently( self, "BODY03", true )
    ObjectHideSubObjectPermanently( self, "ARMS01", true )
    ObjectHideSubObjectPermanently( self, "ARMS02", true )
    ObjectHideSubObjectPermanently( self, "ARMS03", true )

    local head          =    GetRandomNumber()
	local sash          =    GetRandomNumber()

	
	if sash <= 0.4 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
        ObjectHideSubObjectPermanently( self, "ARMS01", false )
--     elseif sash <= 0.6 then -- removed
--        ObjectHideSubObjectPermanently( self, "BODY03", false )
--        ObjectHideSubObjectPermanently( self, "ARMS03", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY02", false )
        ObjectHideSubObjectPermanently( self, "ARMS02", false )
    end  

    if head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
	else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end 
end

function OnKataphractCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "SHIELD", true )
	ObjectHideSubObjectPermanently( self, "FireArowTip", true )
	ObjectHideSubObjectPermanently( self, "HELM01", true )
	ObjectHideSubObjectPermanently( self, "HELM02", true )
	ObjectHideSubObjectPermanently( self, "HELM03", true )
	ObjectHideSubObjectPermanently( self, "HA", true )
	ObjectHideSubObjectPermanently( self, "DRAGONORNAMENT", true )
	ObjectHideSubObjectPermanently( self, "SHIELDBACK", true )
	
	local sash          =    GetRandomNumber()
	
	if sash <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HELM01", false )
     elseif sash <= 0.6 then
        ObjectHideSubObjectPermanently( self, "HELM02", false )
     else
        ObjectHideSubObjectPermanently( self, "HELM03", false )
    end  
end

function OnMorgulOrcCreated(self)
    ObjectHideSubObjectPermanently( self, "HEAD06", true )
    ObjectHideSubObjectPermanently( self, "HEAD05", true )
    ObjectHideSubObjectPermanently( self, "HEAD04", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
    ObjectHideSubObjectPermanently( self, "SHIELD1", true )
    ObjectHideSubObjectPermanently( self, "SHIELD2", true )
    ObjectHideSubObjectPermanently( self, "SHIELD3", true )
    ObjectHideSubObjectPermanently( self, "SHOULDER", true )
    ObjectHideSubObjectPermanently( self, "LEGGUARD", true )

    local head          =    GetRandomNumber()
    local shield          =    GetRandomNumber()


    if shield <= 0.2 then
        ObjectHideSubObjectPermanently( self, "SHIELD2", false )
    elseif shield <= 0.5 then
        ObjectHideSubObjectPermanently( self, "SHIELD3", false )
     else
        ObjectHideSubObjectPermanently( self, "SHIELD1", false )
    end  

    if head <= 0.1 then
        ObjectHideSubObjectPermanently( self, "HEAD06", false )
    elseif head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HEAD05", false )
    elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HEAD04", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    elseif head <= 0.9 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
    end   
end

function OnIsengardDunlandGuardCreated(self)
    ObjectHideSubObjectPermanently( self, "HEAVY", true )
	ObjectHideSubObjectPermanently( self, "BODYA", true )
	ObjectHideSubObjectPermanently( self, "BODYB", true )
    ObjectHideSubObjectPermanently( self, "TORCH", true )
    ObjectHideSubObjectPermanently( self, "FireArowTip", true )
    ObjectHideSubObjectPermanently( self, "FORGED_BLADE", true )
    ObjectHideSubObjectPermanently( self, "SHIELDA", true )
    ObjectHideSubObjectPermanently( self, "SHIELDB", true )
    ObjectHideSubObjectPermanently( self, "SHIELDC", true )
    -- helmet types
    ObjectHideSubObjectPermanently( self, "HEAD1", true )
    ObjectHideSubObjectPermanently( self, "HEAD2", true )
    ObjectHideSubObjectPermanently( self, "HEAD3", true )
    ObjectHideSubObjectPermanently( self, "HEAD4", true )
	
    ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
    ObjectHideSubObjectPermanently( self, "HEAD04", true )
    
    local helm = GetRandomNumber()
    local head = GetRandomNumber()
    local head2 = GetRandomNumber()
    local body = GetRandomNumber()
    local armor = GetRandomNumber()	
    local shield   =    GetRandomNumber()


    if shield <= 0.4 then
        ObjectHideSubObjectPermanently( self, "SHIELDA", false )
    elseif shield <= 0.8 then
        ObjectHideSubObjectPermanently( self, "SHIELDB", false )
     else
        ObjectHideSubObjectPermanently( self, "SHIELDC", false )
    end  
	
    if armor <= 0.5 then
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAVY", false )
    end
	
    if body <= 0.5 then
        ObjectHideSubObjectPermanently( self, "BODYA", false )
     else
        ObjectHideSubObjectPermanently( self, "BODYB", false )
    end
	
    if helm <= 0.7 then
        ObjectHideSubObjectPermanently( self, "NOTHING", false )
     else
        ObjectHideSubObjectPermanently( self, "HELMET", false )
    end
    
    if head <= 0.25 then
        ObjectHideSubObjectPermanently( self, "HEAD1", false )
        elseif head <= 0.50 then
          ObjectHideSubObjectPermanently( self, "HEAD2", false )
    elseif head <= 0.75 then
        ObjectHideSubObjectPermanently( self, "HEAD3", false )
    else
        ObjectHideSubObjectPermanently( self, "HEAD4", false )
    end
    if head2 <= 0.25 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
        elseif head2 <= 0.50 then
          ObjectHideSubObjectPermanently( self, "HEAD02", false )
    elseif head2 <= 0.75 then
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    else
        ObjectHideSubObjectPermanently( self, "HEAD04", false )
    end
end

function OnGBarracksCreated(self)
	ObjectHideSubObjectPermanently( self, "RACK1", true )
	ObjectHideSubObjectPermanently( self, "RACK2", true )

    local weapon         =    GetRandomNumber()

    if weapon <= 0.5 then
        ObjectHideSubObjectPermanently( self, "RACK2", false )
     else
        ObjectHideSubObjectPermanently( self, "RACK1", false )
    end
end

function OnStarlightActivated(self) --
	ObjectBroadcastEventToEnemies( self, "BeUncontrollablyAfraid", 350 )
end

function OnFirienholtWardenCreated(self)
end

function OnRohanFarmCreated(self)
	--ObjectDoSpecialPower(self, "SpecialAbilitRohanFarmProduction")
	ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl3" )
	ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl7" )	
	ObjectGrantUpgrade( self, "Upgrade_GondorArcheryRangeLevel2" )
	
	local animals         =    GetRandomNumber()

    if animals <= 0.5 then
        ObjectGrantUpgrade( self, "Upgrade_GondorArcheryRangeLevel2" )
    else
        --ObjectGrantUpgrade( self, "Upgrade_GondorStableLevel2" )
    end	
end

function OnEowynReadinessActivated(self)
	ObjectDoSpecialPower(self, "SpecialAbilitRohanFarmPeasantry")	
    ObjectRemoveUpgrade( self, "Upgrade_SwitchToRockThrowing" )
    ObjectGrantUpgrade( self, "Upgrade_Drafted" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl4" )	-- unlock the peasant button
end

function OnEowynReadinessDeactivated(self)
    ObjectRemoveUpgrade( self, "Upgrade_Drafted" )
    ObjectGrantUpgrade( self, "Upgrade_SwitchToRockThrowing" )
end

function OnEowynWeaponToggle(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl2" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl3" )
end

function OnEowynNotWeaponToggle(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl3" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl2" )
end

function OnEowynMounted(self)
    ObjectRemoveUpgrade( self, "Upgrade_SwitchToRockThrowing" )
    ObjectGrantUpgrade( self, "Upgrade_Drafted" )		
end

function OnEowynNotMounted(self)
    ObjectRemoveUpgrade( self, "Upgrade_Drafted" )
    ObjectGrantUpgrade( self, "Upgrade_SwitchToRockThrowing" )
end

function OnYeomenBannerCreated(self)
	ObjectHideSubObjectPermanently( self, "BODY01", true )
	ObjectHideSubObjectPermanently( self, "BODY02", true )
	ObjectHideSubObjectPermanently( self, "BODY03", true )
	ObjectHideSubObjectPermanently( self, "BODY04", true )	
	ObjectHideSubObjectPermanently( self, "FLAG01", true )
	ObjectHideSubObjectPermanently( self, "FLAG02", true )
	ObjectHideSubObjectPermanently( self, "FLAG03", true )
	ObjectHideSubObjectPermanently( self, "COIF", true )
	ObjectHideSubObjectPermanently( self, "HELM01", true )
	ObjectHideSubObjectPermanently( self, "HELM02", true )
	ObjectHideSubObjectPermanently( self, "HELM03", true )
	ObjectHideSubObjectPermanently( self, "HELMHEAD01", true )
	ObjectHideSubObjectPermanently( self, "HELMHEAD02", true )
	ObjectHideSubObjectPermanently( self, "NOHELM01", true )
	ObjectHideSubObjectPermanently( self, "NOHELM02", true )
    local head          =    GetRandomNumber()
    local flags          =    GetRandomNumber()
    local bodies          =    GetRandomNumber()
    if bodies <= 0.2 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
    elseif bodies <= 0.5 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
	elseif bodies <= 0.8 then
        ObjectHideSubObjectPermanently( self, "BODY03", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY04", false )
    end
    if flags <= 0.3 then
        ObjectHideSubObjectPermanently( self, "FLAG01", false )
    elseif flags <= 0.6 then
        ObjectHideSubObjectPermanently( self, "FLAG03", false )
     else
        ObjectHideSubObjectPermanently( self, "FLAG03", false )
    end
    if head <= 0.1 then
        ObjectHideSubObjectPermanently( self, "NOHELM01", false )
    elseif head <= 0.2 then
        ObjectHideSubObjectPermanently( self, "NOHELM02", false )
    elseif head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "COIF", false )
    elseif head <= 0.4 then
        ObjectHideSubObjectPermanently( self, "HELM01", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HELM02", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.65 then
        ObjectHideSubObjectPermanently( self, "HELM03", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HELM01", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.9 then
        ObjectHideSubObjectPermanently( self, "HELM02", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
     else
        ObjectHideSubObjectPermanently( self, "HELM03", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    end
end

function OnAxemenofLossCreated(self)
    ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
    ObjectHideSubObjectPermanently( self, "ARMOR", true )
    ObjectHideSubObjectPermanently( self, "HEAD01", true )
    ObjectHideSubObjectPermanently( self, "HEAD02", true )
    ObjectHideSubObjectPermanently( self, "HEAD03", true )
    local helmet         =    GetRandomNumber()
    if helmet <= 0.333 then
        ObjectHideSubObjectPermanently( self, "HEAD01", false )
    elseif helmet <= 0.666 then
        ObjectHideSubObjectPermanently( self, "HEAD02", false )
     else
        ObjectHideSubObjectPermanently( self, "HEAD03", false )
    end
end

function OnTheodredCreated(self)
	ObjectHideSubObjectPermanently( self, "HELM", true )
	ObjectHideSubObjectPermanently( self, "CLOAK", true )
end

--function OnRohanBuildingDruedain(self)
--	ObjectDoSpecialPower(self, "SpecialAbilityFaithfulStoneSummon")
--end

function OnDruedainDeactivated(self)
    ObjectRemoveUpgrade( self, "Upgrade_DwarfFighterFearless" )	
    ObjectGrantUpgrade( self, "Upgrade_AngmarFighterFearless" )
end

function OnDruedainActivated(self)
    ObjectRemoveUpgrade( self, "Upgrade_AngmarFighterFearless" )
    ObjectGrantUpgrade( self, "Upgrade_DwarfFighterFearless" )
end

function OnTheodenGloriousChargeChained(self)
	ObjectDoSpecialPower(self, "SpecialAbilityTheodenGloriousCharge")
end

function OnYeomenRamCrewCreated(self)
	ObjectHideSubObjectPermanently( self, "Forged_Blade", true )
	ObjectHideSubObjectPermanently( self, "OBJECT02", true )
	ObjectHideSubObjectPermanently( self, "BODY01", true )
	ObjectHideSubObjectPermanently( self, "BODY02", true )
	ObjectHideSubObjectPermanently( self, "BODY03", true )
	ObjectHideSubObjectPermanently( self, "CLOAK01", true )
	ObjectHideSubObjectPermanently( self, "CLOAK02", true )
	ObjectHideSubObjectPermanently( self, "CLOAK03", true )
	ObjectHideSubObjectPermanently( self, "COIF", true )
	ObjectHideSubObjectPermanently( self, "HELM01", true )
	ObjectHideSubObjectPermanently( self, "HELM02", true )
	ObjectHideSubObjectPermanently( self, "HELM03", true )
	ObjectHideSubObjectPermanently( self, "HELMHEAD01", true )
	ObjectHideSubObjectPermanently( self, "HELMHEAD02", true )
	ObjectHideSubObjectPermanently( self, "NOHELM01", true )
	ObjectHideSubObjectPermanently( self, "NOHELM02", true )
	ObjectHideSubObjectPermanently( self, "LEGS01", true )
	ObjectHideSubObjectPermanently( self, "LEGS02", true )
	ObjectHideSubObjectPermanently( self, "SHIELD01", true )
	ObjectHideSubObjectPermanently( self, "SHIELD02", true )
	ObjectHideSubObjectPermanently( self, "SHIELD03", true )
	ObjectHideSubObjectPermanently( self, "SHIELD04", true )
	ObjectHideSubObjectPermanently( self, "SHIELD05", true )
	ObjectHideSubObjectPermanently( self, "SHIELD06", true )
	ObjectHideSubObjectPermanently( self, "SHIELD07", true )
    local head          =    GetRandomNumber()
    local shield          =    GetRandomNumber()
    local legs          =    GetRandomNumber()
    local cloaks          =    GetRandomNumber()
    local bodies          =    GetRandomNumber()
    if bodies <= 0.3 then
        ObjectHideSubObjectPermanently( self, "BODY01", false )
    elseif bodies <= 0.6 then
        ObjectHideSubObjectPermanently( self, "BODY02", false )
     else
        ObjectHideSubObjectPermanently( self, "BODY03", false )
    end
    if cloaks <= 0.3 then
        ObjectHideSubObjectPermanently( self, "CLOAK01", false )
    elseif cloaks <= 0.6 then
        ObjectHideSubObjectPermanently( self, "CLOAK03", false )
     else
        ObjectHideSubObjectPermanently( self, "CLOAK02", false )
    end
    if legs <= 0.5 then
        ObjectHideSubObjectPermanently( self, "LEGS01", false )
     else
        ObjectHideSubObjectPermanently( self, "LEGS02", false )
    end	
    if shield <= 0.1 then
        ObjectHideSubObjectPermanently( self, "SHIELD01", false )
    elseif shield <= 0.3 then
        ObjectHideSubObjectPermanently( self, "SHIELD02", false )
    elseif shield <= 0.4 then
        ObjectHideSubObjectPermanently( self, "SHIELD03", false )
    elseif shield <= 0.5 then
        ObjectHideSubObjectPermanently( self, "SHIELD04", false )
    elseif shield <= 0.7 then
        ObjectHideSubObjectPermanently( self, "SHIELD05", false )
    elseif shield <= 0.9 then
        ObjectHideSubObjectPermanently( self, "SHIELD06", false )
     else
        ObjectHideSubObjectPermanently( self, "SHIELD07", false )
    end
    if head <= 0.15 then
        ObjectHideSubObjectPermanently( self, "COIF", false )
    elseif head <= 0.3 then
        ObjectHideSubObjectPermanently( self, "HELM01", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.5 then
        ObjectHideSubObjectPermanently( self, "HELM02", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.65 then
        ObjectHideSubObjectPermanently( self, "HELM03", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD01", false )
    elseif head <= 0.7 then
        ObjectHideSubObjectPermanently( self, "HELM01", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    elseif head <= 0.9 then
        ObjectHideSubObjectPermanently( self, "HELM02", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
     else
        ObjectHideSubObjectPermanently( self, "HELM03", false )
        ObjectHideSubObjectPermanently( self, "HELMHEAD02", false )
    end
end

function OnRivendellUnitsEvenstar(self)
	ObjectDoSpecialPower(self, "SpecialAbilityArwenEvenstarUnits")
end

function OnRivendellUnitsEvenstarActive(self)
    ObjectGrantUpgrade( self, "Upgrade_DwarvenForgedBlades" )
    ObjectRemoveUpgrade( self, "Upgrade_RohanForgedBlades" )	
end

function OnRivendellUnitsEvenstarNotActive(self)
    ObjectGrantUpgrade( self, "Upgrade_RohanForgedBlades" )
    ObjectRemoveUpgrade( self, "Upgrade_DwarvenForgedBlades" )
end

function OnAragornBow(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl2" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl3" )		
end

function OnAragornSword(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl3" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl2" )
end

function OnAragornHeroesBuff(self)
	ObjectDoSpecialPower(self, "SpecialAbilityAragornHeroesBuff")
end

function OnHobbitsStealth(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl2" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl3" )
end

function OnHobbitsNotStealth(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl3" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl2" )
end

function OnSwornAllegianceActivated(self)
    ObjectRemoveUpgrade( self, "Upgrade_SwitchToRockThrowing" )
    ObjectGrantUpgrade( self, "Upgrade_Drafted" )	
end

function OnSwornAllegianceDeactivated(self)
    ObjectRemoveUpgrade( self, "Upgrade_Drafted" )
    ObjectGrantUpgrade( self, "Upgrade_SwitchToRockThrowing" )
end

function OnForlongCreated(self)
    ObjectGrantUpgrade( self, "Upgrade_Evilboyos" )
end

function OnTwinUserTwoOn(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl2" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl3" )		
end

function OnTwinUserTwoOff(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl3" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl2" )
end

function BannerOfArwenUser2(self)
  ObjectDoSpecialPower(self, "SpecialAbilityBannerOfArwenSelf")
end

function OnRuinsStatueCreated(self)
	ObjectHideSubObjectPermanently( self, "OSBSTA02", true )
	ObjectHideSubObjectPermanently( self, "OSBSTA03", true )
	local chance  = 	GetRandomNumber()
	if chance <= 0.50 then
		ObjectHideSubObjectPermanently( self, "OSBSTA02", false )
	elseif chance > 0.50 then
		ObjectHideSubObjectPermanently( self, "OSBSTA03", false )
	end
end

function OnRuinsColumnCreated(self)
	ObjectHideSubObjectPermanently( self, "OBJECT20", true )
	ObjectHideSubObjectPermanently( self, "OBJECT21", true )
	ObjectHideSubObjectPermanently( self, "OSBRUIN06", true )
	ObjectHideSubObjectPermanently( self, "OSBRUIN14", true )
	local chance  = 	GetRandomNumber()
	
	if chance <= 0.25 then
		ObjectHideSubObjectPermanently( self, "OBJECT20", false )
	elseif chance <= 0.50 then
		ObjectHideSubObjectPermanently( self, "OBJECT21", false )
	elseif chance <= 0.75 then
		ObjectHideSubObjectPermanently( self, "OSBRUIN14", false )
	elseif chance > 0.75 then
		ObjectHideSubObjectPermanently( self, "OSBRUIN06", false )
	end
end

function OnRuinsDebrisCreated(self)
	ObjectHideSubObjectPermanently( self, "OSBRUINRUB01", true )
	ObjectHideSubObjectPermanently( self, "OSBRUINRUB03", true )
	local chance  = 	GetRandomNumber()
	if chance <= 0.50 then
		ObjectHideSubObjectPermanently( self, "OSBRUINRUB01", false )
	elseif chance > 0.50 then
		ObjectHideSubObjectPermanently( self, "OSBRUINRUB03", false )
	end
end

function DenethorTerror(self)
  ObjectDoSpecialPower(self, "SpecialAbilityScreech")
end

function ChillOfTheGrave(self)
  ObjectDoSpecialPower(self, "SpecialAbilitDraftTowerAnimation")
end

function OnUser2Activated(self)
    ObjectRemoveUpgrade( self, "Upgrade_SwitchToRockThrowing" )
    ObjectGrantUpgrade( self, "Upgrade_Drafted" )
end

function OnUser2Deactivated(self)
    ObjectRemoveUpgrade( self, "Upgrade_Drafted" )
    ObjectGrantUpgrade( self, "Upgrade_SwitchToRockThrowing" )
end

function OnUser5Deactivated(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl2" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl3" )		
end

function OnUser5Activated(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl3" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl2" )
end

function OnUser7Deactivated(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl4" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl5" )		
end

function OnUser7Activated(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl5" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl4" )
end

function OnDraftTowerCreated(self)
	ObjectHideSubObjectPermanently( self, "EORED", true )
	ObjectHideSubObjectPermanently( self, "FIRIEN", true )
	ObjectHideSubObjectPermanently( self, "WESTA", true )
	ObjectHideSubObjectPermanently( self, "WESTB", true )
end

function OnDraftTowerFinished(self)
    ObjectGrantUpgrade( self, "Upgrade_ObjectLevel1" )
end

function OnPeasantsDeactivated(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl2" )
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl4" )	
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl3" )
end

function OnPeasantsActivated(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl3" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl2" )
end

function OnProductionActivated(self)
    ObjectRemoveUpgrade( self, "Upgrade_HorseDiscipline" )
    ObjectGrantUpgrade( self, "Upgrade_CanTaunt" )		
end

function OnProductionDeactivated(self)
    ObjectRemoveUpgrade( self, "Upgrade_CanTaunt" )
    ObjectGrantUpgrade( self, "Upgrade_HorseDiscipline" )
end

function OnFarmProducing(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl7" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl10" )
end

function OnFarmNotProducing(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl10" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl7" )
end

function OnDurinDoorsCreated(self)
    ObjectHideSubObjectPermanently( self, "FX_L", true )
    ObjectHideSubObjectPermanently( self, "FX_R", true )
end

function OnHelmingasCreated(self)
	ObjectHideSubObjectPermanently( self, "SHIELDA", true )
	ObjectHideSubObjectPermanently( self, "SHIELDB", true )
	ObjectHideSubObjectPermanently( self, "SHIELDC", true )	
	ObjectHideSubObjectPermanently( self, "HORSE01", true )	
	ObjectHideSubObjectPermanently( self, "HORSE02", true )	
	ObjectHideSubObjectPermanently( self, "HORSE03", true )	
	ObjectHideSubObjectPermanently( self, "TAIL01", true )	
	ObjectHideSubObjectPermanently( self, "TAIL02", true )	
	ObjectHideSubObjectPermanently( self, "TAIL03", true )	
	local shield  = 	GetRandomNumber()
	local horse  = 	GetRandomNumber()
	local tail  = 	GetRandomNumber()
	if shield <= 0.33 then
		ObjectHideSubObjectPermanently( self, "SHIELDA", false )
	elseif shield <= 0.66 then
		ObjectHideSubObjectPermanently( self, "SHIELDB", false )
	else
		ObjectHideSubObjectPermanently( self, "SHIELDC", false )		
	end
	if horse <= 0.33 then
		ObjectHideSubObjectPermanently( self, "HORSE01", false )
	elseif horse <= 0.66 then
		ObjectHideSubObjectPermanently( self, "HORSE02", false )
	else
		ObjectHideSubObjectPermanently( self, "HORSE03", false )		
	end
	if tail <= 0.33 then
		ObjectHideSubObjectPermanently( self, "TAIL01", false )
	elseif tail <= 0.66 then
		ObjectHideSubObjectPermanently( self, "TAIL02", false )
	else
		ObjectHideSubObjectPermanently( self, "TAIL03", false )		
	end
end

function OnPippinCreated(self)
	ObjectHideSubObjectPermanently( self, "HAHEADNEW", true )
	ObjectHideSubObjectPermanently( self, "LORIENCLOAK", true)
	ObjectHideSubObjectPermanently( self, "LORIENBROOCH", true )
    ObjectHideSubObjectPermanently( self, "HABODY", true )
    ObjectHideSubObjectPermanently( self, "HASHEATHS", true )
    ObjectHideSubObjectPermanently( self, "HASHEATH", true )
    ObjectHideSubObjectPermanently( self, "HASWORD", true )
end

function OnMerryCreated(self)
	ObjectHideSubObjectPermanently( self, "HAHEADNEW", true )
	ObjectHideSubObjectPermanently( self, "LORIENCLOAK", true)
	ObjectHideSubObjectPermanently( self, "LORIENBROOCH", true )
    ObjectHideSubObjectPermanently( self, "HASHIELD", true )
    ObjectHideSubObjectPermanently( self, "HABODY", true )
    ObjectHideSubObjectPermanently( self, "HALEGS", true )
    ObjectHideSubObjectPermanently( self, "HASCABBARDS", true )
    ObjectHideSubObjectPermanently( self, "HASCABBARD", true )
    ObjectHideSubObjectPermanently( self, "HAMERRYSWORD", true )
end

function OnHelmingasMounted(self)
    ObjectRemoveUpgrade( self, "Upgrade_HorseDiscipline" )
    ObjectGrantUpgrade( self, "Upgrade_CanTaunt" )
end

function OnHelmingasDisounted(self)
    ObjectRemoveUpgrade( self, "Upgrade_CanTaunt" )
    ObjectGrantUpgrade( self, "Upgrade_HorseDiscipline" )
end

function OnLibraryCreated(self)
	ObjectGrantUpgrade( self, "Upgrade_GondorStableLevel2" )
	ObjectDoSpecialPower(self, "SpecialAbilityLibraryDisabled")		
end

function OnLibraryIdle(self)
	ObjectDoSpecialPower(self, "SpecialAbilityLibraryDisabled")	
end

function OnTwinsDeathEggCreated(self)
	ObjectDoSpecialPower(self, "SpecialAbilityLibraryDisabled")	
end

function OnIsengardInfantryFocusCreated(self)
	ObjectGrantUpgrade( self, "Upgrade_StructureLevel1" )
	ObjectGrantUpgrade( self, "Upgrade_GondorStableLevel2" )
	ObjectDoSpecialPower(self, "SpecialAbilityLibraryDisabled")		
end

function OnIsengardFocusCreated(self)
	ObjectGrantUpgrade( self, "Upgrade_StructureLevel1" )
end

function OnBoromirStubborn(self)
	ObjectDoSpecialPower(self, "SpecialAbilityBoromirStubborn")
end

function OnBerserkerWithTorch(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl2" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl3" )		
end

function OnBerserkerWithoutTorch(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl3" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl2" )
end

function OnDainMounted(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl2" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl3" )		
end

function OnDainNotMounted(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl3" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl2" )
end

function OnUniversalBuildplotCreated(self)
    ObjectGrantUpgrade( self, "Upgrade_HorseDiscipline" )
end

function OnHaradBCEnabled(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl4" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl5" )
end

function OnHaradBCDisabled(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl5" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl4" )
end

function OnHaradHAEnabled(self)
    ObjectRemoveUpgrade( self, "Upgrade_HorseDiscipline" )
    ObjectGrantUpgrade( self, "Upgrade_CanTaunt" )
end

function OnHaradHADisabled(self)
    ObjectRemoveUpgrade( self, "Upgrade_CanTaunt" )
    ObjectGrantUpgrade( self, "Upgrade_HorseDiscipline" )
end

function OnHaradFAEnabled(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl2" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl3" )
end

function OnHaradFADisabled(self)
    ObjectRemoveUpgrade( self, "Upgrade_MiniHordeLvl3" )
    ObjectGrantUpgrade( self, "Upgrade_MiniHordeLvl2" )
end

function OnCreateRisenDead(self)
	ObjectDoSpecialPower(self, "SpecialAbilityCreateRisenDead")
end

function OnCreateGuldurBuilding(self)
	ObjectDoSpecialPower(self, "SpecialAbilityGuldurBuilderKiller")
end

function OnCreateGuldurSpiderLair(self)
	ObjectDoSpecialPower(self, "SpecialAbilityGuldurBuilderKiller")
	ObjectDoSpecialPower(self, "SpecialAbilityLibraryDisabled")	
end

function OnRestlessWarriorCreated(self)
	ObjectHideSubObjectPermanently( self, "AXE", true )
	ObjectHideSubObjectPermanently( self, "SWORDB", true )

    local weapon    =    GetRandomNumber()
    
    if weapon <= 0.5 then
        ObjectHideSubObjectPermanently( self, "AXE", false )
    else
        ObjectHideSubObjectPermanently( self, "SWORDB", false )
    end
	
end

function OnBrokenRabbleCreated(self)
	ObjectHideSubObjectPermanently( self, "WEAP1", true )
	ObjectHideSubObjectPermanently( self, "WEAP2", true )
	ObjectHideSubObjectPermanently( self, "WEAP3", true )	

    local weapon    =    GetRandomNumber()
    
    if weapon <= 0.33 then
        ObjectHideSubObjectPermanently( self, "WEAP1", false )
	elseif weapon <= 0.66 then
		ObjectHideSubObjectPermanently( self, "WEAP2", false )	
    else
        ObjectHideSubObjectPermanently( self, "WEAP3", false )
    end
	
end

function OnBrokenRabbleSlavedCreated(self)
	ObjectHideSubObjectPermanently( self, "WEAP1", true )
	ObjectHideSubObjectPermanently( self, "WEAP2", true )
	ObjectHideSubObjectPermanently( self, "WEAP3", true )
end

function OnBrokenRabbleHordeCreated(self)
    ObjectGrantUpgrade( self, "Upgrade_HorseDiscipline" )
end